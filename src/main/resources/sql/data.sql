/* Populate USER_PROFILE Table */
INSERT INTO USER_PROFILE(type)
VALUES ('ADMIN');

INSERT INTO USER_PROFILE(type)
VALUES ('CONTADOR');

INSERT INTO USER_PROFILE(type)
VALUES ('ADMINISTRATIVO');

/* Populate one Admin User which will further create other users for the application using GUI
   user: sam
   pass: abc125
*/
INSERT INTO APP_USER(sso_id, password, first_name, last_name, email)
VALUES ('sam','$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'Sam','Smith','samy@xyz.com');

/* Populate JOIN Table */
INSERT INTO APP_USER_USER_PROFILE (user_id, user_profile_id)
SELECT user.id, profile.id FROM APP_USER user, USER_PROFILE profile
where user.sso_id='sam' and profile.type='ADMIN';

INSERT INTO dataconfig (id, name, property, value) VALUES
(1, 'Cantidad de minutos para cancelar un turno', 'treshold.turno.reservation', '15'),
(2, 'Password correo', 'email.password', ''),
(3, 'Correo para notificaciones', 'email.user', ''),
(4, 'Valor de un punto de promocion', 'price.point.promotion', '10');

<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><spring:message code="reporte.title" /></title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet"/>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
  
</head>
<body class="text-center">
	<jsp:include page="../includes/menu.jsp"></jsp:include>
	
	
	
	<div class="d-flex justify-content-center py-4">
	   <div id="main-card" class="card text-center">
	       <div class="card-header">
	            <spring:message code="reporte.title" />
	       </div>
	       <div class="card-body table-responsive"  style="text-align: left !important;">
	       
				<c:url var="servicios" value="/reportes/servicios"/>
				<c:url var="profesionales" value="/reportes/profesionales"/>
				<c:url var="servRecaudacion" value="/reportes/servRecaudacion"/>
				<form method="get" >
					<div  class="custom-control my-1 mr-sm-2">
						<label for="sucursal" class="col-sm-2 col-form-label"><spring:message code="reporte.sucursal" /></label>
						<select class="my-1 mr-sm-7" id="sucursal" name="sucursal" required></select>
					</div>
					<div class="custom-control my-1 mr-sm-2" >
						<label for="sucursal" class="col-sm-2 col-form-label"><spring:message code="reporte.inicio" /></label>
						<input type="text" name = "inicio">
					</div>
					
					<div class="custom-control my-1 mr-sm-2">
						<label for="sucursal" class="col-sm-2 col-form-label"><spring:message code="reporte.fin" /></label>
						<input type="text" name = "fin">
					</div>
						<br>
						<button formaction="${servicios}" type="submit" class="btn btn-success" ><spring:message code="reporte.serviciosMasConsumidos" /></button>
						<br>
						<br>
						<button formaction="${profesionales}" type="submit" class="btn btn-success">Recaudacion por profesional</button>
						<br>
						<br>
						<button formaction="${servRecaudacion}" type="submit" class="btn btn-success">Recaudacion por servicio</button>
				</form>
				
				
			</div>
	    </div>
	</div>  
<script>
function getUrlGetSucursales(){
    return 'http://'+window.location.host+'<c:url value="/sucursales/getSucursales"/>';
}

var user = new SlimSelect({
    select: '#sucursal',
    hideSelectedOption: true,
    ajax: function (search, callback) {
        // Check search value. If you dont like it callback(false) or callback('Message String')
        //if (search.length < 3) {
        // callback('Need 3 characters')
        //return
        // }

        // Perform your own ajax request here
        console.log("Voy a buscar las sucursales");
        console.log(getUrlGetSucursales());
        fetch(getUrlGetSucursales())
            .then(function (response) {
            	console.log(response);
                return response.json()
            })
            .then(function (json) {
                let data = json
                // for (let i = 0; i < json.length; i++) {
                //     data.push({text: json[i].text})
                // }

                // Upon successful fetch send data to callback function.
                // Be sure to send data back in the proper format.
                // Refer to the method setData for examples of proper format.
                callback(data)
            })
            .catch(function(error) {
                // If any erros happened send false back through the callback
                callback(false)
            })
    }

})
</script>

</body>
</html>
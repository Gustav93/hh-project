<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><spring:message code="form.profesional" /></title>
<spring:message code="form.profesional.name" var="profesionalName" />
<spring:message code="form.profesional.lastname" var="profesionalLastName" />
<spring:message code="form.profesional.phone" var="profesionalPhone" />
<spring:message code="form.profesional.fechaNacimiento" var="profesionalFechaNacimiento" />
<spring:message code="form.profesional.fechaBaja" var="profesionalFechaBaja" />

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<link rel="canonical"
	href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
	type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css"
	rel="stylesheet" />

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- Datatable responsive -->
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

	<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="<c:url value='/static/datepicker/jquery-ui.js' />"></script>
	<script src="<c:url value='/static/datepicker/datepicker-es.js' />"></script>
	<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
</head>
<body class="text-center">
	<jsp:include page="../includes/menu.jsp"></jsp:include>

	<div class="d-flex justify-content-center py-4">
		<div id="main-card" class="card text-center">
			<div class="card-header"><spring:message code="form.profesional" />
			</div>
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<c:url var="addUrl" value="/profesionales/create" />
				<form:form method="POST" modelAttribute="profesionalDTO"
					action="${addUrl}">
					<form:input type="hidden" path="id" id="id" />
					<div class="form-group row">
						<label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.profesional.name" /></label>
						<div class="col-sm-10">
							<form:input type="text" path="nombre" id="nombre"
								class="form-control" placeholder="${profesionalName}"
								value="${profesional.nombre}" required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<label for="apellido" class="col-sm-2 col-form-label"><spring:message code="form.profesional.lastname" /></label>
						<div class="col-sm-10">
							<form:input type="text" path="apellido" id="apellido"
								class="form-control" placeholder="${profesionalLastName}"
								value="${profesional.apellido}" required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-sm-2 col-form-label"><spring:message code="form.profesional.email" /></label>
						<div class="col-sm-10">
							<form:input type="text" path="email" id="email"
								class="form-control" placeholder="Email"
								value="${profesional.email}" required="required"/>
						</div>
					</div>

					<div class="form-group row">
						<label for="telefono" class="col-sm-2 col-form-label"><spring:message code="form.profesional.phone" /></label>
						<div class="col-sm-10">
							<form:input type="text" path="telefono" id="telefono"
								class="form-control" placeholder="${profesionalPhone}"
								value="${profesional.telefono}" />
						</div>
					</div>

					<div class="form-group row">
						<label for="fechaDeNacimiento" class="col-sm-2 col-form-label"><spring:message code="form.profesional.fechaNacimiento" /></label>

						<div class="col-sm-10">
							<form:input path="fechaDeNacimiento" id="fechaDeNacimiento"
								class="form-control" value="${profesional.fechaDeNacimiento}" placeholder="${profesionalFechaNacimiento}"
								required="required" />
						</div>
					</div>

					<c:choose>
						<c:when test="${edit}">
							<div class="form-group row">
								<label for="fechaBaja" class="col-sm-2 col-form-label"><spring:message code="form.profesional.fechaBaja" /></label>
								<div class="col-sm-10">
									<form:input type="date" path="fechaBaja" id="avgTime"
										class="form-control" placeholder="${profesionalFechaBaja}"
										value="${profesional.fechaBaja}" />
								</div>
							</div>

							<div class="form-group row">
								<label for="estadoProfesional" class="col-sm-2 col-form-label"><spring:message code="form.profesional.state" /></label>
								<div class="col-sm-10">
									<form:select class="form-control" path="estadoProfesional">
										<form:option value="ACTIVO"><spring:message code="form.profesional.state.active" /></form:option>
										<form:option value="INACTIVO"><spring:message code="form.profesional.inactive" /></form:option>
									</form:select>
								</div>
							</div>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
					<div class="form-group row">
						<input type="submit" value="<spring:message code="form.profesional.button.add" />" class="btn btn-primary" />
					</div>
				</form:form>
			</div>
		</div>
	</div>

	<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
<script>
	$(document).ready(function() {
		//$('#servicios').DataTable();

		$('#precio').attr({
			"min" : "0",
			"step" : "0.01",
			"placeholder" : "0.00"
		});

		$('#avgTime').attr({
			"min" : "00:00",
			"max" : "07:59",
		});

		$('#fechaDeNacimiento').datepicker();
		$.datepicker.setDefaults( $.datepicker.regional[ "<spring:message code="buscar.idioma" />" ] );
	});
</script>
</html>

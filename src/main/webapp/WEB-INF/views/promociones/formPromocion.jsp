<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.promocion" /></title>
    <spring:message code="form.promocion.name" var="promocionName" />
    <spring:message code="form.promocion.discount" var="promocionDiscount" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="<c:url value='/static/datepicker/jquery-ui.js' />"></script>
    <script src="<c:url value='/static/datepicker/datepicker-es.js' />"></script>
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>

<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.promocion" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="promocionDTO">
                
                <form:input type="hidden" path="id" id="id"/>
                <form:input type="hidden" path="fechaDeAlta" id="fechaDeAlta"/>
                
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.promocion.name" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="nombre" id="nombre" class="form-control" placeholder="${promocionName}" required="required" />
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="descuento" class="col-sm-2 col-form-label"><spring:message code="form.promocion.discount" /></label>
                    <div class="col-sm-10">
                    	<form:input type="number" path="descuento" id="descuento" class="form-control" placeholder="${promocionDiscount}" min="0" max="90" required="required"/>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="fechaDeVencimiento" class="col-sm-2 col-form-label"><spring:message code="form.promocion.fechaVencimiento" /></label>
                    <div class="col-sm-2 col-form-label">
                        <form:input type="text" class="form-control" path="fechaDeVencimiento" id="fechaDeVencimiento" required="required"/>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="estado" class="col-sm-2 col-form-label"><spring:message code="form.promocion.state" /></label>
                    <div class="col-sm-10">
                        <form:select class="form-control" path="estado" id="estado">
                            <form:option value="ACTIVO"><spring:message code="form.promocion.state.active" /></form:option>
                           <form:option value="INACTIVO"><spring:message code="form.promocion.state.inactive" /></form:option>
                        </form:select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="estado" class="col-sm-2 col-form-label"><spring:message code="form.promocion.multiplicaPuntos" /></label>
                    <div class="col-sm-10">
                        <form:select class="form-control" path="multiplicaPuntos" id="multiplicaPuntos">
                            <form:option value="1">x1</form:option>
                           <form:option value="2">x2</form:option>
                           <form:option value="3">x3</form:option>
                           <form:option value="4">x4</form:option>
                           <form:option value="5">x5</form:option>
                        </form:select>
                    </div>
                </div>
                
                <div class="form-group row">
                	<label class="col-sm-2 col-form-label" for="servicios"><spring:message code="form.promocion.services" /></label>
                	<div class="col-sm-10">
                            <form:select id="multiple" multiple="multiple" path="servicios" required="requred">
                                <!--  <option selected>Seleccion</option>-->
                                <c:forEach items="${servicios}" var="servicio">
                                    <form:option value="${servicio.id}">${servicio.nombre}</form:option>
                                </c:forEach>
                            </form:select>
                    </div>
                </div>
                
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="<spring:message code="form.promocion.button.update" />" class="btn btn-primary"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="<spring:message code="form.promocion.button.add" />" class="btn btn-primary"/>
                    </c:otherwise>
                </c:choose>
                
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>

	<script>
        $('#fechaDeVencimiento').datepicker();
        $.datepicker.setDefaults( $.datepicker.regional[ "<spring:message code="buscar.idioma" />" ] );
    
    new SlimSelect({
    	  select: '#multiple'
    })
    
	console.log("FC");
	</script>
</body>
</html>
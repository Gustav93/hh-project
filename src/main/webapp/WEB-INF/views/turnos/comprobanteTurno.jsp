<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 15/9/2019
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Comprobante de Turno</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.3.1.js"></script>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
	type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css"
	rel="stylesheet" />


<!-- Bootstrap core CSS -->


<!-- Bootstrap core CSS -->

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- Bootstrap core CSS -->

<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

<!-- jsPDF library -->
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script type="text/javascript" src="<c:url value='/static/core/html2canvas.js' />"> </script>

<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
</head>
<body class="text-center">

	<jsp:include page="../includes/menu.jsp"></jsp:include>
<div id="comprobanteTurno">

	<div class="d-flex justify-content-center py-4">
		<div id="main-card" class="card text-center">
		
		<div class="card-header text-center ">
		      <div class="row align-items-center">
		      <c:choose>
				<c:when test="${not esPago}">
		             <div class="col-md-9">
		                    <h5>Comprobante de Turno</h5>
		             </div>
		        </c:when>
		         <c:otherwise>
			        <div class="col-md-9">
		                    <h5>Comprobante de Pago</h5>
		             </div>
			    </c:otherwise>
			</c:choose>
		             <div class="col-md-3">
					  <img src="<spring:url value="/images/logo-hh.jpeg"/>" height="200" width="200" class="img-fluid img-thumbnail rounded float-right" alt="..."/>
		             </div>
		      </div>
		 </div>
			
			<div class="card-body table-responsive nowrap" style="text-align: left !important;">
			
			<table id="Cliente"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">
					<thead>
						 <tr>
					      <th scope="col">#</th>
					      <th scope="col">Datos de Cliente</th>
					    </tr>
					</thead>
					<tbody>
						<tr>
							 <th scope="row">Numero de Turno</th>
						      <td>${turnoFrontDTO.turnoId}</td>
						</tr>
						<tr>
							 <th scope="row">Nombre</th>
						      <td>${turnoFrontDTO.name}</td>
						</tr>
						<tr>
							 <th scope="row">Apellido</th>
						      <td>${turnoFrontDTO.surname}</td>
						</tr>
						<tr>
							 <th scope="row">Mail</th>
						      <td>${turnoFrontDTO.email}</td>
						</tr>
						<tr>
							 <th scope="row">Estado de Turno</th>
						      <td>${turnoFrontDTO.status}</td>
						</tr>
						<tr>
							 <th scope="row">Dia de la reserva</th>
						      <td>${turnoFrontDTO.date}</td>
						</tr>
					</tbody>
				</table>
				
			<div class="card text-center" style="margin-bottom: 1rem;">
			  <div class="card-header">
			    Reserva
			  </div>
			  <div class="card-body">
			   <table id="reserva"
					class="table dt-responsive nowrap"
					style="width: 100%">
					<thead>
					<tr>
							<th>Servicio</th>
							<th>Profesional</th>
							<th>Duracion de servicio</th>
							<th>Hora de Reserva</th>
							<th>Hora de fin de Reserva</th>
							<th>Precio de Servicio</th>
					</tr>
					</thead>
					<tbody>
					
						<c:forEach items="${detalleTurnoList}" var="detalleTurno">
							<tr>
								<td>${detalleTurno.serviceName}</td>
								<td>${detalleTurno.professionalName}</td>
								<td>${detalleTurno.serviceTime}</td>
								<td>${detalleTurno.horaInicio}</td>
								<td>${detalleTurno.horaFin}</td>
								<td>${detalleTurno.price}</td>
							</tr>
						</c:forEach>
					
					</tbody>
				</table>
			  </div>
			</div>
		<c:choose>
			<c:when test="${not sinPromos}">
			<div class="card text-center" style="margin-bottom: 1rem;">
			<div class="card-header">Promociones</div>
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<table id="promos"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">
					<thead>
						<tr>
							<th>Nombre Promo</th>
							<th>Descuento</th>
							<th>Multiplicador Puntos</th>
						</tr>
					</thead>
					<tbody>

					<c:forEach items="${promosAplicadas}" var="promoAplicada">
						<tr>
							<td>${promoAplicada.nombre}</td>
							<td>${promoAplicada.descuento}%</td>
							<td>x${promoAplicada.multiplicaPuntos}</td>
							
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		</c:when>
	</c:choose>
			
			
		<c:choose>
			<c:when test="${esPago}">
			<div class="card text-center">
			  <div class="card-header">
			    Detalle de pago
			  </div>
			  <div class="card-body">
			   <table id="detalleDePago"
					class="table dt-responsive nowrap"
					style="width: 100%">
					<thead>
						<tr>
							 <th scope="row">Numero de pago</th>
						      <td>${pagoDTO.id}</td>
						</tr>
						<tr>
							 <th scope="row">Monto total de Servicios</th>
						      <td>${turnoFrontDTO.totalPrice}</td>
						</tr>
						<tr>
							 <th scope="row">Monto total a pagar</th>
						      <td>${pagoDTO.montoTotal}$</td>
						</tr>
						<tr>
							 <th scope="row">Estado de pago</th>
						      <td>${pagoDTO.estado}</td>
						</tr>
					<tr>
							<th>Monto de pago</th>
							<th>Tipo</th>
							<th>Fecha de pago</th>
					</tr>
					</thead>
					<tbody>
					
						<c:forEach items="${detallePagoDTO}" var="detallePago">
							<tr>
								 <td>${detallePago.monto}</td>
		                        <td>${detallePago.tipo}</td>
		                        <td>${detallePago.fecha}</td>
							</tr>
						</c:forEach>
					
					</tbody>
				</table>
			  </div>
			</div>
			</c:when>
		</c:choose>
			
		<c:choose>
			<c:when test="${not esPago}">
			<div style="width: 22rem; margin:0 auto;"> 
			<div class="card text-center mt-3" style="max-width: 18rem;">
			  <div class="card-header">
			    Total a Pagar
			  </div>
			  <div class="card-body">
			    <p class="card-text">Precio Final: $ ${turnoFrontDTO.totalPrice}</p>
			  </div>
			</div>
			</div>
			</c:when>
		</c:choose>
			
			</div>
		</div>
	</div>
</div>	

  <%--modal--%>
   
		
			<div id="modalFile" class="modal fade bd-example-modal-lg" tabindex="-1"
				role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Cargar comprobante a enviar</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<div class="modal-body">
						
						<c:url var="addUrl" value="/turnos/enviarComprobante?${_csrf.parameterName}=${_csrf.token}" />
						<form:form action="${addUrl}" modelAttribute="turnoFrontDTO" method="post" enctype="multipart/form-data">
							    <label class="my-1 mr-2" for="file">Archivo</label>
							    <form:input path="turnoId" type="hidden" value="${turnoFrontDTO.turnoId}" id="turnoId"/>
								<input class="my-1 mr-2" id="file" type="file" name="file"/>
							    <input type="submit" />
						</form:form>

								
				  		<input type="submit" id="botonDisp" formmethod="post" formaction="${nuevaDisponibilidad}" value="Agregar" class="btn btn-primary"/>
						</div>
					</div>
				</div>
			</div>
		
		
		   <%--modal--%>


	<button id="descargaComprobante" type="button" onclick="print()" class="btn-secondary" style="margin-bottom: 1rem;" data-esPago="${esPago}">Descargar Comprobante</button>
	<a data-toggle="modal" data-target="#modalFile" 
		class="btn btn-secondary custom-width" style="margin-bottom: 1rem;">Enviar comprobante</a>
		
	<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
<script>
function makePDF() {
	//href="<c:url value='/turnos/enviarComprobante/${turnoFrontDTO.turnoId}'/>"
    var quotes = document.getElementById('comprobanteTurno');

    html2canvas(quotes, {
        onrendered: function(canvas) {

        //! MAKE YOUR PDF
        var pdf = new jsPDF('p', 'pt', 'letter');

        for (var i = 0; i <= quotes.clientHeight/980; i++) {
            //! This is all just html2canvas stuff
            var srcImg  = canvas;
            var sX      = 0;
            var sY      = 980*i; // start 980 pixels down for every new page
            var sWidth  = 900;
            var sHeight = 980;
            var dX      = 0;
            var dY      = 0;
            var dWidth  = 900;
            var dHeight = 980;

            window.onePageCanvas = document.createElement("canvas");
            onePageCanvas.setAttribute('width', 900);
            onePageCanvas.setAttribute('height', 980);
            var ctx = onePageCanvas.getContext('2d');
            // details on this usage of this function: 
            // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

            // document.body.appendChild(canvas);
            var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

            var width         = onePageCanvas.width;
            var height        = onePageCanvas.clientHeight;

            //! If we're on anything other than the first page,
            // add another page
            if (i > 0) {
                pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
            }
            //! now we declare that we're working on that page
            pdf.setPage(i+1);
            //! now we add content to that page!
            pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width*.62), (height*.62));

        }
        //! after the for loop is finished running, we save the pdf.
        var esPago = $("descargaComprobante").attr("data-esPago");
        if(esPago){
        	
            pdf.save('Comprobante_de_Turno.pdf');
        }else{

            pdf.save('Comprobante_de_Pago.pdf');
        }
    }
  });
}

$(function()
{
      $('#archivo').on('change',function ()
      {
          var filePath = $(this).val();
          console.log(filePath);
      });
});
</script>
</html>

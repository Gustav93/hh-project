<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 15/9/2019
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Service Form</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
    <script type="text/javascript"
            src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"
            type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"
          rel="stylesheet" type="text/css" />

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css"
	rel="stylesheet" />
<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<!-- Bootstrap core CSS -->


<!-- Bootstrap core CSS -->

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!-- Bootstrap core CSS -->

<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript"
	src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

<style>
#main-card {
	width: 80%;
}
</style>
</head>
<body id="body"class="text-center">

	<jsp:include page="../includes/menu.jsp"></jsp:include>

	<div class="d-flex justify-content-center py-4">
		<div id="main-card" class="card text-center">
			<div class="card-header"><spring:message code="detail.turno.title" /></div>
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<c:url var="addUrl" value="/turnos/save" />
				<form:form action="${addUrl}" method="post"
					modelAttribute="turnoFrontDTO">
					<c:choose>
						<c:when test="${msgError}">
							<div class="alert alert-danger" role="alert">${msg}</div>
						</c:when>
					</c:choose>
					<div class="custom-control my-1 mr-sm-2">
						<label for="turnoId" class="col-sm-12 col-form-label"><spring:message code="detail.turno.number" /></label>
						<form:input type="text" path="turnoId" id="turnoId"
							class="form-control" readonly="true" />
					</div>
					<div class="custom-control my-1 mr-sm-2">
						<label for="name" class="col-sm-12 col-form-label"><spring:message code="detail.turno.name" /></label>
						<form:input type="text" path="name" id="name" class="form-control"
							readonly="true" />
					</div>
					<div class="custom-control my-1 mr-sm-2">
						<label for="surname" class="col-sm-12 col-form-label"><spring:message code="detail.turno.lastname" /></label>
						<form:input type="text" path="surname" id="surname"
							class="form-control" readonly="true" />
					</div>
					<div class="custom-control my-1 mr-sm-2">
						<label for="date" class="col-sm-12 col-form-label"><spring:message code="detail.turno.date" /></label>
						<form:input type="text" path="date" id="date" class="form-control"
							readonly="true" />
					</div>
					<div class="custom-control my-1 mr-sm-2">
						<label for="email" class="col-sm-12 col-form-label"><spring:message code="detail.turno.email" /></label>
						<form:input type="text" path="email" id="email"
							class="form-control" readonly="true" />
					</div>

					<div class="custom-control my-1 mr-sm-2">
						<label for="status" class="col-sm-12 col-form-label"><spring:message code="detail.turno.state" /></label>
						<form:select class="form-control" path="status" readonly="true">
							<form:option value="BORRADOR"><spring:message code="detail.turno.state.borrador" /></form:option>
							<form:option value="RESERVADO"><spring:message code="detail.turno.state.reservado" /></form:option>
							<form:option value="FINALIZADO"><spring:message code="detail.turno.state.finalizado" /></form:option>
							<form:option value="AUSENTE"><spring:message code="detail.turno.state.ausente" /></form:option>
							<form:option value="CANCELADO"><spring:message code="detail.turno.state.cancelado" /></form:option>
							<form:option value="RECEPCIONADO"><spring:message code="detail.turno.state.recepcionado" /></form:option>
						</form:select>
					</div>

		
					<div class="custom-control my-1 mr-sm-2">
						<label for="totalPriceDesc" class="col-sm-12 col-form-label"><spring:message
								code="detail.turno.monto" /></label>
						<form:input type="text" path="totalPriceDesc" id="totalPriceDesc"
							class="form-control" readonly="true" />
					</div>

					<div class="custom-control my-1 mr-sm-2">
						<label for="totalPrice" class="col-sm-12 col-form-label"><spring:message
								code="detail.turno.montoDesc" /></label>
						<form:input type="text" path="totalPrice" id="totalPrice"
							class="form-control" readonly="true" />
					</div>


					<div class="custom-control my-1 mr-sm-2">
						<button type="submit" class="btn btn-primary mb-2"><spring:message code="detail.turno.button.save" /></button>
					</div>
				</form:form>
			</div>
		</div>

	</div>

	<div class="d-flex justify-content-center py-4">
		<div class="card text-center" style="width: 80%">
			<div class="card-header"><spring:message code="detail.turno.reservas.title" /></div>
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<table id="table-<spring:message code="buscar.idioma" />"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">
					<thead>
						<tr>
							<th><spring:message code="detail.turno.reservas.servicio" /></th>
							<th><spring:message code="detail.turno.reservas.profesional" /></th>
							<th><spring:message code="detail.turno.reservas.time" /></th>
							<th><spring:message code="detail.turno.reservas.price" /></th>
							<th><spring:message code="detail.turno.reservas.inicio" /></th>
							<th><spring:message code="detail.turno.reservas.fin" /></th>
							<th></th>
						</tr>
					</thead>
					<tbody>

						<c:forEach items="${detalleTurnoList}" var="detalleTurno">
							<tr>
								<td>${detalleTurno.serviceName}</td>
								<td>${detalleTurno.professionalName}</td>
								<td>${detalleTurno.serviceTime}</td>
								<td>${detalleTurno.price}</td>
								<td>${detalleTurno.horaInicio}</td>
								<td>${detalleTurno.horaFin}</td>
								<td><a
									href="<c:url value='/turnos/eliminarreserva?idTurno=${turnoFrontDTO.turnoId}&idReserva=${detalleTurno.id}'/>"
									onclick='return confirm("Estas seguro?")'
									class="btn btn-danger custom-width"><spring:message code="detail.turno.reservas.button.delete" /></a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="well">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target=".bd-example-modal-lg"><spring:message code="detail.turno.reservas.addservice" /></button>
				</div>
				
			</div>
			<%--modal--%>
			<div class="modal fade bd-example-modal-lg" tabindex="-1"
				role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel"><spring:message code="detail.turno.reservas.newreserva" /></h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<c:url var="registrationService" value="/turnos/nuevareserva" />
							<form:form class="form-inline" action="${registrationService}"
								method="post">
								<input type="hidden" id="turnoId" name="turnoId"
									value="${turnoFrontDTO.turnoId}">

								<label class="my-1 mr-2" for="selectService"><spring:message code="detail.turno.reservas.modal.servicio" /></label>
								<select class="my-1 mr-sm-2" id="selectService" name="service"
									required></select>

								<label class="my-1 mr-2" for="selectProfessional"><spring:message code="detail.turno.reservas.modal.profesional" /></label>
								<select class="my-1 mr-sm-2" id="selectProfessional"
									name="professional" required></select>


								<div id="tablahorarios" class="card-body table-responsive">

								</div>


								<div style="left: 27%" align="center" class="custom-control my-1 mr-sm-2">
									<label for="timepicker" class="col-sm-2 col-form-label"><spring:message code="detail.turno.reservas.modal.hour" /></label>
									<input id="timepicker" name="hora" class="form-control"
										required />


									<button type="submit" class="btn btn-primary my-1"><spring:message code="detail.turno.reservas.modal.reservar" /></button>
								</div>




							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="d-flex justify-content-center py-4">

		<div class="card text-center" style="width: 80%">

			<div class="card-header"><spring:message code="detail.turno.promociones.title" /></div>
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<div class="custom-control my-1 mr-sm-2">
					<label for="status" class="col-sm-12 col-form-label"><spring:message code="detail.turno.promociones.disponibles" /></label>
					<c:url var="addUrl" value="/turnos/agregarPromo" />
					<form:form action="${addUrl}" method="get">
						<input type="hidden" id="turnoId" name="turnoId"
							value="${turnoFrontDTO.turnoId}">
						<select class="form-control" name="promoId" required>
							<c:forEach items="${promosDisponibles}" var="promo">
								<option value="" disabled selected><spring:message code="detail.turno.promociones.seleccion" /></option>
								<option value="${promo.id}">${promo.nombre}</option>
							</c:forEach>
						</select>
						<button type="submit" class="btn btn-primary my-1"><spring:message code="detail.turno.promociones.add" /></button>
					</form:form>
				</div>
				<table id="promos"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">
					<thead>
						<tr>
							<th><spring:message code="detail.turno.promociones.name" /></th>
							<th><spring:message code="detail.turno.promociones.descount" /></th>
							<th><spring:message code="detail.turno.promociones.mutiplicarpuntos" /></th>
							<th></th>
						</tr>
					</thead>
					<tbody>

					<c:forEach items="${promosAplicadas}" var="promoAplicada">
						<tr>
							<td>${promoAplicada.nombre}</td>
							<td>${promoAplicada.descuento}%</td>
							<td>x${promoAplicada.multiplicaPuntos}</td>
							<td><a
									href="<c:url value='/turnos/eliminarPromo?turnoId=${turnoFrontDTO.turnoId}&promoId=${promoAplicada.id}'/>"
									onclick='return confirm("Estas seguro?")'
									class="btn btn-danger custom-width">Eliminar</a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<a href="<c:url value='/turnos/obtenerComprobante/${turnoFrontDTO.turnoId}/false'/>"
					class="btn btn-secondary custom-width" style="margin-bottom: 1rem;"><spring:message code="detail.turno.comprobante" /></a>
					
	<a href="<c:url value='/turnos/obtenerComprobante/${turnoFrontDTO.turnoId}/true'/>"
		class="btn btn-secondary custom-width" style="margin-bottom: 1rem;"><spring:message code="detail.turno.comprobante.pago" /></a>

	<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
<script>

    function getUrlgetHorariosDisponibles(profesionalId, fecha){
        return 'http://'+window.location.host+'<c:url value="/turnos/getHorariosDisponibles"/>'+'?profesionalId='+profesionalId+'&fecha='+fecha;
    }
    function getUrlObtenerServicios(){
        return 'http://'+window.location.host+'<c:url value="/turnos/obtenerservicios"/>';
    }

    function getUrlListProfesionalesJSON(servicioId, fecha){
        return 'http://'+window.location.host+'<c:url value="/profesionales/listProfesionalesJSON"/>'+'?servicioId='+servicioId+'&fecha='+fecha;
    }

    var selectProfessional = new SlimSelect({
        select: '#selectProfessional',
        hideSelectedOption: true,
        placeholder: true,
        onChange: (info)=> {
            var ajaxReq = $.ajax(getUrlgetHorariosDisponibles(info.value, $('#date').get(0).value), {
                dataType: 'json',
                timeout: 500,
                success: function (data, status, xhr) {
                	  $('#tablahorarios').empty();

                	  var leyenda1 = '<span class="badge badge-primary">Disponible</span>';
                	  var leyenda2 = '<span class="badge badge-danger">Ocupado</span>';
                	  var leyenda3 = '<span class="badge badge-light">No laboral</span>';

                	  var horarios = '<h5>'+ leyenda1 + leyenda2 +leyenda3+'</h5>';
                	  //var leyenda1 = '<button type="button" class="btn btn-info" disabled>Disponible</button>';

                	  //var espacio = '<div class="p-3 mb-2 bg-white text-dark"></div>'
                	  var content = '<table class="table table-bordered"><tr>';
                	  
                      for(i=0; i<data.length; i++){
                    	if(data[i].disponible){
                    		content += '<td align="center" class="table-primary" width='+'"'+data[i].porcentaje+'"'+'>' + data[i].horaInicio + '-' + data[i].horaFin + '</td>';
                    	}
                    	else if(!data[i].disponible && !data[i].libre ){
                    		content += '<td align="center" class="table-danger" width='+'"'+data[i].porcentaje+'"'+'>' + data[i].horaInicio + '-' + data[i].horaFin + '</td>';
                    	}
                    	else{
                    		content += '<td align="center" class="table-light" width='+'"'+data[i].porcentaje+'"'+'>' + data[i].horaInicio + '-' + data[i].horaFin + '</td>';
                    	}
                      }
                      content += "</tr></table>";
                      $('#tablahorarios').append(horarios);
                  	  //$('#tablahorarios').append(leyenda1);
                  	  //$('#tablahorarios').append(leyenda2);
                  	  //$('#tablahorarios').append(leyenda3);
                  	 // $('#tablahorarios').append(espacio);
                  	  $('#tablahorarios').append(content);

                    }
                ,
                error: function (jqXhr, textStatus, errorMessage) {
                    alert('<spring:message code="detail.turno.error.horarios" />')
                }
        	});
		}
    });
    
    new SlimSelect({
        select: '#selectService',
        hideSelectedOption: true,
        placeholder: true,
        ajax: function (search, callback) {
            // Check search value. If you dont like it callback(false) or callback('Message String')
            // if (search.length < 3) {
            //     callback('Need 3 characters');
            //     return;
            // }

            // Perform your own ajax request here
            fetch(getUrlObtenerServicios())
                .then(function (response) {
                    return response.json();
                })
                .then(function (json) {
                    let data = json;
                    console.log(data);
                    callback(data);
                })
                .catch(function(error) {
                    // If any erros happened send false back through the callback
                    callback(false);
                });
        },
        onChange: (info)=> {
        		$('#tablahorarios').empty();
	            var ajaxReq = $.ajax(getUrlListProfesionalesJSON(info.value, $('#date').get(0).value), {
	                dataType: 'json',
	                timeout: 500,
	                success: function (data, status, xhr) {
	                	data.unshift({text:""});
	                    selectProfessional.setData(data);
	                    
	                },
	                error: function (jqXhr, textStatus, errorMessage) {
	                    alert('<spring:message code="detail.turno.error.profesionales" />')
	                }
	        	});
    	}
    });
        
        $('#timepicker').timepicker({
        	uiLibrary: 'bootstrap4'
       	});

        $(document).ready(function() {
            $('#table-es').DataTable({
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ning\u00fan dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "\u00da�ltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            });

            $('#table-en').DataTable();

        });
       
</script>
</html>

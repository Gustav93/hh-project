<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 15/9/2019
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- <html> -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.user.title"/></title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.user.title"/>
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="user">
                <form:input type="hidden" path="id" id="id"/>
                <spring:message code="form.user.name" var="name"/>
                <div class="form-group row">
                    <label for="firstName" class="col-sm-2 col-form-label">${name}</label>
                    <div class="col-sm-10">
                        <form:input type="text" path="firstName" id="firstName" class="form-control" placeholder="${name}"/>
                    </div>
                </div>
                <spring:message code="form.user.lastname" var="lastname"/>
                <div class="form-group row">
                    <label for="lastName" class="col-sm-2 col-form-label">${lastname}</label>
                    <div class="col-sm-10">
                        <form:input type="text" path="lastName" id="lastName" class="form-control" placeholder="${lastname}"/>
                    </div>
                </div>
                <spring:message code="form.user.id" var="id"/>
                <div class="form-group row">
                    <label for="ssoId" class="col-sm-2 col-form-label">${id}</label>
                    <div class="col-sm-10">
                        <c:choose>
                            <c:when test="${edit}">
                                <form:input type="text" path="ssoId" id="ssoId" class="form-control" disabled="true" placeholder="${id}"/>
                            </c:when>
                            <c:otherwise>
                                <form:input type="text" path="ssoId" id="ssoId" class="form-control" placeholder="${id}"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <spring:message code="form.user.password" var="password"/>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">${password}</label>
                    <div class="col-sm-10">
                        <form:input type="password" path="password" id="password" class="form-control" placeholder="${password}"/>
                    </div>
                </div>

				<spring:message code="form.user.email" var="email"/>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">${email}</label>
                    <div class="col-sm-10">
                        <form:input type="text" path="email" id="email" class="form-control" placeholder="${email}"/>
                    </div>
                </div>
                
                <spring:message code="form.user.role" var="email"/>
                <div class="form-group row">
                    <label for="userProfiles" class="col-sm-2 col-form-label">${role}</label>
                    <div class="col-sm-10">
                        <form:select path="userProfiles" items="${roles}" multiple="false" itemValue="id" itemLabel="type" class="form-control" />
                        <!-- </select> -->
                    </div>
                </div>
                <div class="form-group row">
                    <label for="estado" class="col-sm-2 col-form-label"><spring:message code="form.user.state"/></label>
                    <div class="col-sm-10">
                        <form:select path="estado" multiple="false" itemValue="id" itemLabel="type" class="form-control">
                            <form:option value="ACTIVO"><spring:message code="form.user.state.active"/></form:option>
                            <form:option value="INACTIVO"><spring:message code="form.user.state.inactive"/></form:option>
                        </form:select>
                        <!-- </select> -->
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label"><spring:message code="form.user.inactivesince"/></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="fechaBaja" id="date" class="form-control" disabled="true"/>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="<spring:message code="form.user.button.update"/>" class="btn btn-primary"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="<spring:message code="form.user.button.add"/>" class="btn btn-primary"/>
                    </c:otherwise>
                </c:choose>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
<!-- </html> -->

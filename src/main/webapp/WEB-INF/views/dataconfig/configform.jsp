<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.config.title"/></title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <spring:url value="/sucursales/edit" var="urlEdit" />
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.config.title"/>
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="config">

                <form:input type="hidden" path="id" id="id"/>
                <form:input type="hidden" path="property" id="property"/>
                <div class="form-group row">
                    <label for="property" class="col-sm-2 col-form-label"><spring:message code="form.config.name"/></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="name" id="name" class="form-control" required="required"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="value" class="col-sm-2 col-form-label"><spring:message code="form.config.value"/></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="value" id="value" class="form-control" required="required"/>
                    </div>
                </div>
                <input type="submit" value="<spring:message code="form.config.button.edit"/>" formaction="<spring:url value="/config/edit"/>"class="btn btn-primary"/>
            </form:form>
        </div>
    </div>
</div>

<jsp:include page="../includes/footer.jsp"></jsp:include>

</body>
</html>
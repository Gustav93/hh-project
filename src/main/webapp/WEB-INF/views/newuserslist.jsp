<%--
  Created by IntelliJ IDEA.
  User: Gustav
  Date: 15/9/2019
  Time: 11:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><spring:message code="userslist.title"/></title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<jsp:include page="includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header"><spring:message code="userslist.name"/></div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <table id="table-<spring:message code="buscar.idioma" />" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th><spring:message code="userslist.table.header.name"/></th>
                    <th><spring:message code="userslist.table.header.surname"/></th>
                    <th><spring:message code="userslist.table.header.email"/></th>
                    <th><spring:message code="userslist.table.header.ssoid"/></th>
                    <th><spring:message code="userslist.table.header.status"/></th>
                    <th><spring:message code="userslist.table.header.removedate"/></th>
<%--                    <sec:authorize access="hasRole('ADMIN')">--%>
<%--                        <th></th>--%>
<%--                        <th></th>--%>
<%--                    </sec:authorize>--%>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.email}</td>
                        <td>${user.ssoId}</td>
                        <%-- <td>${user.estado}</td> --%>
                        <c:choose>
								<c:when test="${user.estado eq 'ACTIVO'}">
									<td><spring:message code="userslist.table.header.status.active" /></td>
								</c:when>
								<c:otherwise>
									<td><spring:message code="userslist.table.header.status.inactive" /></td>
								</c:otherwise>
						</c:choose>
                        <td>${cliente.fechaDeBaja}</td> 
<%--                        <sec:authorize access="hasRole('ADMIN')">--%>
<%--                            <td>--%>
<%--                                <div class="d-flex justify-content-center">--%>
<%--                                    <a href="<c:url value='/edit-user-${user.ssoId}' />" class="btn btn-success btn-sm center-block"><spring:message code="userslist.table.button.edit"/></a>--%>
<%--                                </div>--%>
<%--                            </td>--%>
<%--                            <td>--%>
<%--                                <div class="d-flex justify-content-center">--%>
<%--                                    <a href="<c:url value='/delete-user-${user.ssoId}' />" class="btn btn-danger btn-sm"><spring:message code="userslist.table.button.delete"/></a>--%>
<%--                                </div>--%>
<%--                            </td>--%>
<%--                        </sec:authorize>--%>
                            <td>
                                <div class="d-flex justify-content-center">
                                    <a href="<c:url value='/user/edit-user-${user.ssoId}' />" class="btn btn-success btn-sm center-block"><spring:message code="userslist.table.button.edit"/></a>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex justify-content-center">
                                    <a href="<c:url value='/user/delete-user-${user.ssoId}' />" onclick='return confirm("<spring:message code="userslist.table.button.seguro"/>")' class="btn btn-danger btn-sm"><spring:message code="userslist.table.button.delete"/></a>
                                </div>
                            </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <sec:authorize access="hasRole('ADMIN')">
                <div class="well">
                	<a href="<c:url value='/user/newuser' />" class="btn btn-primary"><spring:message code="userslist.button.new"/></a>
                    <%-- <a href="<c:url value='/user/newuser' />"><spring:message code="userslist.table.button.seguro"/></a> --%>
                </div>
            </sec:authorize>
<%--                <div class="well">--%>
<%--                    <a href="<c:url value='/user/newuser' />">Add New User</a>--%>
<%--                </div>--%>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#table-es').DataTable({
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning\u00fan dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "\u00daltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }

        });

        $('#table-en').DataTable();
    } );
</script>
</body>
</html>

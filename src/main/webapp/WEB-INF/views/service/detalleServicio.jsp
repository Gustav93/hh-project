<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="detail.service.title" /></title>
    
    <spring:url value="/servicios/edit" var="urlEdit" />
    <spring:url value="/servicios/list" var="urlList" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>

<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="detail.service.title" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="servicioDTO"  >
                <dl class="row">
                	<dt class="col-sm-3"><spring:message code="detail.service.name" /></dt>
  					<dd class="col-sm-9">${servicioDTO.nombre}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.service.price" /></dt>
  					<dd class="col-sm-9">$ ${servicioDTO.precio}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.service.tpd" /></dt>
  					<dd class="col-sm-9">${servicioDTO.tiempoPromedioDeDuracion} <spring:message code="detail.service.minutes" /></dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.service.fechaBaja" /></dt>
  					<dd class="col-sm-9">${servicioDTO.fechaDeBaja}</dd>
  					
  					<dt class="col-sm-3"><spring:message code="detail.service.state" /></dt>
  					<c:choose>
						<c:when test="${servicioDTO.estado eq 'ACTIVO'}">
  							<dd class="col-sm-9"><spring:message code="detail.service.state.active" /></dd>
  						</c:when>
						<c:otherwise>
							<dd class="col-sm-9"><spring:message code="detail.service.state.inactive" /></dd>
						</c:otherwise>
					</c:choose>
  					<dt class="col-sm-3"><spring:message code="detail.service.description" /></dt>
  					<dd class="col-sm-9">${servicioDTO.descripcion}</dd>
  					
                </dl>
                
                			
			<a href="${urlEdit}/${servicioDTO.id}" class="btn btn-success"><spring:message code="detail.service.button.edit" /></a>
			<a href="${urlList}" class="btn btn-primary"><spring:message code="detail.service.button.return" /></a>
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>

</body>
</html>
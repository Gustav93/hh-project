<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><spring:message code="servicelist.name" /></title>

	<spring:url value="/servicios/create" var="urlCreate" />
	<spring:url value="/servicios/delete" var="urlDelete" />
	<spring:url value="/servicios/edit" var="urlEdit" />
	<spring:url value="/servicios/detail" var="urlDetail" />
	<spring:url value="/servcios" var="urlServicios" />

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

<link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
</head>

<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>
	<div class="d-flex justify-content-center py-4">
		<div id="main-card" class="card text-center">

			<div class="card-header">
				<spring:message code="servicelist.name" />
			</div>
			<c:if test="${mensaje!=null}">
      	
      	<div class='alert alert-success' role="alert">mensaje: ${mensaje}</div>
      	
      </c:if> 
			<div class="card-body table-responsive"
				style="text-align: left !important;">
				<table id="users-table-<spring:message code="buscar.idioma" />"
					class="table table-striped table-bordered dt-responsive nowrap"
					style="width: 100%">

					<thead>
						<tr>
							<th><spring:message code="servicelist.table.header.name" /></th>
							<th><spring:message code="servicelist.table.header.price" /></th>
							<th><spring:message code="servicelist.table.header.timeprom" /></th>
							<th><spring:message code="servicelist.table.header.state" /></th>
							<%-- <th><spring:message code="servicelist.table.header.fechaBaja" /></th> --%>
							<%-- <th><spring:message code="servicelist.table.header.description" /></th> --%>
							<th width="100"></th>
							<th width="100"></th>
							<th width="100"></th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${servicios}" var="servicio">
							<tr>
								<td>${servicio.nombre}</td>
								<td>$ ${servicio.precio}</td>
								<td>${servicio.tiempoPromedioDeDuracion} <spring:message code="servicelist.table.button.minutes" /></td>
								<c:choose>
								<c:when test="${servicio.estado eq 'ACTIVO'}">
									<td><spring:message code="list.cliente.state.active" /></td>
								</c:when>
								<c:otherwise>
									<td><spring:message code="list.cliente.state.inactive" /></td>
								</c:otherwise>
								</c:choose>
								<%-- <td>${servicio.fechaDeBaja}</td> --%>
								<%-- <td>${servicio.descripcion}</td> --%>
								<td><a href="${urlDetail}/${servicio.id}" class="btn btn-info"><spring:message code="servicelist.table.button.detail" /></a></td>
								<td><a href="<c:url value='/servicios/edit/${servicio.id}'/>" class="btn btn-success custom-width"><spring:message code="servicelist.table.button.edit" /></a></td>
								<c:choose>
						<c:when test="${servicio.fechaDeBaja eq null}">
							<td><a href="<c:url value='/servicios/delete/${servicio.id}'/>" onclick='return confirm("Estas seguro?")' class="btn btn-danger custom-width"><spring:message code="servicelist.table.button.delete" /></a></td>
						</c:when>
						<c:otherwise>
							<td><a href="<c:url value='/servicios/active/${servicio.id}'/>" class="btn btn-primary custom-width"><spring:message code="servicelist.table.button.active" /></a></td>
						</c:otherwise>
					</c:choose>
								
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div class="well">
					<%-- <a href="${urlCreate}">Add New Service</a> --%>
					<a href="${urlCreate}" class="btn btn-primary"><spring:message code="servicelist.table.button.new" /></a>
				</div>
			</div>
		</div>


	</div>

	<script>
		$(document)
				.ready(
						function() {
							$('#users-table-es')
									.DataTable(
											{
												"language" : {
													"sProcessing" : "Procesando...",
													"sLengthMenu" : "Mostrar _MENU_ registros",
													"sZeroRecords" : "No se encontraron resultados",
													"sEmptyTable" : "Ning\u00fan dato disponible en esta tabla",
													"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
													"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
													"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
													"sInfoPostFix" : "",
													"sSearch" : "Buscar:",
													"sUrl" : "",
													"sInfoThousands" : ",",
													"sLoadingRecords" : "Cargando...",
													"oPaginate" : {
														"sFirst" : "Primero",
														"sLast" : "\u00daltimo",
														"sNext" : "Siguiente",
														"sPrevious" : "Anterior"
													},
													"oAria" : {
														"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
														"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
													}
												}

											});

							$('#users-table-en').DataTable();
						});
	</script>
	<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
</html>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><spring:message code="form.pago.title" /></title>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.0/slimselect.min.css" rel="stylesheet"/>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

<style>
#main-card {
	width: 80%;
}
</style>
</head>

<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.pago.title" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            
            <c:url var="addUrl" value="/pagos/create"/>
            <c:url var="imprimirUrl" value="/pagos/imprimir"/>
            <form:form action="${addUrl}" method="post" modelAttribute="pagoDTO">
                <div  class="custom-control my-1 mr-sm-2">
                    <label for="turnoId" class="col-sm-12 col-form-label"><spring:message code="form.pago.number" /></label>
                    <form:input type="text" path="id" id="id" class="form-control" readonly="true"/>
                  
                </div>
                
                <div  class="custom-control my-1 mr-sm-2">
                    <label for="name" class="col-sm-12 col-form-label"><spring:message code="form.pago.total" /></label>
                    <input value="${pagoDTO.montoTotal}" readonly
							type="number" id="montoTotal" class="form-control">
                </div>

					<div class="well">
                        <a href="<c:url value='/turnos/obtenerComprobante/${pagoDTO.idTurno}/true'/>"
                           class="btn btn-secondary custom-width" style="margin-bottom: 1rem;"><spring:message code="detail.turno.comprobante.pago" /></a>
                    </div>




				</form:form>

				<c:choose>
					<c:when test="${promo}">
						<div class="alert alert-success" role="alert">${msgPromo}</div>
					</c:when>
				</c:choose>
				
				<c:choose>
					<c:when test="${error}">
						<div class="alert alert-danger" role="alert">${msgError}</div>
					</c:when>
				</c:choose>
				
			</div>
    </div>

</div>
<div class="d-flex justify-content-center">
    <div  class="card text-center" style="width: 80%">
        <div class="card-header">
            <spring:message code="form.pago.detail" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <table id="detalle-turno" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th><spring:message code="form.pago.detail.monto" /></th>
                    <th><spring:message code="form.pago.detail.type" /></th>
                    <th><spring:message code="form.pago.detail.date" /></th>
                    
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${pagoDTO.detallesDePago}" var="detallePago">
                    <tr>
                        <td>${detallePago.monto}</td>
                        <td>${detallePago.tipo}</td>
                        <td>${detallePago.fecha}</td>
                    </tr>
                </c:forEach>
                
                </tbody>
            </table>
            
            <div class="well">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><spring:message code="form.pago.button.add" /></button>
            </div>
            

            
        </div>
        <%--modal--%>
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><spring:message code="form.pago.modal" /></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        <c:url var="registrarDetallePago" value="/pagos/registrarDetallePago"/>
                        <form:form  class="form-inline" action="${registrarDetallePago}" method="post" modelAttribute = "detallePagoDTO">
<%--                        <form class="form-inline" method="get">--%>
                            
                            <input hidden="true" name = "idTurno" value = "${pagoDTO.idTurno}">
                            
                            <input hidden="true" name = "idPago" value = "${pagoDTO.id}">
                           
                   		    <label class="my-1 mr-2" for="inlineFormCustomInputPref"><spring:message code="form.pago.modal.point" /></label>
							<input class="my-1 mr-sm-2" id = "inlineFormCustomSelectPref" contenteditable="false" readonly="readonly" value="${pagoDTO.puntosAcumulados}">
                        
                        
							<label class="my-1 mr-2" for="inlineFormCustomSelectPref2"><spring:message code="form.pago.modal.type" /></label>
							<form:select  class="my-1 mr-sm-2" id="inlineFormCustomSelectPref2" path = "tipo" items = "${tiposDePago}"></form:select>
							
							<label class="my-1 mr-2" for="inlineFormCustomInputPref3">Monto</label>
							<form:input  class="my-1 mr-sm-2"  path = "monto" id = "inlineFormCustomSelectPref3"></form:input>
							
				
							
								
                            <button type="submit" class="btn btn-primary my-1"><spring:message code="form.pago.modal.button.pagar" /></button>
                        </form:form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>

</html>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.cliente" /></title>
    <spring:message code="form.cliente.name" var="clientename"/>
    <spring:message code="form.cliente.lastname" var="clientelastname"/>
    <spring:message code="form.cliente.phone" var="clientephone"/>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">

    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">
<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.cliente" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="cliente">
                
                <form:input type="hidden" path="id" id="id"/>
                
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.cliente.name" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="nombre" id="nombre" class="form-control" placeholder="${clientename}" required="required" />
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="apellido" class="col-sm-2 col-form-label"><spring:message code="form.cliente.lastname" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="apellido" id="apellido" class="form-control" placeholder="${clientelastname}" required="required"/>
                    </div>
                </div>
                
                 <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label"><spring:message code="form.cliente.email" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="email" id="email" class="form-control" placeholder="Email" required="required"/>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="Telefono" class="col-sm-2 col-form-label"><spring:message code="form.cliente.phone" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="Telefono" id="Telefono" class="form-control" placeholder="${clientephone}" required="required"/>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${edit}">
               <div class="form-group row">
                    <label for="estado" class="col-sm-2 col-form-label"><spring:message code="form.cliente.state" /></label>
                    <div class="col-sm-10">
                        <form:select class="form-control" path="estado" id="estado">
                            <form:option value="ACTIVO"><spring:message code="form.cliente.option.active" /></form:option>
                           <form:option value="INACTIVO"><spring:message code="form.cliente.option.inactive" /></form:option>
                        </form:select>
                    </div>
                </div>
                </c:when>
                </c:choose>
                
               <div class="form-group row">
                    <label for="puntosAcumulados" class="col-sm-2 col-form-label"><spring:message code="form.cliente.point" /></label>
                    <div class="col-sm-10">
                    	<form:input type="number" path="puntosAcumulados" id="puntosAcumulados" class="form-control" placeholder="Accumulated points"/>
                    </div>
                </div>
                
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="<spring:message code="form.cliente.button.update" />" class="btn btn-primary"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="<spring:message code="form.cliente.button.add" />" class="btn btn-primary"/>
                    </c:otherwise>
                </c:choose>
                
            </form:form>
        </div>
    </div>
</div>
<jsp:include page="../includes/footer.jsp"></jsp:include>
</body>
</html>
<%@ page contentType="text/html" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><spring:message code="form.sucursal" /></title>
    <spring:message code="form.sucursal.name" var="sucursalName" />
    <spring:message code="form.sucursal.location" var="sucursalLocation" />

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <link rel="stylesheet" href="<c:url value='/static/css/bootnavbar.css' />">
	
	<spring:url value="/sucursales/edit" var="urlEdit" />
	
    <style>
        #main-card{
            width: 80%;
        }
    </style>
</head>
<body class="text-center">

<jsp:include page="../includes/menu.jsp"></jsp:include>

<div class="d-flex justify-content-center py-4">
    <div id="main-card" class="card text-center">
        <div class="card-header">
            <spring:message code="form.sucursal" />
        </div>
        <div class="card-body table-responsive"  style="text-align: left !important;">
            <form:form method="POST" modelAttribute="sucursalDTO">
                
                <form:input type="hidden" path="id" id="id"/>
                
                <div class="form-group row">
                    <label for="nombre" class="col-sm-2 col-form-label"><spring:message code="form.sucursal.name" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="nombre" id="nombre" class="form-control" placeholder="${sucursalName}" required="required" />
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="localidad" class="col-sm-2 col-form-label"><spring:message code="form.sucursal.location" /></label>
                    <div class="col-sm-10">
                        <form:input type="text" path="localidad" id="localidad" class="form-control" placeholder="${sucursalLocation}" required="required"/>
                    </div>
                </div>
                
      
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="<spring:message code="form.sucursal.button.update" />" formaction="${urlEdit}/${sucursalDTO.id}"class="btn btn-primary"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="<spring:message code="form.sucursal.button.add" />" class="btn btn-primary"/>
                    </c:otherwise>
                </c:choose>
                
            </form:form>
        </div>
    </div>
</div>

<jsp:include page="../includes/footer.jsp"></jsp:include>

</body>
</html>
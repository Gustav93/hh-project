package com.hairandhead.project.pago.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.pago.dao.DetallePagoDao;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;

@Transactional
@Service("DetallePagoService")
public class DetallePagoServiceImpl implements DetallePagoService {
	
	@Autowired
	DetallePagoDao detallePagoDao;
	
	@Override
	public DetallePago findById(int id) {
		return detallePagoDao.findById(id);
	}

	@Override
	public void save(DetallePago detallePago) {
		detallePagoDao.save(detallePago);
	}

	@Override
	public void delete(DetallePago detallePago) {
		detallePagoDao.delete(detallePago);
	}

	@Override
	public void update(DetallePago detallePago) {
		detallePagoDao.update(detallePago);
	}

	@Override
	public void convertToDTO(DetallePago detallePago, DetallePagoDTO detallePagoDTO) {
		detallePagoDTO.setFecha(detallePago.getFecha().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
		detallePagoDTO.setId(detallePago.getId());
		detallePagoDTO.setMonto(detallePago.getMonto().toString());
		detallePagoDTO.setTipo(detallePago.getTipo().toString());
	}

	@Override
	public void convertToDetallePago(DetallePago detallePago, DetallePagoDTO detallePagoDTO) {
		try {
			detallePago.setMonto(new BigDecimal(detallePagoDTO.getMonto()));
		}
		catch(Exception e) {
			//No le pude poner el monto
			detallePago.setMonto(new BigDecimal(0));
			System.out.println(e);
		}
		try{
			detallePago.setTipo(TipoDetallePago.valueOf(detallePagoDTO.getTipo()));
		}
		catch(Exception e) {
			//No le pude poner el tipo
			System.out.println(e);
			detallePago.setTipo(TipoDetallePago.EFECTIVO);
		}
		detallePago.setFecha(LocalDateTime.now());
	}
	
	@Override
	public List<DetallePagoDTO> convertDetallePagoToDTOList(Pago pago) {
		List<DetallePagoDTO> detallesPago = new ArrayList<>();
		for(DetallePago detallePago : pago.getDetallePago()) {
			DetallePagoDTO detallePagoDTO = new DetallePagoDTO();
			convertToDTO(detallePago, detallePagoDTO);
			detallesPago.add(detallePagoDTO);
			
		}
		return detallesPago;
	}

}

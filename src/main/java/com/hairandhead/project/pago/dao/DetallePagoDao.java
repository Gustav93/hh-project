package com.hairandhead.project.pago.dao;

import java.util.List;

import com.hairandhead.project.pago.model.DetallePago;

public interface DetallePagoDao {
	
    DetallePago findById(int id);

    void save(DetallePago detallePago);

    void delete(DetallePago detallePago);

    void update(DetallePago detallePago);


}

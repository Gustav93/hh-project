package com.hairandhead.project.cliente.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.model.EstadoCliente;
import com.hairandhead.project.cliente.service.ClienteService;

@Controller
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public String list(ModelMap model) {
		model.addAttribute("clientes", this.clienteService.findAllClientes());
		return "clientes/listClientes";
	}

	@RequestMapping(value = { "/create" }, method = RequestMethod.GET)
	public String create(ModelMap model) {
		model.addAttribute("cliente", new Cliente());
		model.addAttribute("edit", false);
		return "clientes/formCliente";
	}

	@RequestMapping(value = { "/create" }, method = RequestMethod.POST)
	public String save(@Valid Cliente cliente, ModelMap model, BindingResult result) {

		if (result.hasErrors()) {
			return "clientes/formCliente";
		}

		cliente.setFechaDeBaja(cliente.getEstado().equals(EstadoCliente.INACTIVO) ? LocalDateTime.now() : null);
		cliente.setEstado(EstadoCliente.ACTIVO);
		this.clienteService.saveCliente(cliente);
		model.addAttribute("mensaje",
				"Cliente: " + cliente.getNombre() + " " + cliente.getApellido() + ", Fue guardado con exito!!");
		return "redirect:/clientes/list";
	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap model) {
		model.addAttribute("cliente", this.clienteService.findClienteById(id));
		model.addAttribute("edit", true);
		return "clientes/formCliente";
	}

	@RequestMapping(value = { "/edit/{id}" }, method = RequestMethod.POST)
	public String update(@Valid Cliente cliente, ModelMap model, BindingResult result) {
		if (result.hasErrors()) {
			return "clientes/formCliente";
		}

		cliente.setFechaDeBaja(cliente.getEstado().equals(EstadoCliente.INACTIVO) ? LocalDateTime.now() : null);
		this.clienteService.updateCliente(cliente);
		model.addAttribute("mensaje",
				"Cliente: " + cliente.getNombre() + " " + cliente.getApellido() + ", Fue Actualizado con exito!!");
		return "redirect:/clientes/list";
	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id, ModelMap model) {
		Cliente c = this.clienteService.findClienteById(id);
		c.setEstado(EstadoCliente.INACTIVO);
		c.setFechaDeBaja(LocalDateTime.now());
		this.clienteService.updateCliente(c);
		model.addAttribute("mensaje", "Cliente: " + c.getNombre() + " " + c.getApellido() + ", Fue dado de Baja!!");
		return "redirect:/clientes/list";
	}

	@RequestMapping(value = { "/active/{id}" }, method = RequestMethod.GET)
	public String active(@PathVariable("id") int id, ModelMap model) {
		Cliente c = this.clienteService.findClienteById(id);
		c.setEstado(EstadoCliente.ACTIVO);
		c.setFechaDeBaja(null);
		this.clienteService.updateCliente(c);
		model.addAttribute("mensaje", "Cliente: " + c.getNombre() + " " + c.getApellido() + ", Fue dado de Alta!!");
		return "redirect:/clientes/list";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}

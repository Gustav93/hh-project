package com.hairandhead.project.cliente.service;

import java.util.List;

import com.hairandhead.project.cliente.model.Cliente;

public interface ClienteService {

	Cliente findClienteById(int id);
	Cliente findClienteByMail(String mail);
	void saveCliente(Cliente c);
	void updateCliente(Cliente c);
	List<Cliente> findAllClientes();
	public List<Cliente> findAllClientesActivos();
}

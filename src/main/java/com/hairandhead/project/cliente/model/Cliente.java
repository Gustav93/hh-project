package com.hairandhead.project.cliente.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="Clientes")
public class Cliente{
	
	

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Size(min=3, max=50)
	@Column(name="Nombre", nullable=false)
	private String nombre;
	
	@Size(min=3, max=50)
	@Column(name="Apellido", nullable=false)
	private String apellido;
	
	@Size(min=3, max=50)
	@Column(name="Email", nullable=false)
	private String email;
	
	@Size(min=3, max=50)
	@Column(name="Telefono", nullable=false)
	private String telefono;
	
	@Column(name = "PuntosAcumulados")
	private double puntosAcumulados;
	
	@Column(name = "FechaDeBaja")
	private LocalDateTime fechaDeBaja;
	
	@Column(name = "Estado")
	private EstadoCliente estado;

	public Cliente() {
		this.id = null;
		this.nombre = "";
		this.apellido = "";
		this.email = "";
		this.telefono = "";
		this.puntosAcumulados = 0.0;
		this.estado = EstadoCliente.ACTIVO;
		this.fechaDeBaja = null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public double getPuntosAcumulados() {
		return puntosAcumulados;
	}

	public void setPuntosAcumulados(double puntosAcumulados) {
		this.puntosAcumulados = puntosAcumulados;
	}

	public LocalDateTime getFechaDeBaja() {
		return fechaDeBaja;
	}

	public void setFechaDeBaja(LocalDateTime fechaDeBaja) {
		this.fechaDeBaja = fechaDeBaja;
	}

	public EstadoCliente getEstado() {
		return estado;
	}

	public void setEstado(EstadoCliente estado) {
		this.estado = estado;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Cliente cliente = (Cliente) o;
		return Double.compare(cliente.puntosAcumulados, puntosAcumulados) == 0 &&
				Objects.equals(id, cliente.id) &&
				Objects.equals(nombre, cliente.nombre) &&
				Objects.equals(apellido, cliente.apellido) &&
				Objects.equals(email, cliente.email) &&
				Objects.equals(telefono, cliente.telefono) &&
				Objects.equals(fechaDeBaja, cliente.fechaDeBaja) &&
				estado == cliente.estado;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nombre, apellido, email, telefono, puntosAcumulados, fechaDeBaja, estado);
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email
				+ ", telefono=" + telefono + ", puntosAcumulados=" + puntosAcumulados + ", fechaDeBaja=" + fechaDeBaja
				+ ", estado=" + estado + "]";
	}

}

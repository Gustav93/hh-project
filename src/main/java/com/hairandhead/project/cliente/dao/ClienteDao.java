package com.hairandhead.project.cliente.dao;

import java.util.List;

import com.hairandhead.project.cliente.model.Cliente;

public interface ClienteDao {

	Cliente findById(int id);
	Cliente findByMail(String email);
	public void save(Cliente c);
	public List<Cliente> findAll();
	public List<Cliente> findAllClientesActivos();
	public void update(Cliente c);
}

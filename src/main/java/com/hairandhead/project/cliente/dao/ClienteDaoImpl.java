package com.hairandhead.project.cliente.dao;

import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.model.EstadoCliente;
import com.hairandhead.project.profesional.model.Profesional;

@Repository("clienteDao")
public class ClienteDaoImpl extends AbstractDao<Integer,Cliente> implements ClienteDao {

	@Override
	public Cliente findById(int id) {
		return super.getByKey(id);
	}
	
	@Override
	public Cliente findByMail(String email) {
		 Criteria criteria = super.createEntityCriteria();
	     criteria.add(Restrictions.eq("email", email));
	     return (Cliente) criteria.uniqueResult();
	}

	@Override
	public void save(Cliente c) {
		super.persist(c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> findAll() {
		Criteria criteria = createEntityCriteria();
		return (List<Cliente>) criteria.list();
	}

	@Override
	public void update(Cliente c) {
		super.update(c);
	}

	@Override
	public List<Cliente> findAllClientesActivos() {
		List<Cliente> ret = new ArrayList<Cliente>();
		for(Cliente c : findAll()) {
			if(c.getEstado().equals(EstadoCliente.ACTIVO)) {
				ret.add(c);
			}
		}
		return ret;
	}

}

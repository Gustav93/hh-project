package com.hairandhead.project.servicio.dto;

public class ServicioDTO {

    private Integer id;

    private String nombre;

    private String descripcion;

    private String precio;

    private String tiempoPromedioDeDuracion;

    private String estado;

    private String fechaDeBaja;

    public ServicioDTO() {
        this.id = null;
        this.nombre = "";
        this.descripcion = "";
        this.precio = "";
        this.tiempoPromedioDeDuracion = "";
        this.estado = "";
        this.fechaDeBaja = "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getTiempoPromedioDeDuracion() {
        return tiempoPromedioDeDuracion;
    }

    public void setTiempoPromedioDeDuracion(String tiempoPromedioDeDuracion) {
        this.tiempoPromedioDeDuracion = tiempoPromedioDeDuracion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaDeBaja() {
        return fechaDeBaja;
    }

    public void setFechaDeBaja(String fechaDeBaja) {
        this.fechaDeBaja = fechaDeBaja;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fechaDeBaja == null) ? 0 : fechaDeBaja.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((precio == null) ? 0 : precio.hashCode());
		result = prime * result + ((tiempoPromedioDeDuracion == null) ? 0 : tiempoPromedioDeDuracion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicioDTO other = (ServicioDTO) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (fechaDeBaja == null) {
			if (other.fechaDeBaja != null)
				return false;
		} else if (!fechaDeBaja.equals(other.fechaDeBaja))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (precio == null) {
			if (other.precio != null)
				return false;
		} else if (!precio.equals(other.precio))
			return false;
		if (tiempoPromedioDeDuracion == null) {
			if (other.tiempoPromedioDeDuracion != null)
				return false;
		} else if (!tiempoPromedioDeDuracion.equals(other.tiempoPromedioDeDuracion))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ServicioDTO [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", tiempoPromedioDeDuracion=" + tiempoPromedioDeDuracion + ", estado=" + estado + ", fechaDeBaja="
				+ fechaDeBaja + "]";
	}
    
    
}

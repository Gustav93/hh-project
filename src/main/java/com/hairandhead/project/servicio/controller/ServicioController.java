package com.hairandhead.project.servicio.controller;



import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.hairandhead.project.servicio.dto.ServicioDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;

@Controller
@RequestMapping("/servicios")
public class ServicioController {
	
	@Autowired
	private ServicioService servicioService;
	
	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public String list(ModelMap model) 
	{
		model.addAttribute("servicios", this.findAllServiciosDTO());
		return "service/listServices";
	}
	
	@RequestMapping(value = { "/create" }, method = RequestMethod.GET)
	public String create(ModelMap model)
	{
		ServicioDTO servicioDTO = new ServicioDTO();
		model.addAttribute("serviceDTO", servicioDTO);
		model.addAttribute("edit", false);
		return "service/formservice";
	}


    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public String save(@Valid ServicioDTO serviceDTO, BindingResult result, ModelMap model){

		if(result.hasErrors()) {
			return "servicice/formservice";
		}

		Servicio servicio = new Servicio();
		servicioService.convertToServicio(serviceDTO, servicio);
		servicioService.saveServicio(servicio);
		model.addAttribute("mensaje", "El servicio "+ servicio.getNombre() + " agregado con Exito");
        return "redirect:/servicios/list";
    }
	
	@RequestMapping(value= { "/edit/{id}" }, method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap model)
	{
		
		ServicioDTO dto = new ServicioDTO();
		servicioService.convertToDTO(servicioService.findServicioById(id),dto);
//		model.addAttribute("servicio", this.service.findServicioById(id));
		model.addAttribute("serviceDTO", dto);
		model.addAttribute("edit", true);
		return "service/formservice";
	}

	@RequestMapping(value= { "/edit/{id}" }, method = RequestMethod.POST)
	public String updateServicio(@Valid ServicioDTO serviceDTO, BindingResult result, ModelMap model)
	{
		if(result.hasErrors()) {
			return "servicice/formservice";
		}
		
		Servicio servicio = new Servicio();
		servicioService.convertToServicio(serviceDTO, servicio);
		servicioService.updateServicio(servicio);
		return "redirect:/servicios/list";
	}
	
	@RequestMapping(value= { "/delete/{id}" }, method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id, ModelMap model)
	{
		Servicio servicio = this.servicioService.findServicioById(id);
		servicio.setEstado(EstadoServicio.INACTIVO);
		LocalDateTime fechaHoraActual = LocalDateTime.now();
		servicio.setFechaDeBaja(fechaHoraActual);
		this.servicioService.updateServicio(servicio);
		model.addAttribute("mensaje", "El servicio fue eliminado");
		return "redirect:/servicios/list";
	}
	
	@RequestMapping(value= { "/active/{id}" }, method = RequestMethod.GET)
	public String active(@PathVariable("id") int id, ModelMap model)
	{
		Servicio servicio = this.servicioService.findServicioById(id);
		servicio.setEstado(EstadoServicio.ACTIVO);
		servicio.setFechaDeBaja(null);
		this.servicioService.updateServicio(servicio);
		model.addAttribute("mensaje", "El servicio fue activado");
		return "redirect:/servicios/list";
	}
	
	@RequestMapping(value= { "/detail/{id}" }, method = RequestMethod.GET)
	public String detail(@PathVariable("id") int id, ModelMap model)
	{
		Servicio s = this.servicioService.findServicioById(id);
		ServicioDTO dto = new ServicioDTO();
		this.servicioService.convertToDTO(s, dto);
		model.addAttribute("servicioDTO", dto);
		return "service/detalleServicio";
	}
	
	private List<ServicioDTO> findAllServiciosDTO()
	{
		List<ServicioDTO> lista = new ArrayList<>(); 
		for (Servicio s : this.servicioService.findAllServicios()) {
			ServicioDTO dto = new ServicioDTO();
			this.servicioService.convertToDTO(s, dto);
			lista.add(dto);
		}
		return lista;
	}
}

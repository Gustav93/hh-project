package com.hairandhead.project.servicio.service;

import java.util.List;

import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;

public interface ServicioService {

	Servicio findServicioById(int id);
	void saveServicio(Servicio servicio);
	void updateServicio(Servicio servicio);
	List<Servicio> findAllServicios();
	
	public List<Servicio> findAllServiciosActivos();

	void convertToDTO(Servicio source, ServicioDTO target);

	ServicioDTO convertToDTO(Servicio source);

	void convertToServicio(ServicioDTO source, Servicio target);
}

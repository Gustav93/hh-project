package com.hairandhead.project.config.dao;

import com.hairandhead.project.config.model.DataConfig;

import java.util.List;

public interface DataConfigDao {
    DataConfig findById(int id);

    DataConfig findByProperty(String property);

    List<DataConfig> findAll();

    void save(DataConfig dataConfig);

    void update(DataConfig dataConfig);

    void delete(DataConfig dataConfig);
}

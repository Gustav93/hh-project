package com.hairandhead.project.sucursal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.utils.AbstractDao;

@Repository("sucursalDao")
public class SucursalDaoImpl extends AbstractDao<Integer, Sucursal> implements SucursalDao {

	@Override
	public void guardar(Sucursal sucursal) {
		super.persist(sucursal);
	}
	
	@Override
	public void update(Sucursal sucursal) {
		super.update(sucursal);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sucursal> obtenerSucursales() {
		Criteria cri = super.createEntityCriteria();
		return (List<Sucursal>) cri.list();
	}

	@Override
	public Sucursal buscarPorId(Integer id) {
		return (Sucursal) super.getByKey(id);
	}

	@Override
	public List<Sucursal> obtenerSucursalesActivas() {
		List<Sucursal> sucursales = this.obtenerSucursales();
		List<Sucursal> ret = new ArrayList<Sucursal>();
		for(Sucursal sucursal : sucursales) {
			if(sucursal.getEstado().equals(EstadoSucursal.ALTA)) {
				ret.add(sucursal);
			}
		}
		return ret;
	}

}

package com.hairandhead.project.sucursal.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hairandhead.project.cliente.dto.ClienteJsonDTO;
import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.sucursal.dto.SucursalDTO;
import com.hairandhead.project.sucursal.dto.SucursalJsonDTO;
import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;


@Controller
@RequestMapping(value = "/sucursales")
public class SucursalController {
	@Autowired
	SucursalService serviceSucursal;
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listar(ModelMap model) {
		List<Sucursal> sucursales = serviceSucursal.obtenerSucursales();
		List<SucursalDTO> sucursalesDTO = new ArrayList<SucursalDTO>();
		for(Sucursal sucursal: sucursales) {
			SucursalDTO sucursalDTO = new SucursalDTO();
			serviceSucursal.convertToDto(sucursal, sucursalDTO);
			sucursalesDTO.add(sucursalDTO);
		}
		
		model.addAttribute("sucursalesDTO", sucursalesDTO);
		
		return "/sucursales/listarSucursales";
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String crear(ModelMap model) {
		SucursalDTO sucursalDTO = new SucursalDTO();
		model.addAttribute("sucursalDTO", sucursalDTO);
		return "/sucursales/formSucursal";
	}
	
	@RequestMapping(value="/create", method = RequestMethod.POST)
	public String guardar(SucursalDTO sucursalDTO, ModelMap model, RedirectAttributes attributes) {
		Sucursal sucursal = new Sucursal();
		
		if(serviceSucursal.convertToSucursal(sucursalDTO, sucursal)) {
			serviceSucursal.guardar(sucursal);
			attributes.addAttribute("msgExito", "La sucursal se agrego con exito");
			System.out.println("Pude crear con exito");
			return "redirect:/sucursales/list";
		}
		else {
			attributes.addAttribute("msgFallo", "No se pudo agregar la sucursal");
			System.out.println("No pude crear");
			return "redirect:/sucursales/create";
		}
		
		
	}
	
	@RequestMapping(value = "/state/{id}", method = RequestMethod.GET)
	public String editarEstado(@PathVariable Integer id, ModelMap model) {
		
		Sucursal sucursal = serviceSucursal.buscarPorId(id);
		if(sucursal.getEstado().equals(EstadoSucursal.ALTA)) {
			sucursal.setEstado(EstadoSucursal.BAJA);
		}
		else {
			sucursal.setEstado(EstadoSucursal.ALTA);
		}
		serviceSucursal.update(sucursal);
		
		return "redirect:/sucursales/list";
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public String editar(@PathVariable Integer id,ModelMap model) {
		Sucursal sucursal = serviceSucursal.buscarPorId(id);
		SucursalDTO sucursalDTO = new SucursalDTO();
		serviceSucursal.convertToDto(sucursal, sucursalDTO);
		model.addAttribute("sucursalDTO", sucursalDTO);
		model.addAttribute("edit", true);
		
		return "/sucursales/formSucursal";
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.POST)
	public String update(SucursalDTO sucursalDTO, @PathVariable Integer id, ModelMap model) {
		
		Sucursal sucursal = new Sucursal();
		if(serviceSucursal.convertToSucursal(sucursalDTO, sucursal)) {
			serviceSucursal.update(sucursal);
			System.out.println("Pude editar con exito");
			return "redirect:/sucursales/list";
		}
		else {
			System.out.println("No pude editar");
			return "redirect:/sucursales/list";
		}
	
	}
	
	@RequestMapping(value = { "/getSucursales" }, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<SucursalJsonDTO>> getClientes() {
		List<SucursalJsonDTO> sucursalJsonList = new ArrayList<SucursalJsonDTO>();
		SucursalJsonDTO sucursalJson = null;
		List<Sucursal> sucursales = serviceSucursal.obtenerSucursalesActivas();
		for (Sucursal s : sucursales) {
			sucursalJson = new SucursalJsonDTO();
			sucursalJson.setValue(s.getId());
			sucursalJson.setText(s.getNombre());

			sucursalJsonList.add(sucursalJson);
		}
		return new ResponseEntity<List<SucursalJsonDTO>>(sucursalJsonList, HttpStatus.OK);
	}
	
	
	
}

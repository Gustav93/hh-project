package com.hairandhead.project.sucursal.dto;

public class SucursalJsonDTO {
	 	private Integer value;
	    private String text;
	    
		public Integer getValue() {
			return value;
		}
		public void setValue(Integer value) {
			this.value = value;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
	    
	    
}

package com.hairandhead.project.sucursal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.sucursal.dao.SucursalDao;
import com.hairandhead.project.sucursal.dto.SucursalDTO;
import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
@Service
@Transactional
public class SucursalServiceImpl implements SucursalService {
	
	@Autowired
	SucursalDao dao;
	
	@Override
	public void guardar(Sucursal sucursal) {
		dao.guardar(sucursal);
	}

	@Override
	public List<Sucursal> obtenerSucursales() {
		return dao.obtenerSucursales();
	}
	
	@Override
	public Sucursal buscarPorId(Integer id) {
		return dao.buscarPorId(id);
	}

	@Override
	public boolean convertToSucursal(SucursalDTO sucursalDTO, Sucursal sucursal) {
		try{
			sucursal.setId(sucursalDTO.getId());
			sucursal.setNombre((sucursalDTO.getNombre()));
			sucursal.setLocalidad(sucursalDTO.getLocalidad());
			sucursal.setEstado(EstadoSucursal.ALTA);
			return true;
		}
		catch(Exception e) {
			System.out.println(e);
			return false;
		}
	
		
	}

	@Override
	public boolean convertToDto(Sucursal sucursal, SucursalDTO sucursalDTO) {
		try{
			sucursalDTO.setId(sucursal.getId());
			sucursalDTO.setNombre(sucursal.getNombre());
			sucursalDTO.setLocalidad(sucursal.getLocalidad());
			sucursalDTO.setEstado(sucursal.getEstado().name());	
			return true;
		}
		catch(Exception e) {
			System.out.println(e);
			return false;
		}
			
	}

	@Override
	public void update(Sucursal sucursal) {
		dao.update(sucursal);
	}

	@Override
	public List<Sucursal> obtenerSucursalesActivas() {
		return dao.obtenerSucursalesActivas();
	}

}

package com.hairandhead.project.promocion.dao;

import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ServicioPromocionAplicadaDao")
public class ServicioPromocionAplicadaDaoImpl extends AbstractDao<Integer, ServicioPromocionAplicada> implements ServicioPromocionAplicadaDao {
    @Override
    public ServicioPromocionAplicada findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public void save(ServicioPromocionAplicada servicio) {
        super.persist(servicio);
    }

    @Override
    public List<ServicioPromocionAplicada> findAll() {
        Criteria criteria = createEntityCriteria();
        return (List<ServicioPromocionAplicada>) criteria.list();
    }
}

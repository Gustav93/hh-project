package com.hairandhead.project.promocion.dao;

import java.util.List;

import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;

public interface PromocionDao {

	Promocion findById(int id);

    void save(Promocion p);

    void update(Promocion p);

    List<Promocion> findAll();

    List<Promocion> findByEstado(EstadoPromocion estadoPromocion);
}

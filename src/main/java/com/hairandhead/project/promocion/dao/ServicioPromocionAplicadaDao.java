package com.hairandhead.project.promocion.dao;

import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;

import java.util.List;

public interface ServicioPromocionAplicadaDao {
    ServicioPromocionAplicada findById(int id);

    public void save(ServicioPromocionAplicada servicio);

    public List<ServicioPromocionAplicada> findAll();

    public void update(ServicioPromocionAplicada servicio);
}

package com.hairandhead.project.promocion.dao;

import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("PromocionAplicadaDao")
public class PromocionAplicadaDaoImpl extends AbstractDao<Integer, PromocionAplicada> implements PromocionAplicadaDao{

    @Autowired
    private ServicioPromocionAplicadaDao servicioPromocionAplicadaDao;

    @Override
    public PromocionAplicada findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public void save(PromocionAplicada p) {
        p.getServicios().forEach(servicio->{
            servicioPromocionAplicadaDao.save(servicio);
        });
        super.persist(p);
    }

    @Override
    public void update(PromocionAplicada p)
    {
        super.update(p);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PromocionAplicada> findAll() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("id"));
        return (List<PromocionAplicada>) criteria.list();
    }
}

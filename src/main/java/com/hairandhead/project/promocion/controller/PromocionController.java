package com.hairandhead.project.promocion.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;

@Controller
@RequestMapping("/promociones")
public class PromocionController {

	@Autowired
	private PromocionService promocionService;
	
	@Autowired
	private ServicioService servicioService;
	
	@RequestMapping(value = {"/list"}, method = RequestMethod.GET)
	public String list(ModelMap model) 
	{
		model.addAttribute("promocionesDTO", this.finAllPromocionesDTO());
		return "promociones/listPromociones";
	}
	
	@RequestMapping(value = {"/create"}, method = RequestMethod.GET)
	public String create(ModelMap model)
	{
		model.addAttribute("promocionDTO", new PromocionDTO());
		model.addAttribute("servicios", this.getServiciosActivos());
		model.addAttribute("edit", false);
		return "promociones/formPromocion";
	}
	
	@RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public String save(@Valid PromocionDTO promocionDTO, BindingResult result, ModelMap model){

		if(result.hasErrors()) {
			return "promociones/formPromocion";
		}
				
		Promocion p = promocionService.convertToPromocion(promocionDTO);
		p.setFechaDeAlta(LocalDate.now());
		p.setFechaDeBaja(p.getEstado().equals(EstadoPromocion.INACTIVO)?LocalDate.now():null);
		p.setServicios(this.obteberServicios(promocionDTO.getServicios()));
		p.setValorFinal(this.setValorFinal(p.getDescuento(),p.getServicios()));
		String cadena = p.getNombre();
		String res = cadena.replace("ó", "\u00f3");
		p.setNombre(res);
		promocionService.savePromocion(p);
		model.addAttribute("mensaje", "La Promoción "+ p.getNombre() + " fue guardada con Exito");
        return "redirect:/promociones/list";
    }
	
	@RequestMapping(value= { "/edit/{id}" }, method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap model)
	{
		PromocionDTO dto = new PromocionDTO();
		Promocion p = this.promocionService.findPromocionById(id);
		dto = promocionService.convertToDTO(p);
		dto.setServicios(this.obtenerIds(p.getServicios()));
		model.addAttribute("promocionDTO", dto);
		model.addAttribute("servicios", this.getServiciosActivos());
		model.addAttribute("edit", true);
		return "promociones/formPromocion";
	}
	
	@RequestMapping(value= { "/edit/{id}" }, method = RequestMethod.POST)
	public String updatePromocion(@Valid PromocionDTO promocionDTO, BindingResult result, ModelMap model)
	{
		if(result.hasErrors()) {
			return "promociones/formPromocion";
		}
		
		Promocion p = this.promocionService.convertToPromocion(promocionDTO);
		p.setServicios(this.obteberServicios(promocionDTO.getServicios()));
		p.setFechaDeBaja(p.getEstado().equals(EstadoPromocion.INACTIVO)?LocalDate.now():null);
		p.setValorFinal(this.setValorFinal(p.getDescuento(),p.getServicios()));
		promocionService.updatePromocion(p);
		model.addAttribute("mensaje", "La Promoción "+ p.getNombre() + " fue actualizada con Exito");
		return "redirect:/promociones/list";
	}
	
	@RequestMapping(value= { "/delete/{id}" }, method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id, ModelMap model)
	{
		Promocion p = this.promocionService.findPromocionById(id);
		p.setEstado(EstadoPromocion.INACTIVO);
		p.setFechaDeBaja(LocalDate.now());
		this.promocionService.updatePromocion(p);
		model.addAttribute("mensaje", "La promomocion "+p.getNombre()+" fue dada de Baja");
		return "redirect:/promociones/list";
	}
	
	@RequestMapping(value= { "/active/{id}" }, method = RequestMethod.GET)
	public String active(@PathVariable("id") int id, ModelMap model)
	{
		Promocion p = this.promocionService.findPromocionById(id);
		p.setEstado(EstadoPromocion.ACTIVO);
		p.setFechaDeBaja(null);
		this.promocionService.updatePromocion(p);
		model.addAttribute("mensaje", "La promomocion "+p.getNombre()+" fue dada de Alta");
		return "redirect:/promociones/list";
	}
	
	public List<PromocionDTO> finAllPromocionesDTO()
	{	
		List<PromocionDTO> lista = new ArrayList<>();
		for (Promocion p : promocionService.findAllPromociones())
		{
			PromocionDTO dto = promocionService.convertToDTO(p);
			if(!lista.contains(dto))
				lista.add(dto);
		}
			
		return lista;
	}
	
	@RequestMapping(value= { "/detail/{id}" }, method = RequestMethod.GET)
	public String detalle(@PathVariable("id") int id, ModelMap model)
	{
		Promocion p = this.promocionService.findPromocionById(id);
		PromocionDTO dto = this.promocionService.convertToDTO(p);
		model.addAttribute("promocionDTO", dto);
		model.addAttribute("serviciosDTO", this.obtenerServiciosDTO(p.getServicios()));
		return "promociones/detallePromocion";
	}
	
	public List<ServicioDTO> obtenerServiciosDTO(List<Servicio> servicios)
	{
		List<ServicioDTO> dtos = new ArrayList<>();
		ServicioDTO dto;
		for (Servicio s : servicios) {
			dto = new ServicioDTO(); 
			this.servicioService.convertToDTO(s, dto);
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	public List<Servicio> obteberServicios(List<Integer> idServicios)
	{
		List<Servicio> servicios = new ArrayList<>();
		for (Integer id : idServicios) {
			servicios.add(this.servicioService.findServicioById(id));
		}
		return servicios;
	}
	
	public List<Integer> obtenerIds(List<Servicio> servicios)
	{
		List<Integer> ids = new ArrayList<>();
		for (Servicio s : servicios) {
			ids.add(s.getId());
		}
		return ids;
	}
	
	public List<Servicio> getServiciosActivos()
	{
		List<Servicio> activos = new ArrayList<>();
		for (Servicio s : this.servicioService.findAllServicios()) {
			if(s.getEstado().equals(EstadoServicio.ACTIVO))
				activos.add(s);
		}
		return activos;
	}
	
	public BigDecimal setValorFinal(BigDecimal descuento, List<Servicio> servicios) {
		if(descuento == null || servicios == null || servicios.size() == 0) return BigDecimal.ZERO;
		
		BigDecimal acumulador = BigDecimal.ZERO;
		BigDecimal a;
		for (Servicio s : servicios) {
			a = BigDecimal.ZERO;
			a = a.add(descuento);
			a = a.multiply(s.getPrecio());
			a = a.divide(new BigDecimal("100"));
			acumulador = acumulador.add(s.getPrecio());
			acumulador = acumulador.subtract(a);
		}
		return acumulador;
	}
	
	/**
	 * Personalizamos el Data Binding para todas las propiedades de tipo Date
	 * @param webDataBinder
	 */
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
}

package com.hairandhead.project.promocion.dto;

import java.util.ArrayList;
import java.util.List;

public class PromocionAplicadaDTO {

    private Integer id ;

    private String nombre, descuento, estado, valorFinal, multiplicaPuntos;

    private String fechaDeAlta, fechaDeVencimiento, fechaDeBaja;

    private List<Integer> servicios;

    public PromocionAplicadaDTO() {
        this.id = null;
        this.nombre = "";
        this.descuento = "";
        this.fechaDeAlta = null;
        this.fechaDeVencimiento = null;
        this.fechaDeBaja = null;
        this.estado = "ACTIVO";
        this.servicios = new ArrayList<>();
        this.multiplicaPuntos = "1";
        this.valorFinal = "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Integer> getServicios() {
        return servicios;
    }

    public void setServicios(List<Integer> servicios) {
        this.servicios = servicios;
    }

    public String getFechaDeVencimiento() {
        return fechaDeVencimiento;
    }

    public void setFechaDeVencimiento(String fechaDeVencimiento) {
        this.fechaDeVencimiento = fechaDeVencimiento;
    }

    public String getFechaDeBaja() {
        return fechaDeBaja;
    }

    public void setFechaDeBaja(String fechaDeBaja) {
        this.fechaDeBaja = fechaDeBaja;
    }

    public String getFechaDeAlta() {
        return fechaDeAlta;
    }

    public void setFechaDeAlta(String fechaDeAlta) {
        this.fechaDeAlta = fechaDeAlta;
    }

    public String getMultiplicaPuntos() {
        return multiplicaPuntos;
    }

    public void setMultiplicaPuntos(String multiplicaPuntos) {
        this.multiplicaPuntos = multiplicaPuntos;
    }

    public String getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(String valorFinal) {
        this.valorFinal = valorFinal;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descuento == null) ? 0 : descuento.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fechaDeAlta == null) ? 0 : fechaDeAlta.hashCode());
		result = prime * result + ((fechaDeBaja == null) ? 0 : fechaDeBaja.hashCode());
		result = prime * result + ((fechaDeVencimiento == null) ? 0 : fechaDeVencimiento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((multiplicaPuntos == null) ? 0 : multiplicaPuntos.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((servicios == null) ? 0 : servicios.hashCode());
		result = prime * result + ((valorFinal == null) ? 0 : valorFinal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PromocionAplicadaDTO other = (PromocionAplicadaDTO) obj;
		if (descuento == null) {
			if (other.descuento != null)
				return false;
		} else if (!descuento.equals(other.descuento))
			return false;
		if (estado == null) {
			if (other.estado != null)
				return false;
		} else if (!estado.equals(other.estado))
			return false;
		if (fechaDeAlta == null) {
			if (other.fechaDeAlta != null)
				return false;
		} else if (!fechaDeAlta.equals(other.fechaDeAlta))
			return false;
		if (fechaDeBaja == null) {
			if (other.fechaDeBaja != null)
				return false;
		} else if (!fechaDeBaja.equals(other.fechaDeBaja))
			return false;
		if (fechaDeVencimiento == null) {
			if (other.fechaDeVencimiento != null)
				return false;
		} else if (!fechaDeVencimiento.equals(other.fechaDeVencimiento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (multiplicaPuntos == null) {
			if (other.multiplicaPuntos != null)
				return false;
		} else if (!multiplicaPuntos.equals(other.multiplicaPuntos))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (servicios == null) {
			if (other.servicios != null)
				return false;
		} else if (!servicios.equals(other.servicios))
			return false;
		if (valorFinal == null) {
			if (other.valorFinal != null)
				return false;
		} else if (!valorFinal.equals(other.valorFinal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PromocionAplicadaDTO [id=" + id + ", nombre=" + nombre + ", descuento=" + descuento + ", estado="
				+ estado + ", valorFinal=" + valorFinal + ", multiplicaPuntos=" + multiplicaPuntos + ", fechaDeAlta="
				+ fechaDeAlta + ", fechaDeVencimiento=" + fechaDeVencimiento + ", fechaDeBaja=" + fechaDeBaja
				+ ", servicios=" + servicios + "]";
	}
    
    
}

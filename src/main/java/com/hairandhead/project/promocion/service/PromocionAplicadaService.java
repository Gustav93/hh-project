package com.hairandhead.project.promocion.service;

import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;

import java.util.List;

public interface PromocionAplicadaService {

    PromocionAplicada findPromocionAplicadaById(int id);
    void savePromocionAplicada(PromocionAplicada p);
    void updatePromocion(PromocionAplicada p);
    List<PromocionAplicada> findAllPromocionesAplicadas();
    PromocionAplicadaDTO convertPromocionApliocadaToDTO(PromocionAplicada p);
    List<PromocionAplicadaDTO> convertPromocionAplicadaToDTOList(List<PromocionAplicada> promocionList);
}

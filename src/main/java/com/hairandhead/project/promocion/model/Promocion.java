package com.hairandhead.project.promocion.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import com.hairandhead.project.servicio.model.Servicio;

@Entity
@Table(name ="Promociones")
public class Promocion implements Serializable, Comparable<Promocion>{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Digits(integer=8, fraction=2)
	@Column(name="Descuento")
	private BigDecimal descuento;
	
	@Column(name="FechaDeAlta")
	private LocalDate fechaDeAlta;
	
	@Column(name="FechaDeVencimiento")
	private LocalDate fechaDeVencimiento;
	
	@Column(name="FechaDeBaja")
	private LocalDate fechaDeBaja;
	
	@Column(name="Estado")
	private EstadoPromocion estado;
	
	@Column(name="ValorFinal")
	private BigDecimal valorFinal;
	
	@Column(name="MultiplicaPuntos")
	private BigDecimal multiplicaPuntos;
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private List<Servicio> servicios;

	public Promocion() {
		this.estado = EstadoPromocion.ACTIVO;
		this.fechaDeAlta = null;
		this.fechaDeVencimiento = null;
		this.fechaDeBaja = null;
		this.servicios = new ArrayList<>();
		this.multiplicaPuntos = new BigDecimal("1");
		this.descuento = new BigDecimal("0");
		this.valorFinal = new BigDecimal("0");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getDescuento() {
		return descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public LocalDate getFechaDeAlta() {
		return fechaDeAlta;
	}

	public void setFechaDeAlta(LocalDate fechaDeAlta) {
		this.fechaDeAlta = fechaDeAlta;
	}

	public LocalDate getFechaDeVencimiento() {
		return fechaDeVencimiento;
	}

	public void setFechaDeVencimiento(LocalDate fechaDeVencimiento) {
		this.fechaDeVencimiento = fechaDeVencimiento;
	}

	public LocalDate getFechaDeBaja() {
		return fechaDeBaja;
	}

	public void setFechaDeBaja(LocalDate fechaDeBaja) {
		this.fechaDeBaja = fechaDeBaja;
	}

	public EstadoPromocion getEstado() {
		return estado;
	}

	public void setEstado(EstadoPromocion estado) {
		this.estado = estado;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public BigDecimal getValorFinal() {
		return valorFinal;
	}

	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}

	public BigDecimal getMultiplicaPuntos() {
		return multiplicaPuntos;
	}

	public void setMultiplicaPuntos(BigDecimal multiplicaPuntos) {
		this.multiplicaPuntos = multiplicaPuntos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descuento == null) ? 0 : descuento.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((fechaDeAlta == null) ? 0 : fechaDeAlta.hashCode());
		result = prime * result + ((fechaDeBaja == null) ? 0 : fechaDeBaja.hashCode());
		result = prime * result + ((fechaDeVencimiento == null) ? 0 : fechaDeVencimiento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((multiplicaPuntos == null) ? 0 : multiplicaPuntos.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((servicios == null) ? 0 : servicios.hashCode());
		result = prime * result + ((valorFinal == null) ? 0 : valorFinal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promocion other = (Promocion) obj;
		if (descuento == null) {
			if (other.descuento != null)
				return false;
		} else if (!descuento.equals(other.descuento))
			return false;
		if (estado != other.estado)
			return false;
		if (fechaDeAlta == null) {
			if (other.fechaDeAlta != null)
				return false;
		} else if (!fechaDeAlta.equals(other.fechaDeAlta))
			return false;
		if (fechaDeBaja == null) {
			if (other.fechaDeBaja != null)
				return false;
		} else if (!fechaDeBaja.equals(other.fechaDeBaja))
			return false;
		if (fechaDeVencimiento == null) {
			if (other.fechaDeVencimiento != null)
				return false;
		} else if (!fechaDeVencimiento.equals(other.fechaDeVencimiento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (multiplicaPuntos == null) {
			if (other.multiplicaPuntos != null)
				return false;
		} else if (!multiplicaPuntos.equals(other.multiplicaPuntos))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (servicios == null) {
			if (other.servicios != null)
				return false;
		} else if (!servicios.equals(other.servicios))
			return false;
		if (valorFinal == null) {
			if (other.valorFinal != null)
				return false;
		} else if (!valorFinal.equals(other.valorFinal))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Promocion [id=" + id + ", nombre=" + nombre + ", descuento=" + descuento + ", fechaDeAlta="
				+ fechaDeAlta + ", fechaDeVencimiento=" + fechaDeVencimiento + ", fechaDeBaja=" + fechaDeBaja
				+ ", estado=" + estado + ", valorFinal=" + valorFinal + ", multiplicaPuntos=" + multiplicaPuntos
				+ ", servicios=" + servicios + "]";
	}


	@Override
	public int compareTo(Promocion arg0) {
		if(this.getDescuento().compareTo(arg0.getDescuento()) > 0) {
			return 1;
		}
		else if(this.getDescuento().compareTo(arg0.getDescuento()) == 0) {
			return 0;
		}
		return 1;
	}

	

}

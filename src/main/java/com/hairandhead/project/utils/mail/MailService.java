package com.hairandhead.project.utils.mail;

import java.io.File;
import java.util.List;

public interface MailService {
    void sendMail(List<String> toList, String subject, String body);
    void sendMailWithAttachment(List<String> toList, String subject, String body, List<File> fileList);
}

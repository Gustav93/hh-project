package com.hairandhead.project.utils.mail;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.*;
import java.util.List;
import java.util.Properties;

@Service("MailService")
public class MailServiceImpl implements MailService {

    @Autowired
    private DataConfigService dataConfigService;

    private static String EMAIL_SENDER;
    private static String PWD;

    @Override
    public void sendMail(List<String> toList, String subject, String body) {
        setUsernameAndPassword();
        Session session = Session.getDefaultInstance(getProperties());
        Transport t;
        try {
            MimeMultipart multipart = new MimeMultipart();

            //Agrego el cuerpo del mail
            BodyPart texto = new MimeBodyPart();

            texto.setText(body);
            multipart.addBodyPart(texto);

            MimeMessage message = new MimeMessage(session);

            // Quien envia el correo
            message.setFrom(new InternetAddress(EMAIL_SENDER));

            // A quien va dirigido
            message.addRecipients(Message.RecipientType.TO, convertToAddresArray(toList));

            message.setSubject(subject);

            message.setContent(multipart);

            t = session.getTransport("smtp");
            t.connect(EMAIL_SENDER, PWD);

            t.sendMessage(message,message.getAllRecipients());

            t.close();
        }
        catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMailWithAttachment(List<String> toList, String subject, String body, List<File> fileList) {
        setUsernameAndPassword();
        Session session = Session.getDefaultInstance(getProperties());
        Transport t;
        try {
            MimeMultipart multipart = new MimeMultipart();

            for(File attachment : fileList){
                if(!isEmpty(attachment)) {
                    BodyPart attachedFile = new MimeBodyPart();
                    attachedFile.setDataHandler(new DataHandler(new FileDataSource(attachment.getName())));
                    attachedFile.setFileName(attachment.getName());
                    multipart.addBodyPart(attachedFile);
                }
            }

            //Agrego el cuerpo del mail
            BodyPart texto = new MimeBodyPart();

            texto.setText(body);
            multipart.addBodyPart(texto);

            MimeMessage message = new MimeMessage(session);

            // Quien envia el correo
            message.setFrom(new InternetAddress(EMAIL_SENDER));

            // A quien va dirigido
            message.addRecipients(Message.RecipientType.TO, convertToAddresArray(toList));

            message.setSubject(subject);

            message.setContent(multipart);

            t = session.getTransport("smtp");
            t.connect(EMAIL_SENDER, PWD);

            t.sendMessage(message,message.getAllRecipients());

            t.close();
        }
        catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Properties getProperties(){
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.host", "smtp.gmail.com");

        // TLS si está disponible
        props.setProperty("mail.smtp.starttls.enable", "true");

        // Puerto de gmail para envio de correos
        props.setProperty("mail.smtp.port","587");

        // Nombre del usuario
        props.setProperty("mail.smtp.user", EMAIL_SENDER);

        // Si requiere o no usuario y password para conectarse.
        props.setProperty("mail.smtp.auth", "true");

        return props;
    }

    //transforma el arreglo de string con las direcciones de mail a un arreglo de Address para poder enviar los correos
    private static Address[] convertToAddresArray(List<String> addessList) throws AddressException
    {
        Address[] addresArray = new Address[addessList.size()];

        for(int i=0; i < addessList.size(); i++)
            addresArray[i] = new InternetAddress(addessList.get(i));

        return addresArray;
    }

    private boolean isEmpty(File file) {
        FileReader fr;
        int lineNumber = 0;
        try {
            fr = new FileReader(file);
            LineNumberReader lnr = new LineNumberReader(fr);

            while(lnr.readLine() != null)
                lineNumber++;
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lineNumber <= 1;
    }

    //busca los datos de las properties, si falta algun dato, usa el mail y pass por defecto.
    private void setUsernameAndPassword(){
        DataConfig email = dataConfigService.findByProperty("email.user");
        DataConfig password = dataConfigService.findByProperty("email.password");
        if(email==null||password==null||email.getValue().isEmpty()||password.getValue().isEmpty()){
            EMAIL_SENDER = "gustavsanchez93@gmail.com";
            PWD = "fff0303456fff";
            return;
        }
        EMAIL_SENDER = email.getValue();
        PWD = password.getValue();
    }
}

package com.hairandhead.project.profesional.dto;


import java.util.Objects;

public class ProfesionalDTOJson {

    private Integer value;

    private String text;

    public ProfesionalDTOJson() {
        this.value = null;
        this.text = "";
    }
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfesionalDTOJson that = (ProfesionalDTOJson) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, text);
    }
}
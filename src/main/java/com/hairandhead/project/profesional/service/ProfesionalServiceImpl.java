package com.hairandhead.project.profesional.service;

import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.HorarioDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteService;
import com.hairandhead.project.profesional.dao.ProfesionalDao;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.mail.MailService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Transactional
@Service("ProfesionalService")
public class ProfesionalServiceImpl implements ProfesionalService {

    @Autowired
    private ProfesionalDao profesionalDao;
    @Autowired
    private ServicioService service;
    @Autowired
    private ClienteService clienteService;
	@Autowired
	private MailService mailService;

    private static final Logger LOG = Logger.getLogger(ProfesionalServiceImpl.class);

    @Override
    public Profesional findById(int id) {
        return profesionalDao.findById(id);
    }

    @Override
    public void save(Profesional profesional) {
            profesionalDao.save(profesional);
    }

    @Override
    public void update(Profesional profesional) {
            profesionalDao.update(profesional);
    }

    @Override
    public List<Profesional> findAll() {
        return profesionalDao.findAll();
    }

    @Override
    public Profesional findByEmail(String email) {
        if(email==null){
            return null;
        }
        return profesionalDao.findByEmail(email);
    }

    @Override
    public Profesional convertToProfesional(ProfesionalDTO dto) {
        Profesional profesional = new Profesional();
        Servicio servicio =null;
        List<Servicio> listServicio = new ArrayList<Servicio>();
        if(dto==null){
            return null;
        }
        
        profesional.setId(dto.getId());
        profesional.setNombre(dto.getNombre()!=null?dto.getNombre():"");
        profesional.setApellido(dto.getApellido()!=null?dto.getApellido():"");
        profesional.setEmail(dto.getEmail()!=null?dto.getEmail():"");
        profesional.setTelefono(dto.getTelefono()!=null?dto.getTelefono():"");
        
        try {
        	profesional.setFechaDeNacimiento(dto.getFechaDeNacimiento()!=null?LocalDate.parse(dto.getFechaDeNacimiento(), DateTimeFormatter.ofPattern("MM/dd/yyyy")):null);
        } catch (Exception e) {
			 LOG.error(e.getCause());
		}
        profesional.setEstadoProfesional(EstadoProfesional.ACTIVO);
        profesional.setServicios(listServicio);
        
        return profesional;
    }

    @Override
    public ProfesionalDTO convertToDTO(Profesional profesional) {
        ProfesionalDTO dto = new ProfesionalDTO();
      
        ServicioDTO servicioDTO =null;
        List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
        if(profesional==null){
            return null;
        }
        
        for(Servicio s: profesional.getServicios()) {
        	servicioDTO = new ServicioDTO();
        	service.convertToDTO(s, servicioDTO);
        	listServicioDTO.add(servicioDTO);
        }
        
        dto.setId(profesional.getId());
        dto.setNombre(profesional.getNombre()!=null?profesional.getNombre():"");
        dto.setApellido(profesional.getApellido()!=null?profesional.getApellido():"");
        dto.setEmail(profesional.getEmail()!=null?profesional.getEmail():"");
        dto.setTelefono(profesional.getTelefono()!=null?profesional.getTelefono():"");
        dto.setFechaDeNacimiento(profesional.getFechaDeNacimiento()!=null?profesional.getFechaDeNacimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):"");
        dto.setEstadoProfesional(profesional.getEstadoProfesional()!=null?profesional.getEstadoProfesional().name():"");
        dto.setFechaBaja(profesional.getFechaBaja()!=null? profesional.getFechaBaja().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):"");
        dto.setEstado(profesional.getEstadoProfesional()!=null&&profesional.getEstadoProfesional().name()!=null?profesional.getEstadoProfesional().name():"");
        return dto;
    }

    @Override
    public List<ProfesionalDTO> convertToDTOList(List<Profesional> profesionalList) {
        List<ProfesionalDTO> dtoList = new ArrayList<>();
        profesionalList.forEach(profesional -> dtoList.add(convertToDTO(profesional)));
        return dtoList;
    }
    
    public List<DisponibilidadDTO> convertDisponibilidadToDTOList(List<Disponibilidad> listaDisp) {
        List<DisponibilidadDTO> dtoList = new ArrayList<>();
        DisponibilidadDTO dto=null;
        for(Disponibilidad disponibilidad: listaDisp){
        	dto = new DisponibilidadDTO();
        	dto.setId(disponibilidad.getId());
        	dto.setDia(disponibilidad.getDia());
        	dto.setHoraDisponible(convertHorarioToDTOList(disponibilidad.getHoraDisponible()));
        	dtoList.add(dto);
        }
        return dtoList;
    }
    
    public List<HorarioDTO> convertHorarioToDTOList(List<Horario> listaHorario){
    	 List<HorarioDTO> dtoList = new ArrayList<>();
    	 HorarioDTO dto=null;
         for(Horario horario: listaHorario){
         	dto = new HorarioDTO();
         	dto.setId(horario.getId());
         	dto.setHoraInicio(horario.getHoraInicio().format(DateTimeFormatter.ofPattern("HH:mm")));
         	dto.setHoraFin(horario.getHoraFin().format(DateTimeFormatter.ofPattern("HH:mm")));
         	dtoList.add(dto);
         }
         return dtoList;
    	
    }

	@Override
	public Disponibilidad findDisponibilidadById(int disponibilidad, int profesional) {
		Profesional prof = findById(profesional);
		for(Disponibilidad p: prof.getDisponibilidad()) {
			if(p.getId().equals(disponibilidad)) {
				return p;
			}
		}
		return null;
	}

	@Override
	public List<Profesional> findAllWithServiceEquals(Integer servicioId) {
		// TODO Auto-generated method stub
		return profesionalDao.findAllProfesionalesActivosConServicio(servicioId);
	}
	
	@Override
	public void sendMailToClients(String email, ArrayList<Turno> turno) {
		Cliente user = clienteService.findClienteByMail(email);
		if(user==null){
			return;
		}
		String subject = "Aviso de profesional de baja";
		StringBuilder body = new StringBuilder();
		body.append("Estimado ");
		body.append(user.getNombre() + " " +  user.getApellido());
		body.append(", se le notifica que un profesional con el que tiene turno/s reservado/s: ");
		for(Turno t: turno){
			body.append("\n\n ID de turno: " +  t.getId().toString());
			body.append("\n\n Fecha de turno: " + t.getFecha().toString());
			body.append("\n\n Hora de inicio: " + t.getHoraInicio().toString());
			body.append("\n\n----------------------");
		}
		body.append("\n\n Saludos!");
		
		/*String bodyMail = "Estimado "
				.concat(user.getNombre() + " " +  user.getApellido())
				.concat(", se le notifica que un profesional con el que tiene un turno reservado" +
				turno.get(0).getFecha().toString() + "se encuentra dado de baja.")
				.concat("\n\n Saludos!");*/
		 
         mailService.sendMail(Collections.singletonList(user.getEmail()), subject, body.toString()); 
	}
}

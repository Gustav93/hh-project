package com.hairandhead.project.profesional.service;

import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.turno.model.Turno;

import java.util.ArrayList;
import java.util.List;

public interface ProfesionalService {

    Profesional findById(int id);

    void save(Profesional profesional);

    void update(Profesional profesional);

    List<Profesional> findAll();

//    List<Profesional> findByEstado(EstadoProfesional estadoProfesional);

    Profesional findByEmail(String email);

    Profesional convertToProfesional(ProfesionalDTO dto);

    ProfesionalDTO convertToDTO(Profesional profesional);

    List<ProfesionalDTO> convertToDTOList(List<Profesional> profesionalList);

	List<DisponibilidadDTO> convertDisponibilidadToDTOList(List<Disponibilidad> profesional);

	Disponibilidad findDisponibilidadById(int disponibilidad, int profesional);

	List<Profesional> findAllWithServiceEquals(Integer servicioId);

	void sendMailToClients(String email, ArrayList<Turno> turno);
}

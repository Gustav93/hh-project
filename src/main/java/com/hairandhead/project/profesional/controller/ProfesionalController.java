package com.hairandhead.project.profesional.controller;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTOJson;
import com.hairandhead.project.profesional.model.Dias;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.profesional.service.ProfesionalService;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.controller.AppControllerTurno;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.DetalleTunoService;
import com.hairandhead.project.turno.service.TurnoService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/profesionales")
public class ProfesionalController {

    @Autowired
    private ProfesionalService profesionalService;
    @Autowired
    private ServicioService servicioService;
    
    @Autowired
    private TurnoService turnoService;
    
    @Autowired
	private DetalleTunoService detalleTunoService;
	
    @Autowired
	AppControllerTurno controllerTurno;
    
    List<Profesional> profesionalesIguales = new ArrayList<>();

    private static final Logger LOG = Logger.getLogger(ProfesionalController.class);

    @SuppressWarnings("null")
	@RequestMapping(value = { "/listProfesionalesJSON" }, method = RequestMethod.GET)
    public ResponseEntity<List<ProfesionalDTOJson>> listJson(@RequestParam Integer servicioId,@RequestParam String fecha)
    {
        List<ProfesionalDTO> profesionalDTO = new ArrayList<ProfesionalDTO>();
        profesionalDTO.addAll(profesionalService.convertToDTOList(profesionalService.findAllWithServiceEquals(servicioId)));
        
        ProfesionalDTOJson profesional = null;
		List<ProfesionalDTOJson> profesionalJson = new ArrayList<ProfesionalDTOJson>();
		LocalDate fechaSolicitada = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		HashMap<Integer, String> diasDeLaSemana = controllerTurno.diasDeLaSemana();
		String dia = diasDeLaSemana.get(fechaSolicitada.getDayOfWeek().getValue());
		boolean tieneDisponibilidad = false;
		
		for(ProfesionalDTO t: profesionalDTO) {
			Profesional profesionalDisp = profesionalService.findById(t.getId());
			List<DisponibilidadDTO> diasDisponibles = cargarDatosDisponibilidadDeProfesionales(profesionalDisp.getDisponibilidad());
			
			for(DisponibilidadDTO d: diasDisponibles) {
				if(d.getDia().equals(dia) && !d.getHoraDisponible().isEmpty()) {
					tieneDisponibilidad = true;
					break;
				}
			}
			
			if(tieneDisponibilidad) {
				profesional = new ProfesionalDTOJson();
				profesional.setValue(t.getId());
				profesional.setText(t.getNombre() +" "+ t.getApellido() + " - " + t.getEmail());
				profesionalJson.add(profesional);
			}
				
		}
		
		return new ResponseEntity<List<ProfesionalDTOJson>>(profesionalJson, HttpStatus.OK);
    }
    
    @RequestMapping(value = { "/list" }, method = RequestMethod.GET)
    public String list(ModelMap model)
    {
    	List<ProfesionalDTO> profesionalList = profesionalService.convertToDTOList(profesionalService.findAll());
    	List<ProfesionalDTO> filtrarRepetidos = new ArrayList<ProfesionalDTO>();
    	
    	for(ProfesionalDTO p : profesionalList) {
    		if(!filtrarRepetidos.contains(p)) {
    			filtrarRepetidos.add(p);
    		}
    	}
    	System.out.println(profesionalList);
        model.addAttribute("profesionales", filtrarRepetidos);
        return "profesionales/listProfesionales";
    }
    
    @RequestMapping(value = { "/create" }, method = RequestMethod.GET)
	public String create(ModelMap model)
	{
		ProfesionalDTO profesionalDTO = new ProfesionalDTO();
		
		model.addAttribute("profesionalDTO", profesionalDTO);

		model.addAttribute("serviciosList", servicioService.findAllServiciosActivos());
		model.addAttribute("edit", false);
		return "profesionales/formprofesional";
	}
    
    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public String save(@Valid ProfesionalDTO profesionalDTO, ModelMap model, RedirectAttributes attributes){
    	//Valido los campos y creo el profesional
		Profesional profesionalACrear = profesionalService.convertToProfesional(profesionalDTO);
		//Lo guardo
		profesionalService.save(profesionalACrear);
		//Aca tendria que recuperar el ultimo profesional que guarde.
		Profesional profesionalCreado = profesionalService.findByEmail(profesionalDTO.getEmail());
		System.out.println("Profesional creado:" + profesionalCreado);
		profesionalesIguales.add(profesionalCreado);
		profesionalDTO = profesionalService.convertToDTO(profesionalCreado);
		
		model.addAttribute("profesionalDTO", profesionalDTO);
		//Agrego los servicios a la vista
		List<ServicioDTO> servicios = new ArrayList<ServicioDTO>();
		List<Servicio> serv = servicioService.findAllServiciosActivos();
		for(Servicio s : serv) {
			ServicioDTO servicio = new ServicioDTO();
			servicioService.convertToDTO(s, servicio);
			servicios.add(servicio);
		}
		model.addAttribute("serviciosList", servicios );
		//Agrego los servicios del profesional a la vista
		List<Servicio> listServicios = profesionalCreado.getServicios();
		List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
		for(Servicio s: listServicios) {
			ServicioDTO servicio1 = new ServicioDTO();
			servicioService.convertToDTO(s, servicio1);
			listServicioDTO.add(servicio1);
		}
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesionalACrear.getDisponibilidad());
		
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("listaDisponibles",dias);
		
        return "profesionales/registerserviciosprofesional";
    }
    
	@RequestMapping(value = { "/nuevoservicio" }, method = RequestMethod.POST)
	public String newServicio(@Valid ProfesionalDTO profDTO, @RequestParam int servicio, ModelMap model, RedirectAttributes attributes) {
		Profesional profesional = profesionalService.findById(profDTO.getId());
		System.out.println("Profesional al cual le voy agregar un servicio:" + profesional);
		profesionalesIguales.add(profesional);
		List<Servicio> listServicios = profesional.getServicios();
		Servicio service = servicioService.findServicioById(servicio);
		
		listServicios.add(service);
		profesional.setServicios(listServicios);
		profesionalService.update(profesional);
		
		ProfesionalDTO turnoDTO = profesionalService.convertToDTO(profesional);
		
		//Agrego los servicios a la vista
		List<ServicioDTO> servicios = new ArrayList<ServicioDTO>();
		for(Servicio s : servicioService.findAllServiciosActivos()) {
			ServicioDTO servicioDTO = new ServicioDTO();
			servicioService.convertToDTO(s, servicioDTO);
			servicios.add(servicioDTO);
		}

		//Agrego los servicios del profesional a la vista
		List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
		for(Servicio s: listServicios) {
			ServicioDTO servicio1 = new ServicioDTO();
			servicioService.convertToDTO(s, servicio1);
			listServicioDTO.add(servicio1);
		}
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesional.getDisponibilidad());
		
		model.addAttribute("profesionalDTO", turnoDTO);
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("serviciosList", servicios);
		model.addAttribute("listaDisponibles",dias);
		return "profesionales/registerserviciosprofesional";
	}
	
	@RequestMapping(value = { "/nuevaDisponibilidad" }, method = RequestMethod.POST)
	public String newdisponibilidad(
			@RequestParam(value = "idProfesional", required = false)Integer idProfesional,
			@RequestParam(value = "dia", required = false)String dia,
			@RequestParam(value = "horaInicio", required = false)String horaInicio,
			@RequestParam(value = "horaFin", required = false)String horaFin, 
			ModelMap model, RedirectAttributes attributes) {
		Profesional profesional = profesionalService.findById(idProfesional);
		System.out.println("Profesional al cual le voy agregar una disponibilidad:" + profesional);
		profesionalesIguales.add(profesional);
		
		List<Servicio> listServicios = profesional.getServicios();
		List<Disponibilidad> listDisponibles = profesional.getDisponibilidad();
		//Dias diaDisponible = null;
		List<Horario> listHorarios = null;
		Horario horario = new Horario();
		Integer index = null;

		final Dias diaDisponible = Dias.valueOf(dia);
		
		
		List<Disponibilidad> listaDispAux = listDisponibles.stream().filter(d -> d.getDia().equals(diaDisponible.name())).collect(Collectors.toList());

		if(!listaDispAux.isEmpty()) {
			listHorarios = listaDispAux.get(0).getHoraDisponible();
			index = listDisponibles.indexOf(listaDispAux.get(0));
		}
		else {
			listHorarios = new ArrayList<Horario>();
		}
		
		Disponibilidad disp = new Disponibilidad();

			int indice = diaDisponible.getCode();
			disp.setDia(diaDisponible.toString());
			horario.setHoraInicio(LocalTime.parse(horaInicio));
			horario.setHoraFin(LocalTime.parse(horaFin));
			listHorarios.add(horario);
			disp.setHoraDisponible(listHorarios);
		
			if(index!=null) {
				listDisponibles.get(index).setHoraDisponible(listHorarios);
			}else {
				listDisponibles.add(disp);
			}
		
		profesional.setDisponibilidad(listDisponibles);
		profesionalService.update(profesional);
		
		ProfesionalDTO profesionalDTO = profesionalService.convertToDTO(profesional);
		
		//muestra todos los servicios 
		List<ServicioDTO> servicios = new ArrayList<ServicioDTO>();
		for(Servicio s : servicioService.findAllServiciosActivos()) {
			ServicioDTO servicioDTO = new ServicioDTO();
			servicioService.convertToDTO(s, servicioDTO);
			servicios.add(servicioDTO);
		}
		
		//Agrego los servicios del profesional a la vista
				List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
				for(Servicio s: listServicios) {
					ServicioDTO servicio1 = new ServicioDTO();
					servicioService.convertToDTO(s, servicio1);
					listServicioDTO.add(servicio1);
				}
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesional.getDisponibilidad());

				
		model.addAttribute("profesionalDTO", profesionalDTO);
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("serviciosList", servicios);
		model.addAttribute("listaDisponibles",dias);
		return "redirect:/profesionales/edit/"+profesionalDTO.getId();
	}
    
    @RequestMapping(value= { "/edit/{id}" }, method = RequestMethod.GET)
	public String edit(@PathVariable("id") int id, ModelMap model)
	{
    	Profesional profesional = profesionalService.findById(id);
		ProfesionalDTO dto = profesionalService.convertToDTO(profesional);
		List<Servicio> listServicios = profesional.getServicios();
		List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
		for(Servicio s: listServicios) {
			ServicioDTO servicio1 = new ServicioDTO();
			servicioService.convertToDTO(s, servicio1);
			listServicioDTO.add(servicio1);
		}
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesional.getDisponibilidad());
		
		model.addAttribute("profesionalDTO", dto);
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("serviciosList", servicioService.findAllServiciosActivos());
		model.addAttribute("listaDisponibles", dias);
		model.addAttribute("edit", true);
		return "profesionales/registerserviciosprofesional";
	}

	@RequestMapping(value= { "/edit" }, method = RequestMethod.POST)
	public String updateProfesional(@Valid ProfesionalDTO profesionalDTO , ModelMap model)
	{
		Profesional profesionalAux = profesionalService.findById(profesionalDTO.getId());
		List<Servicio> listServicios = profesionalAux.getServicios();	
		Profesional profesional = profesionalService.convertToProfesional(profesionalDTO);
		List<Disponibilidad> listDisponibilidad = profesionalAux.getDisponibilidad();	
		profesional.setServicios(listServicios);
		profesional.setDisponibilidad(listDisponibilidad);
		
		if(profesionalDTO.getEstado() != null) {
			if(profesionalDTO.getEstado().equals("ACTIVO")) {
				profesional.setEstadoProfesional(EstadoProfesional.ACTIVO);
			}
			if(profesionalDTO.getEstado().equals("INACTIVO")) {
				profesional.setEstadoProfesional(EstadoProfesional.INACTIVO);
			}
			if(profesionalDTO.getEstado().equals("LICENCIA")) {
				profesional.setEstadoProfesional(EstadoProfesional.LICENCIA);
			}
			if(profesionalDTO.getEstado().equals("VACACIONES")) {
				profesional.setEstadoProfesional(EstadoProfesional.VACACIONES);
			}
		}
		
		profesionalService.update(profesional);
		
		return "redirect:/profesionales/list";
	}
	
	
	@RequestMapping(value= { "/deleteDisponibilidad/{id}/{idHorario}" }, method = RequestMethod.POST)
	public String deleteDisponibilidad(@Valid ProfesionalDTO profesionalDTO,
			@PathVariable("id") int disponibilidad,
			@PathVariable("idHorario")Integer idHorario,
			ModelMap model)
	{
		Profesional profesional = profesionalService.findById(profesionalDTO.getId());
		List<Disponibilidad> listDisponibilidad = profesional.getDisponibilidad();
		List<Disponibilidad> listDisponibilidadNuevo = new ArrayList<Disponibilidad>();
		List<Disponibilidad> listDisponibilidadModificar = new ArrayList<Disponibilidad>();
		List<Horario> listHorario = new ArrayList<Horario>();
		Disponibilidad disponibilidadNueva = new Disponibilidad ();
		
		Disponibilidad disponibilidadABorrar = profesionalService.findDisponibilidadById(disponibilidad, profesionalDTO.getId());
		
		listDisponibilidadNuevo.addAll(listDisponibilidad.stream().filter(d -> !d.getDia().equals(disponibilidadABorrar.getDia()))
															 .collect(Collectors.toList()));
		
		
		listDisponibilidadModificar.addAll(listDisponibilidad.stream().filter(d -> d.getDia().equals(disponibilidadABorrar.getDia()))
															  .collect(Collectors.toList()));
		
		if(listDisponibilidadModificar != null) {
			for(Horario h: listDisponibilidadModificar.get(0).getHoraDisponible()) {
				if(!h.getId().equals(idHorario)) {
					listHorario.add(h);
				}
			}
			disponibilidadNueva.setDia(listDisponibilidadModificar.get(0).getDia());
			disponibilidadNueva.setId(listDisponibilidadModificar.get(0).getId());
			disponibilidadNueva.setHoraDisponible(listHorario);
			listDisponibilidadNuevo.add(disponibilidadNueva);
		}
		
		
		profesional.setDisponibilidad(listDisponibilidadNuevo);
		profesionalService.update(profesional);
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesional.getDisponibilidad());
		List<Servicio> listServicios = profesional.getServicios();
		List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
		for(Servicio s: listServicios) {
			ServicioDTO servicio1 = new ServicioDTO();
			servicioService.convertToDTO(s, servicio1);
			listServicioDTO.add(servicio1);
		}
		
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("serviciosList", servicioService.findAllServiciosActivos());
		model.addAttribute("listaDisponibles", dias);
		model.addAttribute("edit", true);
		return "profesionales/registerserviciosprofesional";
	}
	
	@RequestMapping(value= { "/deleteServicio/{id}" }, method = RequestMethod.POST)
	public String updateProfesional(@Valid ProfesionalDTO profesionalDTO,@PathVariable("id") int servicio, ModelMap model)
	{
		Profesional profesional = profesionalService.findById(profesionalDTO.getId());
		List<Servicio> listServicios = profesional.getServicios();
		
		Servicio servicioABorrar = servicioService.findServicioById(servicio);
		listServicios.remove(servicioABorrar);
		
		profesional.setServicios(listServicios);
		profesionalService.update(profesional);
		
		List<DisponibilidadDTO> dias = cargarDatosDisponibilidadDeProfesionales(profesional.getDisponibilidad());
		List<ServicioDTO> listServicioDTO = new ArrayList<ServicioDTO>();
		for(Servicio s: listServicios) {
			ServicioDTO servicio1 = new ServicioDTO();
			servicioService.convertToDTO(s, servicio1);
			listServicioDTO.add(servicio1);
		}
		
		model.addAttribute("profesionalesServiciosList", listServicioDTO);
		model.addAttribute("serviciosList", servicioService.findAllServiciosActivos());
		model.addAttribute("listaDisponibles", dias);
		model.addAttribute("edit", true);
		
		return "profesionales/registerserviciosprofesional";
	}
	
	@RequestMapping(value= { "/delete/{id}" }, method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id)
	{
		Profesional profesional = this.profesionalService.findById(id);
		profesional.setEstadoProfesional(EstadoProfesional.INACTIVO);
		profesional.setFechaBaja(LocalDateTime.now());
		this.profesionalService.update(profesional);
		this.sendMailToClients(profesional.getId());
		
		return "redirect:/profesionales/list";
	}
	
	public void sendMailToClients(Integer idProfesional)
	{
		Profesional profesional = this.profesionalService.findById(idProfesional);
		
		List<DetalleTurno> detalleTurnos = this.detalleTunoService.findAllDetalleTurnosPorProfesional(profesional.getId());
		
		List<Turno> turnos = turnoService.findAllTurnosReservados();
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		Map<Cliente, ArrayList<Turno>> clientesTurnos = new HashMap<Cliente, ArrayList<Turno>>();
		
		for(Turno t: turnos) {
			if(detalleTurnos.stream().noneMatch(dt -> t.getDetalleTurnos().contains(dt))) {
				
				clientes.add(t.getCliente());
				
				if(clientesTurnos.containsKey(t.getCliente())) {
					ArrayList<Turno> listaTurnosClientes = clientesTurnos.get(t.getCliente());
					listaTurnosClientes.add(t);
					clientesTurnos.replace(t.getCliente(), listaTurnosClientes);
				}else {
					clientesTurnos.put(t.getCliente(), new ArrayList<Turno>());
				}
				
			}
		}
		List<Cliente> clientesDif = clientes.stream().distinct().collect(Collectors.toList());
			
		clientesDif.forEach(cl -> profesionalService.sendMailToClients(cl.getEmail(), clientesTurnos.get(cl)));
	}
	
	@RequestMapping(value= { "/active/{id}" }, method = RequestMethod.GET)
	public String active(@PathVariable("id") int id)
	{
		Profesional profesional = this.profesionalService.findById(id);
		profesional.setEstadoProfesional(EstadoProfesional.ACTIVO);
		profesional.setFechaBaja(null);
		this.profesionalService.update(profesional);
		return "redirect:/profesionales/list";
	}
	
	@ResponseBody
	@RequestMapping(value= {"/test"}, method = RequestMethod.GET)
	public List<Servicio> test() {
		System.out.println(servicioService.findAllServiciosActivos());
		return servicioService.findAllServiciosActivos();
	}
	
	private List<DisponibilidadDTO> cargarDiasEnDTO(){
		List<DisponibilidadDTO> dias = new ArrayList<DisponibilidadDTO>();
		dias.add(new DisponibilidadDTO(Dias.LUNES.toString()));
		dias.add(new DisponibilidadDTO(Dias.MARTES.toString()));
		dias.add(new DisponibilidadDTO(Dias.MIERCOLES.toString()));
		dias.add(new DisponibilidadDTO(Dias.JUEVES.toString()));
		dias.add(new DisponibilidadDTO(Dias.VIERNES.toString()));
		dias.add(new DisponibilidadDTO(Dias.SABADO.toString()));
		dias.add(new DisponibilidadDTO(Dias.DOMINGO.toString()));
		return dias;
	}
	
	public List<DisponibilidadDTO> cargarDatosDisponibilidadDeProfesionales(List<Disponibilidad> disponibilidad){
		List<DisponibilidadDTO> dias = cargarDiasEnDTO();
		
		List<DisponibilidadDTO> listDisponibles = profesionalService.convertDisponibilidadToDTOList(disponibilidad);
		for(DisponibilidadDTO d: listDisponibles) {
			for(int i=0; i<dias.size(); i++) {
				if(dias.get(i).getDia().equals(d.getDia())) {
					dias.get(i).setId(d.getId());
					dias.get(i).setHoraDisponible(d.getHoraDisponible());
				}
			}
		}
		return dias;
	}

	
//	private List<ProfesionalDTO> findAllProfesionalesDTO()
//	{
//		List<ProfesionalDTO> lista = new ArrayList<>();
//		for (Profesional p : this.profesionalService.findAll()) {
//
//			ProfesionalDTO dto = this.profesionalService.convertToDTO(p);
//			lista.add(dto);
//		}
//		return lista;
//	}
}

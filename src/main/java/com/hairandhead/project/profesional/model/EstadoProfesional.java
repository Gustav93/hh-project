package com.hairandhead.project.profesional.model;

public enum EstadoProfesional {
    ACTIVO, INACTIVO, LICENCIA, VACACIONES
}

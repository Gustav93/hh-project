package com.hairandhead.project.profesional.model;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "horarios")
public class Horario implements Comparable<Horario>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column
	private LocalTime horaInicio;
    
    @Column
	private LocalTime horaFin;

    public Horario() {
    	id = null;
		horaInicio = null;
		horaFin = null;
	}
    
	public Horario(LocalTime horaInicio2, LocalTime horaFin2) {
		horaInicio = horaInicio2;
		horaFin = horaFin2;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	@Override
	public int compareTo(Horario arg0) {
		if (getHoraInicio().isBefore(arg0.getHoraInicio()))
			return -1;
		else
			return 1;

	}

	@Override
	public String toString() {
		return "Horario [horaInicio=" + horaInicio + ", horaFin=" + horaFin + "]";
	}
	
	
}

package com.hairandhead.project.profesional.dao;

import com.hairandhead.project.profesional.model.Disponibilidad;

import java.util.List;

public interface DisponibilidadDao {

    Disponibilidad findById(int id);

    void save(Disponibilidad disponibilidad);

    void update(Disponibilidad disponibilidad);

    List<Disponibilidad> findAll();
}

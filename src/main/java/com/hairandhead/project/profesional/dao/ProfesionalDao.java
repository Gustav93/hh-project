package com.hairandhead.project.profesional.dao;

import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Profesional;

import java.util.List;

public interface ProfesionalDao {

    Profesional findById(int id);

    void save(Profesional profesional);

    void update(Profesional profesional);

    List<Profesional> findAll();

    List<Profesional> findByEstado(EstadoProfesional estadoProfesional);
    
    List<Profesional> findAllProfesionalesActivos();
    
    List<Profesional> findAllProfesionalesActivosConServicio(int id);

    Profesional findByEmail(String email);
}

package com.hairandhead.project.profesional.dao;

import com.hairandhead.project.profesional.dao.ProfesionalDao;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.AbstractDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("ProfesionalDao")
public class ProfesionalDaoImpl extends AbstractDao<Integer, Profesional> implements ProfesionalDao {
    @Override
    public Profesional findById(int id) {
        return super.getByKey(id);
    }

    @Override
    public void save(Profesional profesional) {
        super.persist(profesional);
    }
    
    @Override
    public void update(Profesional profesional) {
    	super.update(profesional);
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<Profesional> findAll() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("id"));
		List<Profesional> profesionales = (List<Profesional>) criteria.list();
		
        return profesionales;
    }

    @Override
    public List<Profesional> findByEstado(EstadoProfesional estadoProfesional) {
        Criteria criteria = super.createEntityCriteria();
        criteria.add(Restrictions.eq("estadoProfesional", estadoProfesional));
        return criteria.list();
    }

    @Override
    public Profesional findByEmail(String email) {
        Criteria criteria = super.createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (Profesional) criteria.uniqueResult();
    }
    
	@Override
	public List<Profesional> findAllProfesionalesActivosConServicio(int id) {
			
		Criteria criteria = super.createEntityCriteria();
		criteria.createAlias("servicios", "serv");
		criteria.add(Restrictions.eq("serv.id",id));
	
		List<Profesional> profesionales = criteria.list();
		List<Profesional> ret = new ArrayList<Profesional>();
		for(Profesional p : profesionales) {
			if(p.getEstadoProfesional().equals(EstadoProfesional.ACTIVO)) {
				ret.add(p);
			}
		}
		return ret;
	}

	@Override
	public List<Profesional> findAllProfesionalesActivos() {
		List<Profesional> ret = new ArrayList<Profesional>();
		for(Profesional p : findAll()) {
			if(p.getEstadoProfesional().equals(EstadoProfesional.ACTIVO)) {
				ret.add(p);
			}
		}
		return ret;
	}

}

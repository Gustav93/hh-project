package com.hairandhead.project.turno.service;

import com.hairandhead.project.turno.model.DetalleTurno;

import java.util.List;

public interface DetalleTunoService {
    DetalleTurno findById(int id);

    void save(DetalleTurno detalleTurno);

    void delete(DetalleTurno detalleTurno);

    void delete(int id);

    void update(DetalleTurno detalleTurno);

	List<DetalleTurno> findAllDetalleTurnosPorProfesional(Integer idProfesional);
}

package com.hairandhead.project.turno.service;

import com.hairandhead.project.turno.dao.DetalleTurnoDao;
import com.hairandhead.project.turno.model.DetalleTurno;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("DetalleTurnoService")
public class DetalleTurnoServiceImpl implements DetalleTunoService {

    @Autowired
    private DetalleTurnoDao detalleTurnoDao;

    @Override
    public DetalleTurno findById(int id) {
        return detalleTurnoDao.findById(id);
    }

    @Override
    public void save(DetalleTurno detalleTurno) {
        detalleTurnoDao.save(detalleTurno);
    }

    @Override
    public void delete(DetalleTurno detalleTurno) {
        detalleTurnoDao.delete(detalleTurno);
    }

    @Override
    public void delete(int id) {
        DetalleTurno detalleTurno = findById(id);
        if(detalleTurno!=null)
            detalleTurnoDao.delete(detalleTurno);
    }

    @Override
    public void update(DetalleTurno detalleTurno) {
        detalleTurnoDao.update(detalleTurno);
    }
    
    @Override
    public List<DetalleTurno> findAllDetalleTurnosPorProfesional(Integer idProfesional) {
       return detalleTurnoDao.findAllDetalleTurnosPorProfesional(idProfesional);
    }
    
}

package com.hairandhead.project.turno.service;

import java.io.File;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.turno.dto.EventoDTO;
import com.hairandhead.project.turno.dto.HorarioDTO;
import com.hairandhead.project.turno.dto.DetalleTurnoFrontDTO;
import com.hairandhead.project.turno.dto.TurnoFrontDTO;
import com.hairandhead.project.turno.model.Horario;
import com.hairandhead.project.turno.model.Turno;


public interface TurnoService {
	
	Turno findById(int id);
	void saveTurno(Turno turno);
	void updateTurno(Turno turno);
	void deleteTurnoById(int id);
	List<Turno> findAllTurnos();
	List<Turno> findAllTurnosReservados();
	List<Turno> findAllBetween(LocalDate inicio, LocalDate fin);
	boolean isUnique(Integer id);
	List<EventoDTO> convertTurnoToEventDTO(Turno s);
	void convertToHorarioDTO(Horario horario, HorarioDTO horarioDTO);
	HorarioDTO convertToHorarioDTO(Horario horario);
	TurnoFrontDTO convertToTurnoFrontDTO(Turno turno);
	List<DetalleTurnoFrontDTO> convertToDetalleTurnoFrontDTOList(Turno turno);
	Turno findByEmail(String email);
	Set<Promocion> getPromotionsAvailable(Turno turno);
	void sendReceipt(Integer id, File file);
	List<Turno> getExpiredTurnos();
	List<Turno> findAllBetweenAndSucursalEquals(LocalDate in, LocalDate fn, String sucursal);
}
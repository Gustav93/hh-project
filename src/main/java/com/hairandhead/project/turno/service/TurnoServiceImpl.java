package com.hairandhead.project.turno.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.Duration;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.turno.dto.DetalleTurnoFrontDTO;
import com.hairandhead.project.turno.dto.TurnoFrontDTO;
import org.apache.commons.lang.RandomStringUtils;
import com.hairandhead.project.turno.model.EstadoTurno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hairandhead.project.turno.dao.TurnoDao;
import com.hairandhead.project.turno.dto.EventoDTO;
import com.hairandhead.project.turno.dto.HorarioDTO;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Horario;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.utils.mail.MailService;



@Service("turnoService")
@Transactional
public class TurnoServiceImpl implements TurnoService{

	@Autowired
	private TurnoDao dao;

	@Autowired
	private PromocionService promocionService;
	
	@Autowired
	private MailService mailService;

	@Autowired
	private DataConfigService dataConfigService;

	@Override
	public Turno findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveTurno(Turno turno) {
		dao.save(turno);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	@Override
	public void updateTurno(Turno turno) {
		dao.update(turno);
	}

	@Override
	public void deleteTurnoById(int id) {
		dao.deleteById(id);
	}

	@Override
	public List<Turno> findAllTurnos() {
		return dao.findAllTurnos();
	}

	@Override
	public boolean isUnique(Integer id) {
		Turno turno = findById(id);
		return ( turno == null || ((id != null) && (turno.getId() == id)));
	}

	@Override
	public List<EventoDTO> convertTurnoToEventDTO(Turno source) {
		EventoDTO event;
		List<EventoDTO> target = new ArrayList<>();
		for(DetalleTurno t: source.getDetalleTurnos()){
			event = new EventoDTO();
			event.setId(source.getId());
			event.setTitle(t.getServicio()!=null&&t.getServicio().getNombre()!=null&&
                    t.getServicio().getNombre()!=null&&t.getServicio().getNombre()!=null?
                    t.getServicio().getNombre() + " - " + t.getProfesional().getNombre().concat(" ") + t.getProfesional().getApellido():"");

			event.setStart((source.getFecha()!= null && t.getHoraInicio()!=null) ?
					source.getFecha().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T" +t.getHoraInicio().format(DateTimeFormatter.ofPattern("HH:mm:ss")):"");
			event.setEnd((source.getHoraFin()!=null && t.getHoraFin()!=null) ?
					source.getFecha().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "T" + t.getHoraFin().format(DateTimeFormatter.ofPattern("HH:mm:ss")):"");
			target.add(event);
		}
		return target;
	}

	@Override
	public TurnoFrontDTO convertToTurnoFrontDTO(Turno turno) {
		TurnoFrontDTO target = new TurnoFrontDTO();
		BigDecimal totalConDesc = new BigDecimal("0");
		BigDecimal totalSinDesc = new BigDecimal("0");
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(0);
        target.setTurnoId(turno.getId()!=null?turno.getId().toString():"");
		target.setEmail(turno.getCliente()!=null&&turno.getCliente().getEmail()!=null?turno.getCliente().getEmail():"");
		target.setName(turno.getCliente()!=null&&turno.getCliente().getNombre()!=null?turno.getCliente().getNombre():"");
		target.setSurname(turno.getCliente()!=null&&turno.getCliente().getApellido()!=null?turno.getCliente().getApellido():"");
		target.setStatus(turno.getEstadoTurno() != null ?turno.getEstadoTurno().name():"");
		//Cuento el monto total

		boolean agregoPromo = false;
		boolean aplicoPromo = false;
		for (DetalleTurno dt : turno.getDetalleTurnos()) {
			for (PromocionAplicada promo : turno.getPromociones()) {
				for (ServicioPromocionAplicada serv : promo.getServicios()) {

					if (dt.getServicio().getNombre().equals(serv.getNombre()) && !aplicoPromo) {
						totalConDesc = totalConDesc.add(
								dt.getServicio().getPrecio().subtract(
										dt.getServicio().getPrecio().multiply(promo.getDescuento().divide(new BigDecimal(100)))
						));
						agregoPromo = true;
						aplicoPromo = true;
					}
				}
			}
			if (!agregoPromo) totalConDesc = new BigDecimal(totalConDesc.add(dt.getServicio().getPrecio()).floatValue());
			agregoPromo = false;
			aplicoPromo = false;
		}
		
		for (DetalleTurno detalleTurno : turno.getDetalleTurnos()) {
			if (detalleTurno.getServicio() != null) {
				totalSinDesc = totalSinDesc.add(detalleTurno.getServicio().getPrecio());
			}
		}
		
		System.out.println("SIN DESC"+totalSinDesc);
		System.out.println("CON DESC"+totalConDesc);
		
		//target.setTotalPrice(totalSinDesc.floatValue()+"$");
		//target.setTotalPriceDesc(totalConDesc.floatValue()+"$");
		
		target.setDate(turno.getFecha()!=null? turno.getFecha().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")):"");
		target.setTotalPrice(df.format(totalConDesc)+"$");
		target.setTotalPriceDesc(df.format(totalSinDesc)+"$");
		return target;
	}

	@Override
	public List<DetalleTurnoFrontDTO> convertToDetalleTurnoFrontDTOList(Turno turno) {
		List<DetalleTurnoFrontDTO> target = new ArrayList<>();
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(0);
		if(turno.getDetalleTurnos()!=null){
			for(DetalleTurno detalleTurno : turno.getDetalleTurnos()){
				if(detalleTurno.getServicio()!=null){
					DetalleTurnoFrontDTO detalleTurnoFrontDTO = new DetalleTurnoFrontDTO();
					detalleTurnoFrontDTO.setId(detalleTurno.getId()!=null?detalleTurno.getId().toString():"");
					detalleTurnoFrontDTO.setServiceName(detalleTurno.getServicio().getNombre());
					detalleTurnoFrontDTO.setProfessionalName(detalleTurno.getProfesional()!=null&&detalleTurno.getProfesional().getNombre()!=null? detalleTurno.getProfesional().getNombre():"");
					detalleTurnoFrontDTO.setServiceTime(detalleTurno.getServicio()!=null&&detalleTurno.getServicio().getTiempoPromedioDeDuracion()!=null? Long.valueOf(detalleTurno.getServicio().getTiempoPromedioDeDuracion().toMinutes()).toString():"");
					
					boolean aplicarPromo = false;
					for(PromocionAplicada promo : turno.getPromociones()) {
						for (ServicioPromocionAplicada serv : promo.getServicios()) {
							//Servicio es distinto a ServicioPromocionAplicada
							if(detalleTurno.getServicio().getNombre().equals(serv.getNombre())) {
								String info = detalleTurno.getServicio().getPrecio().doubleValue()+"$"+ " Desc: "+
										promo.getDescuento().toString()+"%" +" ->" + detalleTurno.getServicio().getPrecio()
										.subtract(detalleTurno.getServicio().getPrecio().multiply(promo.getDescuento()).divide(new BigDecimal(100)))
										.doubleValue() +"$";
								
								detalleTurnoFrontDTO.setPrice(info);
								//detalleTurnoFrontDTO.setPrice(detalleTurno.getServicio().getPrecio()!=null?
										//df.format(detalleTurno.getServicio().getPrecio()):"");
								aplicarPromo = true;
							}
						}
					}
					if(!aplicarPromo) {
						detalleTurnoFrontDTO.setPrice(detalleTurno.getServicio().getPrecio()!=null?
								df.format(detalleTurno.getServicio().getPrecio())+"$":"");
					}
					
					
					detalleTurnoFrontDTO.setHoraInicio(detalleTurno.getHoraInicio().format(DateTimeFormatter.ofPattern("HH:mm")));
					detalleTurnoFrontDTO.setHoraFin(detalleTurno.getHoraFin().format(DateTimeFormatter.ofPattern("HH:mm")));
					target.add(detalleTurnoFrontDTO);
				}
			}
		}
		return target;
	}

	@Override
	public Turno findByEmail(String email) {
		return dao.findTurnoByEmail(email);
	}

	@Override
	public Set<Promocion> getPromotionsAvailable(Turno turno) {
		Set<Promocion> promocionList = new HashSet<>();

		for(DetalleTurno detalleTurno : turno.getDetalleTurnos()){
			for(Promocion promo : promocionService.findByEstado(EstadoPromocion.ACTIVO)){
				for(Servicio servicio : promo.getServicios()) {
					if(detalleTurno.getServicio().getId().equals(servicio.getId())&&!isApplied(promo, turno)){
						promocionList.add(promo);
					}
				}
			}
	}
		return promocionList;
	}

	@Override
	public List<Turno> getExpiredTurnos() {
		LocalTime now = LocalTime.now();
		LocalTime end = LocalTime.of(now.getHour(),now.getMinute());
		LocalTime start = end.minusMinutes(setThreshold());
		List<Turno> turnos = findAllTurnos();
		List<Turno> res = new ArrayList<>();
		turnos.forEach(turno->{
			if(turno.getEstadoTurno().equals(EstadoTurno.RESERVADO) && turno.getHoraInicio() != null && 
					!turno.getHoraInicio().isBefore(start) && !turno.getHoraInicio().isAfter(end)){
				res.add(turno);
			}
		});
		return res;
	}

	@Override
	public void convertToHorarioDTO(Horario horario, HorarioDTO horarioDTO) {
		horarioDTO.setHoraFin(horario.getHoraFin().format(DateTimeFormatter.ofPattern("HH:mm")));
		horarioDTO.setHoraInicio(horario.getHoraInicio().format(DateTimeFormatter.ofPattern("HH:mm")));
		horarioDTO.setDisponible(horario.isDisponible());
		horarioDTO.setLibre(horario.isLibre());
		System.out.println(horario);
		System.out.println(horario.getHoraFin());
		System.out.println(horario.getHoraInicio());
		System.out.println(horario.getHoraFin());
		System.out.println(horario.getHoraInicio());
		System.out.println("MINUTOS INICIO: "+ horario.getHoraInicio().getMinute());
		System.out.println("MINUTOS FIN: "+ horario.getHoraFin().getMinute());

		long minutos = Duration.between(horario.getHoraInicio(), horario.getHoraFin()).toMinutes();
		System.out.println("MINUTOS: "+minutos);
		System.out.println(minutos);
		long total = 720;
		long ancho = (minutos * 100) / total;
		System.out.println(ancho);
		String porcentaje = ancho + "%";
		horarioDTO.setPorcentaje(porcentaje);

	}

	@Override
	public HorarioDTO convertToHorarioDTO(Horario horario) {
		HorarioDTO horarioDTO = new HorarioDTO();
		convertToHorarioDTO(horario, horarioDTO);
		return horarioDTO;
	}

	@Override
	public List<Turno> findAllTurnosReservados() {
		// TODO Auto-generated method stub
		return dao.findAllTurnosReservados();
	}
	
	@Override
	public void sendReceipt(Integer id, File file) {
		Turno user = dao.findById(id);
		if(user==null){
			return;
		}
		String subject = "Comprobante";
		String bodyMail = "Estimado "
				.concat(user.getCliente().getNombre() + " " +  user.getCliente().getApellido())
				.concat(", se envia su comprobante solicitado.")
				.concat("\n\n Saludos!");
		
		mailService.sendMailWithAttachment(Collections.singletonList(user.getCliente().getEmail()), subject, bodyMail,Collections.singletonList(file));
	}

	@Override
	public List<Turno> findAllBetween(LocalDate inicio, LocalDate fin) {
		List<Turno> turnos = dao.findAllTurnos();
		List<Turno> ret = new ArrayList<Turno>();
		for(Turno t : turnos) {
			if(( t.getFecha().compareTo(inicio) == 0 ||  t.getFecha().compareTo(inicio) > 0)
				&& (t.getFecha().compareTo(fin) ==0 || t.getFecha().compareTo(fin) < 0)){
				ret.add(t);
			}
		}
		return ret;
	}

	private int setThreshold(){
		DataConfig dataConfig = dataConfigService.findByProperty("treshold.turno.reservation");
		int res = 15;
		String property = dataConfig!=null&&dataConfig.getValue()!=null?dataConfig.getProperty():"";
		try {
			res = Integer.parseInt(property);
		} catch (NumberFormatException e) {
			System.out.println("No se pudo convertir el valor de la property. Error: ".concat(e.getMessage()));
			return res;
		}
		return res;
	}
	
	@Override
	public List<Turno> findAllBetweenAndSucursalEquals(LocalDate in, LocalDate fn, String sucursal) {
		List<Turno> turnos = this.findAllBetween(in, fn);
		List<Turno> ret = new ArrayList<Turno>();
		for(Turno t: turnos) {
			if(t.getSucursal() != null && t.getSucursal().getNombre().compareTo(sucursal)==0) {
				ret.add(t);
			}
		}
		return ret;
	}

	private boolean isApplied(Promocion promo, Turno turno){
		boolean res = false;
		for (PromocionAplicada promocionAplicada : turno.getPromociones()){
			if(promo.getNombre().equals(promocionAplicada.getNombre())){
				return true;
			}
		}
		return res;
	}
}
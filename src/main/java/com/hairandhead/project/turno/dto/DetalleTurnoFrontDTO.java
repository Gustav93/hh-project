package com.hairandhead.project.turno.dto;

import java.util.Objects;

public class DetalleTurnoFrontDTO {
    private String id, serviceName, professionalName, serviceTime, endDate, price, horaInicio, horaFin;

    public DetalleTurnoFrontDTO() {
        this.id = "";
        this.serviceName = "";
        this.professionalName = "";
        this.serviceTime = "";
        this.endDate = "";
        this.price = "";
        this.horaInicio = "";
        this.horaFin = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    
    public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DetalleTurnoFrontDTO that = (DetalleTurnoFrontDTO) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(serviceName, that.serviceName) &&
				Objects.equals(professionalName, that.professionalName) &&
				Objects.equals(serviceTime, that.serviceTime) &&
				Objects.equals(endDate, that.endDate) &&
				Objects.equals(price, that.price) &&
				Objects.equals(horaInicio, that.horaInicio) &&
				Objects.equals(horaFin, that.horaFin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, serviceName, professionalName, serviceTime, endDate, price, horaInicio, horaFin);
	}
}

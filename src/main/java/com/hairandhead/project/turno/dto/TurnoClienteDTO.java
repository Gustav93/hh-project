package com.hairandhead.project.turno.dto;

public class TurnoClienteDTO {
	private String cliente;
	private String fecha;
	//private String hora;
	private String sucursal;
	
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	/*public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}*/
	@Override
	public String toString() {
		return "TurnoClienteDTO [cliente=" + cliente + ", fecha=" + fecha + ", sucursal=" + sucursal + "]";
	}
	
	
	
	
	
}

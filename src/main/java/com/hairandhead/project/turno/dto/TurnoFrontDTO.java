package com.hairandhead.project.turno.dto;

import java.util.Objects;

public class TurnoFrontDTO {

    private String turnoId, name, surname, email, date ,status, totalPrice, totalPriceDesc;

    public TurnoFrontDTO() {
        this.turnoId = "";
        this.name = "";
        this.surname = "";
        this.date = "";
        this.email = "";
        this.status = "";
        this.totalPrice = "";
        this.totalPriceDesc="";
    }

    public String getTurnoId() {
        return turnoId;
    }

    public void setTurnoId(String turnoId) {
        this.turnoId = turnoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
    
	public String getTotalPriceDesc() {
		return totalPriceDesc;
	}

	public void setTotalPriceDesc(String totalPriceDesc) {
		this.totalPriceDesc = totalPriceDesc;
	}

	@Override
	public String toString() {
		return "TurnoFrontDTO [turnoId=" + turnoId + ", name=" + name + ", surname=" + surname + ", email=" + email
				+ ", date=" + date + ", status=" + status + ", totalPrice=" + totalPrice + "]";
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TurnoFrontDTO that = (TurnoFrontDTO) o;
        return Objects.equals(turnoId, that.turnoId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(email, that.email) &&
                Objects.equals(date, that.date) &&
                Objects.equals(status, that.status) &&
                Objects.equals(totalPrice, that.totalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(turnoId, name, surname, email, date, status, totalPrice);
    }
}

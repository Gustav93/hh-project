package com.hairandhead.project.turno.model;

import java.time.LocalTime;

public class Horario implements Comparable<Horario>{
	private LocalTime horaInicio;
	private LocalTime horaFin;
	private boolean disponible;
	private boolean libre;

	public Horario(LocalTime horaInicio2, LocalTime horaFin2) {
		horaInicio = horaInicio2;
		horaFin = horaFin2;
		disponible = true;
		libre = false;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	
	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	@Override
	public int compareTo(Horario arg0) {
		if (getHoraInicio().isBefore(arg0.getHoraInicio()))
			return -1;
		else
			return 1;

	}

	@Override
	public String toString() {
		return "Horario [horaInicio=" + horaInicio + ", horaFin=" + horaFin + "]";
	}
	
	
}

package com.hairandhead.project.user.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	ADMINISTRATIVO("ADMINISTRATIVO"),
	CONTADOR("CONTADOR"),
	ADMIN("ADMIN");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}

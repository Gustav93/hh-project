package com.hairandhead.project.user.dao;

import java.util.List;

import com.hairandhead.project.user.model.UserProfile;

public interface UserProfileDao {

	List<UserProfile> findAll();

	UserProfile findByType(String type);

	UserProfile findById(int id);
}

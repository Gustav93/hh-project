package com.hairandhead.project.reportes.service;

import net.sf.jasperreports.engine.JasperPrint;

public interface ReporteService {
	
	JasperPrint servicios(String nombreSucursal, String inicio, String fin);
	
	JasperPrint profesionales(String nombreSucursal, String inicio, String fin);
	
	JasperPrint recaudacionPorServicio(String nombreSucursal, String inicio, String fin);

}

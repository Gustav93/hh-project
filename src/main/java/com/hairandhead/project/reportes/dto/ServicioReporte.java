package com.hairandhead.project.reportes.dto;

public class ServicioReporte {
	public String nombre;
	public Double ganancia;
	
	public ServicioReporte() {
		this.nombre = "";
		this.ganancia = 0.0;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getGanancia() {
		return ganancia;
	}

	public void setGanancia(Double ganancia) {
		this.ganancia = ganancia;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) return false;
		if(!(o instanceof ServicioReporte)) return false;
		ServicioReporte other = (ServicioReporte) o;
		return this.getNombre().equals(other.getNombre());
	}

	@Override
	public String toString() {
		return "ServicioReporte [nombre=" + nombre + ", ganancia=" + ganancia + "]";
	}
	
	
}

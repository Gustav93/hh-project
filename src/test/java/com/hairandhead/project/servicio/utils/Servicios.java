package com.hairandhead.project.servicio.utils;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;

public class Servicios {

	public static List<Servicio> getListServices()
	{
		List<Servicio> servicios = new ArrayList<Servicio>();
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Servicio s1 = new Servicio();
		s1.setId(1);
		s1.setNombre("Peluquería");
		s1.setDescripcion("blah blah blah");
		s1.setPrecio(new BigDecimal("100"));
		try{
			s1.setTiempoPromedioDeDuracion(Duration.parse("1:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		s1.setEstado(EstadoServicio.ACTIVO);
		s1.setFechaDeBaja(null);
		
		Servicio s2 = new Servicio();
		s2.setId(2);
		s2.setNombre("Depilación, El Diego");
		s2.setDescripcion("eeeeeeeeeeehhhhh");
		s2.setPrecio(new BigDecimal("200"));
		try{
			s2.setTiempoPromedioDeDuracion(Duration.parse("2:00"));
			s2.setFechaDeBaja(LocalDateTime.parse("2019-10-01T14:00:00"));//format.parse("2019-10-01 14:00:00"));
		}
		catch(DateTimeParseException e){
	        e.getMessage();
//	        return null;
	    }
		s2.setEstado(EstadoServicio.INACTIVO);
		
		
		Servicio s3 = new Servicio();
		s3.setId(3);
		s3.setNombre("Barbería");
		s3.setDescripcion("sarazasarazasaraza");
		s3.setPrecio(new BigDecimal("150"));
		s3.setEstado(EstadoServicio.ACTIVO);
		s3.setFechaDeBaja(null);
	
		servicios.add(s1);
		servicios.add(s2);
		servicios.add(s3);
		return servicios;
	}
	
	public static List<ServicioDTO> getListServicesDTO()
	{
		List<ServicioDTO> serviciosDTO = new ArrayList<ServicioDTO>();
		
		ServicioDTO s1 = new ServicioDTO();
		s1.setId(1);
		s1.setNombre("Peluquería");
		s1.setDescripcion("blah blah blah");
		s1.setPrecio("100");
		
		s1.setTiempoPromedioDeDuracion("01:00");
		s1.setEstado("ACTIVO");
		s1.setFechaDeBaja(null);
		
		ServicioDTO s2 = new ServicioDTO();
		s2.setId(2);
		s2.setNombre("Depilación, El Diego");
		s2.setDescripcion("eeeeeeeeeeehhhhh");
		s2.setPrecio("200");
		s2.setTiempoPromedioDeDuracion("02:00");
		s2.setEstado("INACTIVO");
		s2.setFechaDeBaja("2019-10-01 14:00:00");
		
		ServicioDTO s3 = new ServicioDTO();
		s3.setId(3);
		s3.setNombre("Barbería");
		s3.setDescripcion("sarazasarazasaraza");
		s3.setPrecio("150");
		s3.setTiempoPromedioDeDuracion("01:30");
		s3.setEstado("ACTIVO");
		s3.setFechaDeBaja(null);
		
		serviciosDTO.add(s1);
		serviciosDTO.add(s2);
		serviciosDTO.add(s3);
		return serviciosDTO;
	}
}

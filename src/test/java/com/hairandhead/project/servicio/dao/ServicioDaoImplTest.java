package com.hairandhead.project.servicio.dao;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.utils.Servicios;
import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class ServicioDaoImplTest extends EntityDaoImplTest {

	@Autowired
	ServicioDao servicioDao;
		
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Servicio.xml"));
		
		return dataSet;
	}
	/*
	@BeforeMethod
	public void cargarBD() throws Exception {
		super.setUp();
		System.out.println("Hola");
		Servicio s1 = new Servicio();
		//s1.setId(1);
		s1.setNombre("Tintura");
		s1.setPrecio(new BigDecimal(1230));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(120));
		s1.setDescripcion("Tintura colores premiun unisex");
		
		servicioDao.save(s1);
		for(Servicio s : servicioDao.findAll()) {
			System.out.println(s.getId());
		}
	
	}
	*/
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(servicioDao.findById(1));
		Assert.assertNull(servicioDao.findById(3));
	}
	
	@Test
	public void findAllTest() {
		Assert.assertEquals(servicioDao.findAll().size(), 1);
	}
	
	@Test
	public void saveTest() {
		Servicio servicio = new Servicio();
		servicio.setDescripcion("Corte de cabello por estilista Belga");
		servicio.setEstado(EstadoServicio.INACTIVO);
		servicio.setFechaDeBaja(LocalDateTime.now());
		servicio.setNombre("Corte estilista");
		servicio.setPrecio(new BigDecimal(1700.50));
		servicio.setTiempoPromedioDeDuracion(Duration.ofMinutes(30));
		
		servicioDao.save(servicio);

		Assert.assertEquals(servicioDao.findAll().size(), 2);
	}
	
	@Test
	public void updateTest() {
		Servicio servicio = servicioDao.findById(1);
		servicio.setEstado(EstadoServicio.INACTIVO);
		servicio.setPrecio(new BigDecimal(10000));
		
		servicioDao.update(servicio);
		
		Assert.assertEquals(servicioDao.findById(1).getEstado(), EstadoServicio.INACTIVO);

		Assert.assertNotEquals(servicioDao.findById(1).getPrecio(), new BigDecimal(0));
	}

}

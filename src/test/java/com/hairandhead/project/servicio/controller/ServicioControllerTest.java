package com.hairandhead.project.servicio.controller;


import static org.mockito.Mockito.*;


import java.util.ArrayList;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.servicio.utils.Servicios;

public class ServicioControllerTest {

	@Mock
    ServicioService service;
     
//    @Mock
//    MessageSource message;
     
    @InjectMocks
    ServicioController servicioController;
     
    @Spy
    List<Servicio> servicios = new ArrayList<Servicio>();
    
    @Spy
    List<ServicioDTO> serviciosDTO = new ArrayList<ServicioDTO>();
 
    @Spy
    ModelMap model;
     
    @Mock
    BindingResult result;
    
    @BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		servicios = Servicios.getListServices();
		serviciosDTO = Servicios.getListServicesDTO();
	}
    
    @Test
    public void listServiciosTest(){
        when(service.findAllServicios()).thenReturn(servicios);
        Assert.assertEquals(servicioController.list(model), "service/listServices");
//        Assert.assertEquals(model.get("serviceDTO"), null);
        verify(service, atLeastOnce()).findAllServicios();
    }
    
    @Test
    public void createServicioTest(){
        Assert.assertEquals(servicioController.create(model), "service/formservice");
        Assert.assertNotNull(model.get("serviceDTO"));
        Assert.assertFalse((Boolean)model.get("edit"));
    }
    
    @Test
    public void saveServicioTest(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).saveServicio(any(Servicio.class));
        Assert.assertEquals(servicioController.save(serviciosDTO.get(0),result, model), "redirect:/servicios/list");
    }
    
    @Test
    public void saveServicioErrorTest(){
        when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).saveServicio(any(Servicio.class));
        Assert.assertEquals(servicioController.save(serviciosDTO.get(0),result, model), "servicice/formservice");
    }
    
    @Test
    public void editServicioTest(){
        when(service.findServicioById(anyInt())).thenReturn(this.servicios.get(0));
        Assert.assertEquals(servicioController.edit((anyInt()), model), "service/formservice");
        Assert.assertNotNull(model.get("serviceDTO"));
        Assert.assertTrue((Boolean)model.get("edit"));
    }
    
    @Test
    public void updateServiceTest(){
    	when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).updateServicio(any(Servicio.class));
        Assert.assertEquals(servicioController.updateServicio(serviciosDTO.get(0),result, model), "redirect:/servicios/list");
    }
    
    @Test
    public void updateServiceErrorTest(){
    	when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).updateServicio(any(Servicio.class));
        Assert.assertEquals(servicioController.updateServicio(serviciosDTO.get(0),result, model), "servicice/formservice");
    }
    
    @Test
    public void deleteServicioTest(){
    	Servicio s = servicios.get(0);
        when(service.findServicioById(anyInt())).thenReturn(s);
        Assert.assertEquals(servicioController.delete(anyInt(),model), "redirect:/servicios/list");
        Assert.assertEquals(model.get("mensaje"), "El servicio fue eliminado");
    }
    
    @Test
    public void activeServicioTest(){
    	Servicio s = servicios.get(0);
        when(service.findServicioById(anyInt())).thenReturn(s);
        Assert.assertEquals(servicioController.active(anyInt(),model), "redirect:/servicios/list");
        Assert.assertEquals(model.get("mensaje"), "El servicio fue activado");
    }
    
    @Test
    public void datailTest() {
    	Servicio s = servicios.get(0);
        when(service.findServicioById(anyInt())).thenReturn(s);
        Assert.assertEquals(servicioController.detail(anyInt(),model), "service/detalleServicio");
        Assert.assertNotNull(model.get("servicioDTO"));
    }
    
}

package com.hairandhead.project.servicio.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.servicio.dao.ServicioDao;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.utils.Servicios;


public class ServicioServiceImplTest{

	@Mock
	@Autowired
	ServicioDao dao;
	
	@InjectMocks
	ServicioServiceImpl servicioService;
	
	@Spy
	List<Servicio> servicios = new ArrayList<Servicio>();
	
	@Spy
	List<ServicioDTO> serviciosDTO = new ArrayList<ServicioDTO>();
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		servicios = Servicios.getListServices();
		serviciosDTO = Servicios.getListServicesDTO();
	}
	
	@Test
	public void findByIdTest()
	{
		Servicio s = this.servicios.get(0);
		when(dao.findById(anyInt())).thenReturn(s);
		Assert.assertEquals(servicioService.findServicioById(s.getId()),s);
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getEstado(),s.getEstado());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getNombre(),s.getNombre());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getDescripcion(),s.getDescripcion());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getFechaDeBaja(),s.getFechaDeBaja());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getPrecio(),s.getPrecio());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).getTiempoPromedioDeDuracion(),s.getTiempoPromedioDeDuracion());
		Assert.assertEquals(servicioService.findServicioById(s.getId()).toString(),s.toString());
	}
	
	@Test
	public void getEquals()
	{
		Assert.assertTrue(this.servicios.get(0).equals(this.servicios.get(0)));
		Assert.assertFalse(this.servicios.get(0).equals(this.servicios.get(1)));
	}
	
	@Test
	public void saveServicioTest()
	{
		doNothing().when(dao).save(any(Servicio.class));
		servicioService.saveServicio(any(Servicio.class));
		verify(dao, atLeastOnce()).save(any(Servicio.class));
	}
	
	@Test
    public void updateServicioTest()
    {
		
		doNothing().when(dao).save(any(Servicio.class));
        servicioService.updateServicio(any(Servicio.class));
        verify(dao, atLeastOnce()).update(any(Servicio.class));
    }
	
	@Test
	public void findAllServiciosTest()
	{
		 when(dao.findAll()).thenReturn(this.servicios);
		 Assert.assertEquals(this.servicioService.findAllServicios(), servicios);
	}
	
	@Test
	public void convertToDTOTest()
	{
		ServicioDTO s = this.serviciosDTO.get(1);
		servicioService.convertToDTO(this.servicios.get(1), s);
		Assert.assertEquals(this.serviciosDTO.get(1),s);
	}
	
	@Test
	public void convertToDTOAtributosNullTest()
	{
		ServicioDTO dto = this.serviciosDTO.get(0);
		dto.setPrecio("");
		dto.setTiempoPromedioDeDuracion("");
		dto.setFechaDeBaja(null);
		dto.setPrecio(null);
		Servicio s = this.servicios.get(0);
		s.setPrecio(null);
		s.setTiempoPromedioDeDuracion(null);
		s.setFechaDeBaja(null);
		servicioService.convertToDTO(s, dto);
		Assert.assertEquals(this.serviciosDTO.get(0),dto);
	}
	
	@Test
	public void convertToServicioTest()
	{
		Servicio s = new Servicio();
		servicioService.convertToServicio(this.serviciosDTO.get(0), s);
		Assert.assertEquals(this.servicios.get(0),s);
	}
	
//	@Test
//	public void convertToServicioAtributosNullTest()
//	{
//		ServicioDTO dto = this.serviciosDTO.get(2);
//		dto.setPrecio("");
//		dto.setTiempoPromedioDeDuracion("");
//		dto.setFechaDeBaja(null);
////		dto.setPrecio(null);
//		Servicio s = this.servicios.get(2);
////		s.setPrecio(null);
//		s.setTiempoPromedioDeDuracion(null);
//		s.setFechaDeBaja(null);
//		servicioService.convertToServicio(dto, s);
//		Assert.assertEquals(this.servicios.get(2),s);
//	}
	
	@Test
	public void hascodeTest() {
		Assert.assertEquals(this.serviciosDTO.get(0).hashCode(),this.serviciosDTO.get(0).hashCode());
		Assert.assertEquals(this.serviciosDTO.get(0).toString(),this.serviciosDTO.get(0).toString());
		this.serviciosDTO.get(0).setId(null);
		this.serviciosDTO.get(0).setDescripcion(null);
		this.serviciosDTO.get(0).setEstado(null);
		this.serviciosDTO.get(0).setFechaDeBaja("23/08/1988");
		this.serviciosDTO.get(0).setNombre(null);
		this.serviciosDTO.get(0).setPrecio(null);
		this.serviciosDTO.get(0).setTiempoPromedioDeDuracion(null);
		Assert.assertEquals(this.serviciosDTO.get(0).hashCode(),this.serviciosDTO.get(0).hashCode());
	}
	
	@Test
	public void ServicioDTOequalsTest() {
		ServicioDTO s1 = new ServicioDTO();
		ServicioDTO s2 = null;
		Assert.assertFalse(s1.equals(s2));
		
		s2 = new ServicioDTO();
		s1.setDescripcion(null);
		s2.setDescripcion(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setDescripcion(null);
		s2.setDescripcion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setDescripcion("d1");
		s2.setDescripcion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setDescripcion("d");
		s2.setDescripcion("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setEstado(null);
		s2.setEstado(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setEstado(null);
		s2.setEstado("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setEstado("d1");
		s2.setEstado("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setEstado("d");
		s2.setEstado("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setFechaDeBaja(null);
		s2.setFechaDeBaja(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setFechaDeBaja(null);
		s2.setFechaDeBaja("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setFechaDeBaja("d1");
		s2.setFechaDeBaja("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setFechaDeBaja("d");
		s2.setFechaDeBaja("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setId(null);
		s2.setId(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setId(null);
		s2.setId(2);
		Assert.assertFalse(s1.equals(s2));
		s1.setId(1);
		s2.setId(2);
		Assert.assertFalse(s1.equals(s2));
		s1.setId(3);
		s2.setId(3);
		Assert.assertTrue(s1.equals(s2));
		
		s1.setNombre(null);
		s2.setNombre(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setNombre(null);
		s2.setNombre("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setNombre("d1");
		s2.setNombre("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setNombre("d");
		s2.setNombre("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setPrecio(null);
		s2.setPrecio(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setPrecio(null);
		s2.setPrecio("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setPrecio("d1");
		s2.setPrecio("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setPrecio("d");
		s2.setPrecio("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setTiempoPromedioDeDuracion(null);
		s2.setTiempoPromedioDeDuracion(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion(null);
		s2.setTiempoPromedioDeDuracion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion("d1");
		s2.setTiempoPromedioDeDuracion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion("d");
		s2.setTiempoPromedioDeDuracion("d");
		Assert.assertTrue(s1.equals(s2));
	}
	
	@Test
	public void ServicioequalsTest() {
		Servicio s1 = new Servicio();
		Servicio s2 = null;
		Assert.assertFalse(s1.equals(s2));
		
		s2 = new Servicio();
		s1.setDescripcion(null);
		s2.setDescripcion(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setDescripcion(null);
		s2.setDescripcion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setDescripcion("d1");
		s2.setDescripcion("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setDescripcion("d");
		s2.setDescripcion("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setEstado(null);
		s2.setEstado(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setEstado(null);
		s2.setEstado(EstadoServicio.INACTIVO);
		Assert.assertFalse(s1.equals(s2));
		s1.setEstado(EstadoServicio.ACTIVO);
		s2.setEstado(EstadoServicio.INACTIVO);
		Assert.assertFalse(s1.equals(s2));
		s1.setEstado(EstadoServicio.ACTIVO);
		s2.setEstado(EstadoServicio.ACTIVO);
		Assert.assertTrue(s1.equals(s2));
		
		s1.setFechaDeBaja(null);
		s2.setFechaDeBaja(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setFechaDeBaja(null);
		s2.setFechaDeBaja(LocalDateTime.now());
		Assert.assertFalse(s1.equals(s2));
		s1.setFechaDeBaja(LocalDateTime.now());
		s2.setFechaDeBaja(LocalDateTime.now().plusDays(10));
		Assert.assertFalse(s1.equals(s2));
		s1.setFechaDeBaja(LocalDateTime.now());
		s2.setFechaDeBaja(s1.getFechaDeBaja());
		Assert.assertTrue(s1.equals(s2));
		
		s1.setId(null);
		s2.setId(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setId(null);
		s2.setId(2);
		Assert.assertFalse(s1.equals(s2));
		s1.setId(1);
		s2.setId(2);
		Assert.assertFalse(s1.equals(s2));
		s1.setId(3);
		s2.setId(3);
		Assert.assertTrue(s1.equals(s2));
		
		s1.setNombre(null);
		s2.setNombre(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setNombre(null);
		s2.setNombre("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setNombre("d1");
		s2.setNombre("d2");
		Assert.assertFalse(s1.equals(s2));
		s1.setNombre("d");
		s2.setNombre("d");
		Assert.assertTrue(s1.equals(s2));
		
		s1.setPrecio(null);
		s2.setPrecio(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setPrecio(null);
		s2.setPrecio(new BigDecimal("17"));
		Assert.assertFalse(s1.equals(s2));
		s1.setPrecio(new BigDecimal("32"));
		s2.setPrecio(new BigDecimal("17"));
		Assert.assertFalse(s1.equals(s2));
		s1.setPrecio(new BigDecimal("32"));
		s2.setPrecio(new BigDecimal("32"));
		Assert.assertTrue(s1.equals(s2));
		
		s1.setTiempoPromedioDeDuracion(null);
		s2.setTiempoPromedioDeDuracion(null);
		Assert.assertTrue(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion(null);
		s2.setTiempoPromedioDeDuracion(Duration.ZERO);
		Assert.assertFalse(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion(Duration.ofHours(1));
		s2.setTiempoPromedioDeDuracion(Duration.ZERO);
		Assert.assertFalse(s1.equals(s2));
		s1.setTiempoPromedioDeDuracion(Duration.ZERO);
		s2.setTiempoPromedioDeDuracion(Duration.ZERO);
		Assert.assertTrue(s1.equals(s2));
	}
}

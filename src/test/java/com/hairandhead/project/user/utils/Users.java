package com.hairandhead.project.user.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hairandhead.project.user.model.EstadoUser;
import com.hairandhead.project.user.model.PersistentLogin;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.user.model.UserProfile;
import com.hairandhead.project.user.model.UserProfileType;

public class Users {

	public static List<User> users(){
		List<User> lista = new ArrayList<>();
		
		User u1 = new User();
		u1.setId(1);
		u1.setFirstName("Juan Alberto");
		u1.setLastName("Choteli");
		u1.setPassword("123");
		u1.setEmail("choteli@gmail.com");
		u1.setSsoId("choteli");
		u1.setEstado(EstadoUser.ACTIVO);
		u1.setFechaBaja(null);
		
		User u2 = new User();
		u2.setId(2);
		u2.setFirstName("Gonzalo");
		u2.setLastName("Martinez");
		u2.setPassword("123");
		u2.setEmail("gmp@gmail.com");
		u2.setSsoId("pity");
		u2.setEstado(EstadoUser.INACTIVO);
		u2.setFechaBaja(LocalDateTime.parse("2019-10-10T11:25"));
		
		lista.add(u1);
		lista.add(u2);
		
		return lista;
	}
	
	public static List<UserProfile> userProfiles(){
		List<UserProfile> lista = new ArrayList<UserProfile>();
		
		UserProfile up1 = new UserProfile();
		up1.setId(1);
		up1.setType(UserProfileType.ADMINISTRATIVO.getUserProfileType());
		
		UserProfile up2 = new UserProfile();
		up2.setId(2);
		up2.setType(UserProfileType.CONTADOR.getUserProfileType());
		
		UserProfile up3 = new UserProfile();
		up3.setId(3);
		up3.setType(UserProfileType.ADMIN.getUserProfileType());
		
		lista.add(up1);
		lista.add(up2);
		lista.add(up3);
		
		return lista;
	}
	
	public static List<PersistentLogin> persistentLogins(){
		List<PersistentLogin> lista = new ArrayList<PersistentLogin>();
		
		PersistentLogin pl1 = new PersistentLogin();
		pl1.setSeries("one");
		pl1.setToken("token1");
		pl1.setUsername("choteli");
		pl1.setLast_used(new Date());
		
		PersistentLogin pl2 = new PersistentLogin();
		pl2.setSeries("two");
		pl2.setToken("token2");
		pl2.setUsername("pity");
		pl2.setLast_used(new Date());
		
		lista.add(pl1);
		lista.add(pl2);
		return lista;
	}
}

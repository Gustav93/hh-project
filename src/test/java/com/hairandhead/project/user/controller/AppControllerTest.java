package com.hairandhead.project.user.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.user.model.PersistentLogin;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.user.model.UserProfile;
import com.hairandhead.project.user.service.UserProfileService;
import com.hairandhead.project.user.service.UserService;
import com.hairandhead.project.user.utils.Users;

public class AppControllerTest {

	@Mock
    UserService service;
	
	@Mock
    UserProfileService profileService;
     
    @Mock
    MessageSource message;
     
    @InjectMocks
    AppController appController;
     
    @Spy
    List<User> users = new ArrayList<User>();
    
    @Spy
    List<PersistentLogin> persistentLogins = new ArrayList<PersistentLogin>();
    
    @Spy
    List<UserProfile> userProfiles = new ArrayList<UserProfile>();
 
    @Spy
    ModelMap model;
    
    @Mock
	AuthenticationTrustResolver authenticationTrustResolver;
     
    @Mock
    BindingResult result;
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.users = Users.users();
        this.userProfiles = Users.userProfiles();
        this.persistentLogins = Users.persistentLogins();
        
        User applicationUser = mock(User.class);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(applicationUser);
    }
    
    @Test
    public void listUsersTest() {
        when(service.findAllUsers()).thenReturn(this.users);
        Assert.assertEquals(appController.listUsers(model), "newuserslist");
        Assert.assertEquals(model.get("users"), users);
        verify(service, atLeastOnce()).findAllUsers();
    }
    
    @Test
    public void newUserTest(){
        Assert.assertEquals(appController.newUser(model), "newregistration");
        Assert.assertNotNull(model.get("user"));
        Assert.assertFalse((Boolean)model.get("edit"));
        Assert.assertNotNull(model.get("loggedinuser"));
        Assert.assertNotNull(model.get("roles"));
    }
    
    @Test
    public void saveUserWithValidationError(){
        when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).saveUser(any(User.class));
        Assert.assertEquals(appController.saveUser(this.users.get(0), result, model), "registration");
    }
    
    @Test
    public void saveUserWithValidationErrorNonUniqueSSN(){
        when(result.hasErrors()).thenReturn(false);
        when(service.isUserSSOUnique(anyInt(), anyString())).thenReturn(false);
        Assert.assertEquals(appController.saveUser(users.get(0), result, model), "newregistration");
    }
    
    @Test
    public void saveUserWithSuccess(){
        when(result.hasErrors()).thenReturn(false);
        when(service.isUserSSOUnique(anyInt(), anyString())).thenReturn(true);
        doNothing().when(service).saveUser(any(User.class));
        Assert.assertEquals(appController.saveUser(users.get(0), result, model), "redirect:/user/list");
        Assert.assertEquals(model.get("success"), "User Juan Alberto Choteli registered successfully");
        Assert.assertNotNull(model.get("loggedinuser"));
    }
    
    @Test
    public void editUserTest(){
        User u = users.get(0);
        when(service.findBySSO(anyString())).thenReturn(u);
        Assert.assertEquals(appController.editUser(anyString(), model), "newregistration");
        Assert.assertNotNull(model.get("user"));
        Assert.assertTrue((Boolean)model.get("edit"));
        Assert.assertNotNull(model.get("loggedinuser"));
    }
    
    @Test 
    public void updateUserErrorTest() {
    	when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).updateUser(any(User.class));
        Assert.assertEquals(appController.updateUser(users.get(0), result, model,""), "newregistration");
    }
    
    @Test
    public void updateUserWithSuccess(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).updateUser(any(User.class));
        Assert.assertEquals(appController.updateUser(users.get(0), result, model, ""), "redirect:/user/list");
        Assert.assertEquals(model.get("success"), "User Juan Alberto Choteli updated successfully");
        Assert.assertNotNull(model.get("loggedinuser"));
    }
    
    @Test
    public void deleteEmployee(){
        doNothing().when(service).deleteUser(anyString());
        Assert.assertEquals(appController.deleteUser("choteli"), "redirect:/user/list");
    }
    
    @Test
    public void refreshPasswordFormTest() {
    	Assert.assertEquals(appController.refreshPasswordForm(), "refreshpassform");
    }
    
    @Test
    public void refreshPasswordNullTest() {
    	when(service.findByEmail(anyString())).thenReturn(null);
    	Assert.assertEquals(appController.refreshPassword(anyString()), "redirect:/user/refreshpassform?badEmail");
    }
    
    @Test
    public void refreshPasswordTest() {
    	when(service.findByEmail(anyString())).thenReturn(this.users.get(0));
    	Assert.assertEquals(appController.refreshPassword(anyString()), "redirect:/user/login?refreshOk");
    }
    
    @Test
    public void initializeProfilesTest() {
    	when(profileService.findAll()).thenReturn(this.userProfiles);
    	Assert.assertEquals(appController.initializeProfiles(), this.userProfiles);
    }
    
    @Test 
    public void accessDeniedPageTest() {
    	Assert.assertEquals(appController.accessDeniedPage(model), "accessDenied");
    	Assert.assertNotNull(model.get("loggedinuser"));
    }
    
    @Test
    public void loginPageTrueTest() {
    	when(authenticationTrustResolver.isAnonymous(any(Authentication.class))).thenReturn(true);
    	when(appController.isCurrentAuthenticationAnonymous()).thenReturn(true);
    	Assert.assertEquals(appController.loginPage(), "signin");
    }
    
    @Test
    public void loginPageFalseTest() {
    	when(authenticationTrustResolver.isAnonymous(any(Authentication.class))).thenReturn(false);
    	when(appController.isCurrentAuthenticationAnonymous()).thenReturn(false);
    	Assert.assertEquals(appController.loginPage(), "redirect:/turnos/list");
    }
    
//    @Test
//    public void logoutPageTest() {
//    	when(SecurityContextHolder.getContext().getAuthentication() != null).thenReturn(false);
//    	Assert.assertEquals(appController.logoutPage(request,response), "redirect:/user/login?logout");
//    }
}

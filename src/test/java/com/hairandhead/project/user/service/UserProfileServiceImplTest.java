package com.hairandhead.project.user.service;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.user.dao.UserProfileDao;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.user.model.UserProfile;
import com.hairandhead.project.user.utils.Users;

public class UserProfileServiceImplTest {

	@Mock
    UserProfileDao dao;
	
    @InjectMocks
    UserProfileServiceImpl userProfileService;
     
    @Spy
    List<User> users = new ArrayList<User>();
    
    @Spy
    List<UserProfile> userProfiles = new ArrayList<UserProfile>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.userProfiles = Users.userProfiles();
    }
    
    @Test
    public void findByIdTest(){
    	UserProfile up = this.userProfiles.get(0);
        when(dao.findById(anyInt())).thenReturn(up);
        Assert.assertEquals(userProfileService.findById(up.getId()),up);
    }
    
    @Test
    public void findTypeTest(){
    	UserProfile up = this.userProfiles.get(0);
        when(dao.findByType(anyString())).thenReturn(up);
        Assert.assertEquals(userProfileService.findByType(up.getType()),up);
    }
    
    @Test
    public void findAllTest(){
        when(dao.findAll()).thenReturn(this.userProfiles);
        Assert.assertEquals(userProfileService.findAll(), this.userProfiles);
    }
	
}

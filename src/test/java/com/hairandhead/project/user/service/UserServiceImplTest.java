package com.hairandhead.project.user.service;


import static org.mockito.Matchers.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.user.dao.UserDao;
import com.hairandhead.project.user.model.PersistentLogin;
import com.hairandhead.project.user.model.User;
import com.hairandhead.project.user.model.UserProfile;
import com.hairandhead.project.user.utils.Users;
import com.hairandhead.project.utils.mail.MailService;

public class UserServiceImplTest {

	@Mock
    UserDao dao;
	
	@Mock
    private PasswordEncoder passwordEncoder;

	@Mock
	private MailService mailService;
     
    @InjectMocks
    UserServiceImpl userService;
     
    @Spy
    List<User> users = new ArrayList<User>();
    
    @Spy
    List<PersistentLogin> persistentLogins = new ArrayList<PersistentLogin>();
    
    @Spy
    List<UserProfile> userProfiles = new ArrayList<UserProfile>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.users = Users.users();
        this.userProfiles = Users.userProfiles();
        this.persistentLogins = Users.persistentLogins();
    }
    
    @Test
    public void findByIdTest(){
        User u = users.get(0);
        when(dao.findById(anyInt())).thenReturn(u);
        Assert.assertEquals(userService.findById(u.getId()),u);
    }
    
    @Test
    public void findUserBySsoTest(){
    	User u = users.get(0);
        when(dao.findBySSO(anyString())).thenReturn(u);
        Assert.assertEquals(userService.findBySSO(anyString()), u);
    }
    
    @Test
    public void findUserByEmailTest(){
    	User u = users.get(0);
        when(dao.findByEmail(anyString())).thenReturn(u);
        Assert.assertEquals(userService.findByEmail(anyString()), u);
    }
    
    @Test
    public void saveUserTest(){
        User u = new User();
        when(passwordEncoder.encode("sdffg")).thenReturn("dfgdfg");
        doNothing().when(dao).save(u);
        userService.saveUser(u);
    }
    
    @Test(expectedExceptions = NullPointerException.class)
    public void updateUserTest() {
    	User u = users.get(0);
    	u.setId(1);
        when(dao.findById(1)).thenReturn(u);
        userService.updateUser(u);
    }
    
    @Test
    public void updateUserInactivoTest() {
    	User u = users.get(1);
        when(dao.findById(anyInt())).thenReturn(u);
        userService.updateUser(u);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void updateUserNullTest() {
    	User u = users.get(0);
        when(dao.findById(anyInt())).thenReturn(null);
        userService.updateUser(u);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void deleteUserBySsoTest(){
        doNothing().when(dao).deleteBySSO(anyString());
        userService.deleteUserBySSO(anyString());
        verify(dao, atLeastOnce()).deleteBySSO(anyString());
    }
    
    @Test
    public void findAllUsersTest(){
        when(dao.findAllUsers()).thenReturn(this.users);
        Assert.assertEquals(userService.findAllUsers(), this.users);
    }
    
    @Test
    public void isEmployeeSsnUniqueTest(){
        User u = users.get(0);
        when(dao.findBySSO(anyString())).thenReturn(u);
        Assert.assertEquals(userService.isUserSSOUnique(u.getId(), u.getSsoId()), true);
    }
    
    @Test
    public void isEmployeeSsnUniqueFalseTest(){
        User u = users.get(0);
        when(dao.findBySSO(anyString())).thenReturn(u);
        Assert.assertEquals(userService.isUserSSOUnique(5, u.getSsoId()), false);
    }
    
    @Test
    public void deleteEmployeeBySsnID(){
    	User u = users.get(0);
    	when(dao.findBySSO(anyString())).thenReturn(u);
        doNothing().when(dao).deleteBySSO(anyString());
        userService.deleteUser(u.getSsoId());

    }
    
    @Test(expectedExceptions = NullPointerException.class)
    public void refreshPasswordTest() {
    	User u = users.get(0);


        when(dao.findById(6)).thenReturn(u);
        when(dao.findByEmail("asdf")).thenReturn(u);
        when(passwordEncoder.encode("sdffg")).thenReturn("dfgdfg");
        doNothing().when(mailService).sendMail(new ArrayList<>(),"","");
        userService.refreshPassword(anyString());
    }
}

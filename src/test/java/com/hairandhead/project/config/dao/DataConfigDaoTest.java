package com.hairandhead.project.config.dao;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.utils.EntityDaoImplTest;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DataConfigDaoTest extends EntityDaoImplTest {

    @Autowired
    DataConfigDao dataConfigDao;

    @Override
    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("DataConfig.xml"));
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(dataConfigDao.findById(1));
        Assert.assertNull(dataConfigDao.findById(3));
    }

    @Test
    public void findByPropertyTest() {
        DataConfig dataConfig = dataConfigDao.findByProperty("property");
        Assert.assertEquals(dataConfig, dataConfigDao.findById(1));

    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(dataConfigDao.findAll().size(), 1);
    }

    @Test
    public void deleteTest() {
        DataConfig dataConfig = new DataConfig();
        dataConfig.setId(1);
        dataConfigDao.delete(dataConfig);
        Assert.assertEquals(dataConfigDao.findAll().size(), 0);
    }

    @Test
    public void saveTest() {
        DataConfig dataConfig = new DataConfig();
        dataConfig.setName("asd");
        dataConfig.setProperty("asf");
        dataConfig.setValue("asdf");
        dataConfigDao.save(dataConfig);
        Assert.assertEquals(dataConfigDao.findAll().size(), 2);
    }

    @Test
    public void updateTest(){
        DataConfig dataConfig = dataConfigDao.findById(1);
        dataConfig.setName("test");
        dataConfigDao.update(dataConfig);
        Assert.assertEquals(dataConfig.getName(),"test");
    }
}

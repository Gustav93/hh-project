package com.hairandhead.project.pago.dao;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.pago.utils.Pagos;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class PagoDaoImplTest extends EntityDaoImplTest{
	
	@Autowired
	PagoDao pagoDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Pago.xml"));
		return dataSet;
	}
	
	@Test
	public void buscarPorIdTest() {
		Assert.assertNotNull(pagoDao.buscarPorId(1));
	}
	
	@Test
	public void obtenerPagosTest() {
		Assert.assertEquals(pagoDao.obtenerPagos().size(), 1);
	}
	
	@Test
	public void guardarTest() {
		Pago p = new Pago();
		p.setEstado(EstadoPago.PAGADO);
		p.setFecha(LocalDate.now());
		p.setMontoAbonado(new BigDecimal(1200));
		p.setMontoTotal(new BigDecimal(1200));
		DetallePago detallePago = new DetallePago();
		detallePago.setFecha(LocalDateTime.now());
		detallePago.setMonto(new BigDecimal(1200));
		detallePago.setTipo(TipoDetallePago.EFECTIVO);
		
		pagoDao.guardar(p);
		
		Assert.assertEquals(pagoDao.obtenerPagos().size(), 2);
		
	}
	
	@Test
	public void editarTest() {
		Pago p = pagoDao.buscarPorId(1);
		p.setEstado(EstadoPago.PENDIENTE);
		
		pagoDao.editar(p);
		
		Assert.assertEquals(EstadoPago.PENDIENTE, pagoDao.buscarPorId(1).getEstado());
	}

}

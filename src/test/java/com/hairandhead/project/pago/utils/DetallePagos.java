package com.hairandhead.project.pago.utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.turno.utils.Turnos;

public class DetallePagos {

	public static List<DetallePago> getDetallePagos()
	{
		List<DetallePago> lista = new ArrayList<>();
		
		DetallePago dp1 = new DetallePago();
		dp1.setId(1);
		dp1.setMonto(new BigDecimal(100));
		try {
			dp1.setFecha(LocalDateTime.parse("23-08-2019 13:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		dp1.setTipo(TipoDetallePago.EFECTIVO);
		
		DetallePago dp2 = new DetallePago();
		dp2.setId(2);
		dp2.setMonto(new BigDecimal(200));
		try {
			dp2.setFecha(LocalDateTime.parse("23-08-2019 14:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		dp2.setTipo(TipoDetallePago.PUNTOS);
		
		DetallePago dp3 = new DetallePago();
		dp3.setId(3);
		dp3.setMonto(new BigDecimal(300));
		try {
			dp3.setFecha(LocalDateTime.parse("23-08-2019 15:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		dp3.setTipo(TipoDetallePago.CANCELACION);
		
		lista.add(dp1);
		lista.add(dp2);
		lista.add(dp3);
		
		return lista;
	}
	
	public static List<DetallePagoDTO> getDetallePagosDTO()
	{
		List<DetallePagoDTO> lista = new ArrayList<>();
		
		DetallePagoDTO dp1 = new DetallePagoDTO();
		dp1.setId(1);
		dp1.setMonto("100");
		dp1.setFecha("23-08-2019 13:00:00");
		dp1.setTipo("EFECTIVO");
		dp1.setIdTurno(1);
		dp1.setIdPago(1);
		
		DetallePagoDTO dp2 = new DetallePagoDTO();
		dp2.setId(2);
		dp2.setMonto("200");
		dp2.setFecha("23-08-2019 14:00:00");
		dp2.setTipo("PUNTOS");
		
		DetallePagoDTO dp3 = new DetallePagoDTO();
		dp3.setId(3);
		dp3.setMonto("300");
		dp3.setFecha("23-08-2019 15:00:00");
		dp3.setTipo("CANCELACION");
		
		lista.add(dp1);
		lista.add(dp2);
		lista.add(dp3);
		
		return lista;
	}
}

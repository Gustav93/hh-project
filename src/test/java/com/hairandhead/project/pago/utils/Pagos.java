package com.hairandhead.project.pago.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import com.hairandhead.project.pago.dto.PagoDTO;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;

public class Pagos {
	
	public static List<PagoDTO> getListPagosDTO() {
		
		List<PagoDTO> pagosDTO = new ArrayList<PagoDTO>();
		
		PagoDTO p1 = new PagoDTO();
		p1.setId(1);
		//p1.setMonto("300");
		p1.setFecha("");
		p1.setEstado("PENDIENTE");
		
		PagoDTO p2 = new PagoDTO();
		p2.setId(2);
		//p2.setMonto("200");
		p2.setFecha("02/10/2019 17:00");
		p2.setEstado("PAGADO");
		
		PagoDTO p3 = new PagoDTO();
		p3.setId(3);
		//p3.setMonto("100.00");
		p3.setFecha("");
		p3.setEstado("PENDIENTE");
		
		pagosDTO.add(p1);
		pagosDTO.add(p2);
		pagosDTO.add(p3);
		return pagosDTO;
	}



	public static List<Pago> getListPagos() {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		List<Pago> pagos = new ArrayList<Pago>();
		
		Pago p1 = new Pago();
		p1.setId(1);
		//p1.setMonto(new BigDecimal("300"));
		p1.setFecha(null);
		p1.setEstado(EstadoPago.PENDIENTE);
		
		Pago p2 = new Pago();
		p2.setId(2);
		try{
			p2.setFecha(LocalDate.parse("02/10/2019"));//;format.parse("02/10/2019 17:00:00"));
		}
	    catch(DateTimeParseException e){
	    	e.getMessage();
	    }
		p2.setEstado(EstadoPago.PAGADO);
		
		Pago p3 = new Pago();
		p3.setId(3);
		//p3.setMonto(new BigDecimal("100"));
		
		p3.setFecha(null);
		
		p3.setEstado(EstadoPago.PENDIENTE);
		
		pagos.add(p1);
		pagos.add(p2);
		pagos.add(p3);
		
		return pagos;
	}

}

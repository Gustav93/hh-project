package com.hairandhead.project.pago.detallepago;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.pago.service.DetallePagoServiceImpl;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.pago.dao.DetallePagoDao;
import com.hairandhead.project.pago.dto.DetallePagoDTO;
import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.utils.DetallePagos;
import com.hairandhead.project.turno.utils.Turnos;

public class DetallePagoServiceImplTest {
  
	@Mock
	@Autowired
	DetallePagoDao dao;
	
	@InjectMocks
	DetallePagoServiceImpl detallePagoService;
	
	@Spy
	List<DetallePago> detallePagos = new ArrayList<DetallePago>();
	
	@Spy
	List<DetallePagoDTO> detallePagosDTO = new ArrayList<DetallePagoDTO>();
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		this.detallePagos = DetallePagos.getDetallePagos();
		this.detallePagosDTO = DetallePagos.getDetallePagosDTO();
	}
	
	@Test
	public void findByIdTest(){
		DetallePago dp = detallePagos.get(0);
		when(dao.findById(anyInt())).thenReturn(dp);
		Assert.assertEquals(detallePagoService.findById(dp.getId()),dp);
	}
	
	@Test
    public void saveTest(){
        doNothing().when(dao).save(any(DetallePago.class));
        detallePagoService.save(any(DetallePago.class));
        verify(dao, atLeastOnce()).save(any(DetallePago.class));
    }
	
	@Test
    public void updateTest(){
		DetallePago dp = detallePagos.get(0);
        when(dao.findById(anyInt())).thenReturn(dp);
        detallePagoService.update(dp);
//        verify(dao, atLeastOnce()).findById(anyInt());
    }
	
	@Test
    public void deleteEmployeeBySsn(){
		DetallePago dp = detallePagos.get(0);
        when(dao.findById(anyInt())).thenReturn(dp);
        detallePagoService.delete(dp);
    }
	
	@Test
	public void convertToDTOTest() {
		DetallePago dp = detallePagos.get(0);
		dp.setFecha(LocalDateTime.parse("2019-08-23T13:00:00"));
		DetallePagoDTO dto = new DetallePagoDTO();
		dto.setIdTurno(1);
		dto.setIdPago(1);
		detallePagoService.convertToDTO(dp, dto);
		Assert.assertEquals(dto,this.detallePagosDTO.get(0));
	}
	
	@Test
	public void convertToDetallePagoTest() {
		DetallePago dp = new DetallePago();
		DetallePagoDTO dto = this.detallePagosDTO.get(0);
		detallePagoService.convertToDetallePago(dp,dto);
		dp.setId(1);
		this.detallePagos.get(0).setFecha(dp.getFecha());
		Assert.assertEquals(dp,this.detallePagos.get(0));
	}
	
	@Test
	public void convertToDetallePagoTest2() {
		DetallePago dp = new DetallePago();
		DetallePagoDTO dto = this.detallePagosDTO.get(0);
		dto.setMonto("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeehhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
		dto.setTipo("Batata");
		detallePagoService.convertToDetallePago(dp,dto);
		dp.setId(1);
		this.detallePagos.get(0).setMonto(BigDecimal.ZERO);
		this.detallePagos.get(0).setFecha(dp.getFecha());
		Assert.assertEquals(dp,this.detallePagos.get(0));
	}

}

package com.hairandhead.project.promocion.dao;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class PromocionAplicadaDaoImplTest extends EntityDaoImplTest {

	@Autowired
	PromocionAplicadaDao dao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("PromocionAplicada.xml"));
		return dataSet;
	}
	
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(dao.findById(1));
	}
	
	@Test
	public void saveTest() {
		PromocionAplicada p = new PromocionAplicada();
		p.setNombre("Kun Aguero");
		ServicioPromocionAplicada s1 = new ServicioPromocionAplicada();
		s1.setNombre("Corte");
		s1.setDescripcion("Corte fachero masculino");
		s1.setPrecio(new BigDecimal(370));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(35));
		s1.setEstado(EstadoServicio.ACTIVO);
		
		ServicioPromocionAplicada s2 = new ServicioPromocionAplicada();
		s2.setNombre("Tintura");
		s2.setDescripcion("Tintura colores modernos");
		s2.setPrecio(new BigDecimal(650));
		s2.setTiempoPromedioDeDuracion(Duration.ofMinutes(120));
		s2.setEstado(EstadoServicio.ACTIVO);
		
		List<ServicioPromocionAplicada> servicios = new ArrayList<>();
		servicios.add(s1);
		servicios.add(s2);
		
		p.setServicios(servicios);
		p.setDescuento(new BigDecimal(40));
		
		dao.save(p);
		
		Assert.assertEquals(dao.findAll().size(), 2);
	}
	
	@Test
	public void findAllTest() {
		Assert.assertEquals(dao.findAll().size(), 1);
	}
	
	@Test
	public void updateTest() {
		PromocionAplicada p = dao.findById(1);
		p.setMultiplicaPuntos(new BigDecimal(3));
		
		dao.update(p);
		
		Assert.assertEquals(dao.findById(1).getMultiplicaPuntos(), new BigDecimal(3));
	}
	
}

package com.hairandhead.project.promocion.utils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.servicio.model.EstadoServicio;

public class Promociones {

	public static List<Promocion> promociones() throws ParseException
	{
		List<Promocion> lista = new ArrayList<>();
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Promocion p1 = new Promocion();
		p1.setId(1);
		p1.setNombre("Promocion 1");
		p1.setDescuento(new BigDecimal(10));
		p1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-01 01:30:00"));
		p1.setFechaDeBaja(LocalDate.parse("01/11/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-11-01 01:30:00"));
		p1.setFechaDeVencimiento(null);
		p1.setEstado(EstadoPromocion.ACTIVO);
		p1.setMultiplicaPuntos(new BigDecimal(1));
		p1.setValorFinal(BigDecimal.ZERO);
//		p1.setServicios(Servicios.getListServices());
		
		Promocion p2 = new Promocion();
		p2.setId(2);
		p2.setNombre("Promocion 2");
		p2.setDescuento(new BigDecimal(20));
		p2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-01 01:30:00"));
		p2.setFechaDeBaja(LocalDate.parse("01/11/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-11-01 01:30:00"));
		p2.setFechaDeVencimiento(LocalDate.parse("15/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-15 01:30:00"));
		p2.setEstado(EstadoPromocion.INACTIVO);
		p2.setMultiplicaPuntos(new BigDecimal(2));
		p2.setValorFinal(BigDecimal.ZERO);
//		p2.setServicios(Servicios.getListServices());
		
		lista.add(p1);
		lista.add(p2);
		
		return lista;
	}
	
	public static List<PromocionDTO> promocionesDTO()
	{
		List<PromocionDTO> lista = new ArrayList<>();
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);
		ids.add(3);
		
		PromocionDTO p1 = new PromocionDTO();
		p1.setId(1);
		p1.setNombre("Promocion 1");
		p1.setDescuento("10");
		p1.setFechaDeAlta("01/10/2019");
		p1.setFechaDeBaja("01/11/2019");
		p1.setFechaDeVencimiento(null);
		p1.setEstado("ACTIVO");
		p1.setMultiplicaPuntos("1");
		p1.setValorFinal("0");
//		p1.setServicios(ids);
		
		PromocionDTO p2 = new PromocionDTO();
		p2.setId(2);
		p2.setNombre("Promocion 2");
		p2.setDescuento("20");
		p2.setFechaDeAlta("01/10/2019");
		p2.setFechaDeBaja("01/11/2019");
		p2.setFechaDeVencimiento("15/10/2019");
		p2.setEstado("INACTIVO");
		p2.setMultiplicaPuntos("2");
		p2.setValorFinal("0");
//		p2.setServicios(ids);
		
		lista.add(p1);
		lista.add(p2);
		
		return lista;
	}
	
	public static List<PromocionAplicada> promocionesAplicadas(){
		List<PromocionAplicada> lista = new ArrayList<>();
		PromocionAplicada pa1 = new PromocionAplicada();
		pa1.setId(1);
		pa1.setNombre("Promocio aplicada 1");
		pa1.setDescuento(new BigDecimal("17"));
		pa1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-01 01:30:00"));
		pa1.setFechaDeBaja(LocalDate.parse("01/11/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-11-01 01:30:00"));
		pa1.setFechaDeVencimiento(null);
		pa1.setEstado(EstadoPromocion.ACTIVO);
		pa1.setMultiplicaPuntos(new BigDecimal(1));
		pa1.setValorFinal(BigDecimal.ZERO);
		
		PromocionAplicada pa2 = new PromocionAplicada();
		pa2.setId(2);
		pa2.setNombre("Promocion Aplicada 2");
		pa2.setDescuento(new BigDecimal(20));
		pa2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-01 01:30:00"));
		pa2.setFechaDeBaja(LocalDate.parse("01/11/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-11-01 01:30:00"));
		pa2.setFechaDeVencimiento(LocalDate.parse("15/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));//format.parse("2019-10-15 01:30:00"));
		pa2.setEstado(EstadoPromocion.INACTIVO);
		pa2.setMultiplicaPuntos(new BigDecimal(2));
		pa2.setValorFinal(BigDecimal.ZERO);
		
		lista.add(pa1);
		lista.add(pa2);
		
		return lista;
	}
	
	public static List<PromocionAplicadaDTO> promocionesAplicadasDTO(){
		List<PromocionAplicadaDTO> lista = new ArrayList<>();
		PromocionAplicadaDTO pa1 = new PromocionAplicadaDTO();
		pa1.setId(1);
		pa1.setNombre("Promocio aplicada 1");
		pa1.setDescuento("17");
		pa1.setFechaDeAlta("01/10/2019");//format.parse("2019-10-01 01:30:00"));
		pa1.setFechaDeBaja("01/11/2019");//format.parse("2019-11-01 01:30:00"));
		pa1.setFechaDeVencimiento(null);
		pa1.setEstado("ACTIVO");
		pa1.setMultiplicaPuntos("1");
		pa1.setValorFinal("0");
		
		PromocionAplicadaDTO pa2 = new PromocionAplicadaDTO();
		pa2.setId(2);
		pa2.setNombre("Promocion Aplicada 2");
		pa2.setDescuento("20");
		pa2.setFechaDeAlta("01/10/2019");//format.parse("2019-10-01 01:30:00"));
		pa2.setFechaDeBaja("01/11/2019");//format.parse("2019-11-01 01:30:00"));
		pa2.setFechaDeVencimiento("15/10/2019");//format.parse("2019-10-15 01:30:00"));
		pa2.setEstado("INACTIVO");
		pa2.setMultiplicaPuntos("2");
		pa2.setValorFinal("0");
		
		lista.add(pa1);
		lista.add(pa2);
		
		return lista;
	}
	
	public static List<ServicioPromocionAplicada> servicioPromocionesAplicadas(){
		List<ServicioPromocionAplicada> lista = new ArrayList<>();
		
		ServicioPromocionAplicada spa1 = new ServicioPromocionAplicada();
		spa1.setId(1);
		spa1.setNombre("SPA1");
		spa1.setDescripcion("Descripcion1");
		spa1.setPrecio(new BigDecimal("90"));
		spa1.setTiempoPromedioDeDuracion(Duration.ofHours(1));
		spa1.setEstado(EstadoServicio.ACTIVO);
		spa1.setFechaDeBaja(null);
		
		ServicioPromocionAplicada spa2 = new ServicioPromocionAplicada();
		spa2.setId(2);
		spa2.setNombre("SPA2");
		spa2.setDescripcion("Descripcion2");
		spa2.setPrecio(new BigDecimal("80"));
		spa2.setTiempoPromedioDeDuracion(Duration.ofHours(2));
		spa2.setEstado(EstadoServicio.INACTIVO);
		spa2.setFechaDeBaja(LocalDateTime.parse("2018/10/10T11:25"));
		
		lista.add(spa1);
		lista.add(spa2);
		
		return lista;
	}
}

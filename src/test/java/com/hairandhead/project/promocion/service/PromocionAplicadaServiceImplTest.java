package com.hairandhead.project.promocion.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.promocion.dao.PromocionAplicadaDao;
import com.hairandhead.project.promocion.dto.PromocionAplicadaDTO;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.utils.Promociones;

public class PromocionAplicadaServiceImplTest {

	@Mock
	PromocionAplicadaDao dao;
     
    @InjectMocks
    PromocionAplicadaServiceImpl paService;
     
    @Spy
    List<PromocionAplicada> promocionesAplicadas = new ArrayList<>();
    @Spy
    List<PromocionAplicadaDTO> promocionesAplicadasDTO = new ArrayList<>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        promocionesAplicadas = Promociones.promocionesAplicadas();
        promocionesAplicadasDTO = Promociones.promocionesAplicadasDTO();
    }
    
    @Test
    public void findByIdTest(){
    	PromocionAplicada pa = promocionesAplicadas.get(0);
        when(dao.findById(anyInt())).thenReturn(pa);
        Assert.assertEquals(paService.findPromocionAplicadaById(pa.getId()),pa);
    }
    
    @Test
    public void saveTest(){
        doNothing().when(dao).save(any(PromocionAplicada.class));
        paService.savePromocionAplicada(any(PromocionAplicada.class));
        verify(dao, atLeastOnce()).save(any(PromocionAplicada.class));
    }
    
    @Test
    public void updateTest(){
    	PromocionAplicada pa = promocionesAplicadas.get(0);
        when(dao.findById(anyInt())).thenReturn(pa);
        paService.updatePromocion(pa);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void findAllEmployees(){
        when(dao.findAll()).thenReturn(promocionesAplicadas);
        Assert.assertEquals(paService.findAllPromocionesAplicadas(), promocionesAplicadas);
    }
    
    @Test
    public void convertPromocionApliocadaToDTONullTest() {
    	PromocionAplicada pa =null;
    	Assert.assertNull(this.paService.convertPromocionApliocadaToDTO(pa));
    }
    
    @Test
    public void convertPromocionApliocadaToDTOTest() {
    	PromocionAplicada pa = this.promocionesAplicadas.get(0);
    	PromocionAplicadaDTO dto = paService.convertPromocionApliocadaToDTO(pa);
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0), dto);
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getId(), dto.getId());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getNombre(), dto.getNombre());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getDescuento(), dto.getDescuento());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getEstado(), dto.getEstado());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getServicios(), dto.getServicios());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getFechaDeAlta(), dto.getFechaDeAlta());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getFechaDeBaja(), dto.getFechaDeBaja());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getFechaDeVencimiento(), dto.getFechaDeVencimiento());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getValorFinal(), dto.getValorFinal());
    	Assert.assertEquals(this.promocionesAplicadasDTO.get(0).getMultiplicaPuntos(), dto.getMultiplicaPuntos());
    	
    }
    
    @Test
    public void convertPromocionApliocadaToAtrNullDTOTest() {
    	PromocionAplicada pa = this.promocionesAplicadas.get(0);
    	pa.setFechaDeAlta(null);
    	pa.setFechaDeBaja(null);
    	pa.setFechaDeVencimiento(null);
    	pa.setDescuento(null);
    	pa.setMultiplicaPuntos(null);
    	pa.setValorFinal(null);
    	PromocionAplicadaDTO dto = this.promocionesAplicadasDTO.get(0);
    	dto.setFechaDeAlta(null);
    	dto.setFechaDeBaja(null);
    	dto.setFechaDeVencimiento(null);
    	dto.setDescuento("");
    	dto.setMultiplicaPuntos("");
    	dto.setValorFinal("");
    	PromocionAplicadaDTO dto2 = paService.convertPromocionApliocadaToDTO(pa);
    	Assert.assertEquals(dto, dto2);
    }
    
    @Test 
    public void convertPromocionAplicadaToDTOListTest() {
    	Assert.assertEquals(paService.convertPromocionAplicadaToDTOList(this.promocionesAplicadas),this.promocionesAplicadasDTO);
    }
    
    @Test
	public void hashCodeTest() {
		PromocionAplicada p = new PromocionAplicada();
		p.setDescuento(null);
		p.setEstado(null);
		p.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setValorFinal(null);
		p.setServicios(null);
		p.setNombre("pepe");
		p.setMultiplicaPuntos(null);
		Assert.assertNotEquals(p.hashCode(), this.promocionesAplicadas.hashCode());
	}
    
    @Test
	public void hashCodeDTOTest() {
		PromocionAplicadaDTO p = new PromocionAplicadaDTO();
		p.setId(1);
		p.setDescuento("10");
		p.setEstado("ACTIVO");
		p.setFechaDeAlta("2019/10/01");
		p.setFechaDeBaja("2019/10/01");
		p.setFechaDeVencimiento("2019/10/01");
		p.setValorFinal("100");
		p.setServicios(new ArrayList<>());
		p.setNombre("pepe");
		p.setMultiplicaPuntos("1");
		Assert.assertNotEquals(p.hashCode(), this.promocionesAplicadasDTO.get(0).hashCode());
		Assert.assertNotEquals(p.getValorFinal(), this.promocionesAplicadasDTO.get(0).getValorFinal());
	}
    
    @Test
	public void hashCodeDTOTest2() {
    	PromocionAplicadaDTO p = new PromocionAplicadaDTO();
		p.setId(null);
		p.setDescuento(null);
		p.setEstado(null);
		p.setFechaDeAlta(null);
		p.setFechaDeBaja(null);
		p.setFechaDeVencimiento(null);
		p.setValorFinal(null);
		p.setServicios(null);
		p.setNombre(null);
		p.setMultiplicaPuntos(null);
		Assert.assertNotEquals(p.hashCode(), this.promocionesAplicadasDTO.get(0).hashCode());
	}
    
    @Test
	public void equalsTest() {
    	PromocionAplicada p1 = new PromocionAplicada();
    	PromocionAplicada p2 = new PromocionAplicada();
		
		Assert.assertFalse(p1.equals(null));
		Assert.assertTrue(p1.equals(p1));
		
		p1.setDescuento(new BigDecimal(9));
		p2.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(new BigDecimal(9));
		p1.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(null);
		p1.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setDescuento(new BigDecimal(9));
		p2.setDescuento(new BigDecimal(9));
		
		p1.setEstado(EstadoPromocion.ACTIVO);
		p2.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(EstadoPromocion.ACTIVO);
		p1.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(null);
		p1.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setEstado(EstadoPromocion.ACTIVO);
		p2.setEstado(EstadoPromocion.ACTIVO);
		
		p1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(null);
		p1.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(null);
		p1.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(null);
		p1.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setId(1);
		p2.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(1);
		p1.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(null);
		p1.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setId(1);
		p2.setId(1);
		
		p1.setMultiplicaPuntos(new BigDecimal(9));
		p2.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(new BigDecimal(9));
		p1.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(null);
		p1.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setMultiplicaPuntos(new BigDecimal(9));
		p2.setMultiplicaPuntos(new BigDecimal(9));
		
		p1.setNombre("Promo 1");
		p2.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre("Promo 1");
		p1.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre(null);
		p1.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setNombre("Promo 1");
		p2.setNombre("Promo 1");
		
		p1.setServicios(new ArrayList<>());
		p2.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(new ArrayList<>());
		p1.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(null);
		p1.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setServicios(new ArrayList<>());
		p2.setServicios(new ArrayList<>());
		
		p1.setValorFinal(new BigDecimal(9));
		p2.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(new BigDecimal(9));
		p1.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(null);
		p1.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p1.setValorFinal(new BigDecimal(9));
		p2.setValorFinal(new BigDecimal(9));
		
		Assert.assertFalse(p1.equals(p2));
	}
    
    @Test
	public void equalsDTOTest() {
    	PromocionAplicadaDTO p1 = new PromocionAplicadaDTO();
    	PromocionAplicadaDTO p2 = new PromocionAplicadaDTO();
		
		Assert.assertFalse(p1.equals(null));
		Assert.assertTrue(p1.equals(p1));
		
		p1.setDescuento("9");
		p2.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento("9");
		p1.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(null);
		p1.setDescuento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setDescuento("9");
		p2.setDescuento("9");
		
		p1.setEstado("ACTIVO");
		p2.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado("ACTIVO");
		p1.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(null);
		p1.setEstado(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setEstado("ACTIVO");
		p2.setEstado("ACTIVO");
		
		p1.setFechaDeAlta("2019/10/01");
		p2.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta("2019/10/01");
		p1.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(null);
		p1.setFechaDeAlta(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeAlta("2019/10/01");
		p2.setFechaDeAlta("2019/10/01");
		
		p1.setFechaDeBaja("2019/10/01");
		p2.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja("2019/10/01");
		p1.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(null);
		p1.setFechaDeBaja(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeBaja("2019/10/01");
		p2.setFechaDeBaja("2019/10/01");
		
		p1.setFechaDeVencimiento("2019-10-01");
		p2.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento("2019/10/01");
		p1.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(null);
		p1.setFechaDeVencimiento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeVencimiento("2019/10/01");
		p2.setFechaDeVencimiento("2019/10/01");
		
		p1.setId(1);
		p2.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(1);
		p1.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(null);
		p1.setId(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setId(1);
		p2.setId(1);
		
		p1.setMultiplicaPuntos("9");
		p2.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos("9");
		p1.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(null);
		p1.setMultiplicaPuntos(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setMultiplicaPuntos("9");
		p2.setMultiplicaPuntos("9");
		
		p1.setNombre("Promo 1");
		p2.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre("Promo 1");
		p1.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre(null);
		p1.setNombre(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setNombre("Promo 1");
		p2.setNombre("Promo 1");
		
		p1.setServicios(new ArrayList<>());
		p2.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(new ArrayList<>());
		p1.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(null);
		p1.setServicios(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setServicios(new ArrayList<>());
		p2.setServicios(new ArrayList<>());
		
		p1.setValorFinal("9");
		p2.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal("9");
		p1.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(null);
		p1.setValorFinal(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setValorFinal("9");
		p2.setValorFinal("9");
		
		Assert.assertTrue(p1.equals(p2));
	}
}

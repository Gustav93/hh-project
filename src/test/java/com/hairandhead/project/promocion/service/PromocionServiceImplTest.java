package com.hairandhead.project.promocion.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.promocion.dao.PromocionDao;
import com.hairandhead.project.promocion.dto.PromocionDTO;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.utils.Promociones;

public class PromocionServiceImplTest {

	@Mock
	@Autowired
	private PromocionDao dao;
	
	@InjectMocks
	private PromocionServiceImpl promocionService;
	
	@Spy
	private List<Promocion> promociones = new ArrayList<Promocion>();
	
	@Spy
	private List<PromocionDTO> promocionesDTO = new ArrayList<PromocionDTO>();
	
	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		try {
			this.promociones = Promociones.promociones();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.promocionesDTO = Promociones.promocionesDTO();
	}
	
	@Test
	public void findByIdTest(){
		Promocion p = this.promociones.get(0);
		when(dao.findById(anyInt())).thenReturn(p);
		Assert.assertEquals(promocionService.findPromocionById(anyInt()), p);
	}
	
	@Test
    public void savePromocionTest(){
        doNothing().when(dao).save(any(Promocion.class));
        promocionService.savePromocion(any(Promocion.class));
        verify(dao, atLeastOnce()).save(any(Promocion.class));
    }
	
	@Test
    public void updatePromocionTest(){
		doNothing().when(dao).update(any(Promocion.class));
        promocionService.updatePromocion(any(Promocion.class));
        verify(dao, atLeastOnce()).update(any(Promocion.class));
    }
	
	@Test
    public void findAllPromocionesTest(){
        when(dao.findAll()).thenReturn(this.promociones);
        Assert.assertEquals(promocionService.findAllPromociones(), this.promociones);
    }
	
	@Test
	public void convertToPromocionNullTest() {
		Assert.assertNull(promocionService.convertToPromocion(null));
	}
	
	@Test
	public void convertToPromocionTest() {
		Assert.assertEquals(promocionService.convertToPromocion(this.promocionesDTO.get(0)), this.promociones.get(0));
	}
	
	@Test
	public void convertToPromocionTest2() {
		Promocion p = this.promociones.get(1);
		p.setNombre(null);
		PromocionDTO dto = this.promocionesDTO.get(1);
		dto.setNombre(null);
		Assert.assertEquals(promocionService.convertToPromocion(dto),p);
	}
	
	@Test
	public void convertToDTONullTest() {
		Assert.assertNull(promocionService.convertToDTO(null));
	}
	
	@Test
	public void convertToDTOTest() {
		Assert.assertEquals(promocionService.convertToDTO(this.promociones.get(0)), this.promocionesDTO.get(0));
	}
	
	@Test
	public void convertToDTOTest2() {
		Promocion p = this.promociones.get(1);
		p.setNombre(null);
		p.setDescuento(null);
		p.setMultiplicaPuntos(null);
		p.setValorFinal(null);
		PromocionDTO dto = this.promocionesDTO.get(1);
		dto.setNombre(null);
		dto.setDescuento("");
		dto.setMultiplicaPuntos("");
		dto.setValorFinal("");
		Assert.assertEquals(promocionService.convertToDTO(p),dto);
	}
	
	@Test
	public void findByEstadoTest() {
		when(dao.findByEstado(EstadoPromocion.ACTIVO)).thenReturn(this.promociones);
        Assert.assertEquals(promocionService.findByEstado(EstadoPromocion.ACTIVO), this.promociones);
	}
	
	@Test
	public void convertToDTOListTest() {
		Assert.assertEquals(promocionService.convertToDTOList(this.promociones), this.promocionesDTO);
	}
	
	@Test
	public void hashCodeTest() {
		Promocion p = new Promocion();
		p.setDescuento(null);
		p.setEstado(null);
		p.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p.setValorFinal(null);
		p.setServicios(null);
		p.setNombre("pepe");
		p.setMultiplicaPuntos(null);
		Assert.assertNotEquals(p.hashCode(), this.promociones.get(0).hashCode());
	}
	
	@Test
	public void hashCodeDTOTest() {
		PromocionDTO p = new PromocionDTO();
		p.setId(1);
		p.setDescuento("10");
		p.setEstado("ACTIVO");
		p.setFechaDeAlta("2019/10/01");
		p.setFechaDeBaja("2019/10/01");
		p.setFechaDeVencimiento("2019/10/01");
		p.setValorFinal("100");
		p.setServicios(new ArrayList<>());
		p.setNombre("pepe");
		p.setMultiplicaPuntos("1");
		Assert.assertNotEquals(p.hashCode(), this.promocionesDTO.get(0).hashCode());
		Assert.assertNotEquals(p.getValorFinal(), this.promocionesDTO.get(0).getValorFinal());
	}
	
	@Test
	public void hashCodeDTOTest2() {
		PromocionDTO p = new PromocionDTO();
		p.setId(null);
		p.setDescuento(null);
		p.setEstado(null);
		p.setFechaDeAlta(null);
		p.setFechaDeBaja(null);
		p.setFechaDeVencimiento(null);
		p.setValorFinal(null);
		p.setServicios(null);
		p.setNombre(null);
		p.setMultiplicaPuntos(null);
		Assert.assertNotEquals(p.hashCode(), this.promocionesDTO.get(0).hashCode());
	}
	
	@Test
	public void equalsTest() {
		Promocion p1 = new Promocion();
		Promocion p2 = new Promocion();
		
		Assert.assertFalse(p1.equals(null));
		Assert.assertTrue(p1.equals(p1));
		
		p1.setDescuento(new BigDecimal(9));
		p2.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(new BigDecimal(9));
		p1.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(null);
		p1.setDescuento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setDescuento(new BigDecimal(9));
		p2.setDescuento(new BigDecimal(9));
		
		p1.setEstado(EstadoPromocion.ACTIVO);
		p2.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(EstadoPromocion.ACTIVO);
		p1.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(null);
		p1.setEstado(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setEstado(EstadoPromocion.ACTIVO);
		p2.setEstado(EstadoPromocion.ACTIVO);
		
		p1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(null);
		p1.setFechaDeAlta(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeAlta(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(null);
		p1.setFechaDeBaja(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeBaja(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p1.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(null);
		p1.setFechaDeVencimiento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		p2.setFechaDeVencimiento(LocalDate.parse("01/10/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
		p1.setId(1);
		p2.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(1);
		p1.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(null);
		p1.setId(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setId(1);
		p2.setId(1);
		
		p1.setMultiplicaPuntos(new BigDecimal(9));
		p2.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(new BigDecimal(9));
		p1.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(null);
		p1.setMultiplicaPuntos(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setMultiplicaPuntos(new BigDecimal(9));
		p2.setMultiplicaPuntos(new BigDecimal(9));
		
		p1.setNombre("Promo 1");
		p2.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre("Promo 1");
		p1.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre(null);
		p1.setNombre(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setNombre("Promo 1");
		p2.setNombre("Promo 1");
		
		p1.setServicios(new ArrayList<>());
		p2.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(new ArrayList<>());
		p1.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(null);
		p1.setServicios(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setServicios(new ArrayList<>());
		p2.setServicios(new ArrayList<>());
		
		p1.setValorFinal(new BigDecimal(9));
		p2.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(new BigDecimal(9));
		p1.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(null);
		p1.setValorFinal(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setValorFinal(new BigDecimal(9));
		p2.setValorFinal(new BigDecimal(9));
		
		Assert.assertTrue(p1.equals(p2));
	}
	
	@Test
	public void equalsDTOTest() {
		PromocionDTO p1 = new PromocionDTO();
		PromocionDTO p2 = new PromocionDTO();
		
		Assert.assertFalse(p1.equals(null));
		Assert.assertTrue(p1.equals(p1));
		
		p1.setDescuento("9");
		p2.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento("9");
		p1.setDescuento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setDescuento(null);
		p1.setDescuento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setDescuento("9");
		p2.setDescuento("9");
		
		p1.setEstado("ACTIVO");
		p2.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado("ACTIVO");
		p1.setEstado(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setEstado(null);
		p1.setEstado(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setEstado("ACTIVO");
		p2.setEstado("ACTIVO");
		
		p1.setFechaDeAlta("2019/10/01");
		p2.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta("2019/10/01");
		p1.setFechaDeAlta(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeAlta(null);
		p1.setFechaDeAlta(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeAlta("2019/10/01");
		p2.setFechaDeAlta("2019/10/01");
		
		p1.setFechaDeBaja("2019/10/01");
		p2.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja("2019/10/01");
		p1.setFechaDeBaja(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeBaja(null);
		p1.setFechaDeBaja(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeBaja("2019/10/01");
		p2.setFechaDeBaja("2019/10/01");
		
		p1.setFechaDeVencimiento("2019-10-01");
		p2.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento("2019/10/01");
		p1.setFechaDeVencimiento(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setFechaDeVencimiento(null);
		p1.setFechaDeVencimiento(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setFechaDeVencimiento("2019/10/01");
		p2.setFechaDeVencimiento("2019/10/01");
		
		p1.setId(1);
		p2.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(1);
		p1.setId(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setId(null);
		p1.setId(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setId(1);
		p2.setId(1);
		
		p1.setMultiplicaPuntos("9");
		p2.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos("9");
		p1.setMultiplicaPuntos(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setMultiplicaPuntos(null);
		p1.setMultiplicaPuntos(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setMultiplicaPuntos("9");
		p2.setMultiplicaPuntos("9");
		
		p1.setNombre("Promo 1");
		p2.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre("Promo 1");
		p1.setNombre(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setNombre(null);
		p1.setNombre(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setNombre("Promo 1");
		p2.setNombre("Promo 1");
		
		p1.setServicios(new ArrayList<>());
		p2.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(new ArrayList<>());
		p1.setServicios(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setServicios(null);
		p1.setServicios(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setServicios(new ArrayList<>());
		p2.setServicios(new ArrayList<>());
		
		p1.setValorFinal("9");
		p2.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal("9");
		p1.setValorFinal(null);
		Assert.assertFalse(p1.equals(p2));
		p2.setValorFinal(null);
		p1.setValorFinal(null);
		Assert.assertTrue(p1.equals(p2));
		p1.setValorFinal("9");
		p2.setValorFinal("9");
		
		Assert.assertTrue(p1.equals(p2));
	}
}

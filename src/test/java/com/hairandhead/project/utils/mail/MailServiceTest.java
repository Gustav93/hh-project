package com.hairandhead.project.utils.mail;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class MailServiceTest {

    @InjectMocks
    MailServiceImpl mailService;

    @Mock
    DataConfigService dataConfigService;

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sendMailTest(){
        List<String> mailList = Collections.singletonList("vizaral2@gmail.com");
        when(dataConfigService.findByProperty(anyString())).thenReturn(new DataConfig());
        mailService.sendMail(mailList,"", "");
    }

    @Test
    public void sendMailWithAttachmentTest(){
        List<String> mailList = Collections.singletonList("eeeeeee@gmail2.com");
        when(dataConfigService.findByProperty(anyString())).thenReturn(new DataConfig());
        mailService.sendMailWithAttachment(mailList,"", "",new ArrayList<>());
    }
}

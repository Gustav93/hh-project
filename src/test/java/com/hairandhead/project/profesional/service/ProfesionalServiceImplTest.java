package com.hairandhead.project.profesional.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.profesional.dao.ProfesionalDao;
import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.HorarioDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.profesional.utils.Profesionales;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.servicio.utils.Servicios;

public class ProfesionalServiceImplTest {
	@Mock
	@Autowired
	private ProfesionalDao dao;
	
	@Mock
    @Autowired
    private ServicioService service;
	
	@InjectMocks
	private ProfesionalServiceImpl profesionalService;
	
	@Spy
	private List<Profesional> profesionales = new ArrayList<>();
	
	@Spy
	private List<Servicio> servicios = new ArrayList<>();
	
	@Spy
	private List<ProfesionalDTO> profesionalesDTO = new ArrayList<>();
	
	@Spy
	private List<Disponibilidad> disponbilidades = new ArrayList<>();
	
	@Spy
	private List<DisponibilidadDTO> disponbilidadesDTO = new ArrayList<>();
	
	@Spy
	private List<Horario> horarios = new ArrayList<>();
	
	@Spy
	private List<HorarioDTO> horariosDTO = new ArrayList<>();
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		this.profesionales = Profesionales.getProfesionales();
		this.servicios = Servicios.getListServices();
		this.profesionalesDTO = Profesionales.getProfesionalesDTO();
		this.disponbilidades = Profesionales.getDisponibilidades();
		this.disponbilidadesDTO = Profesionales.getDisponibilidadesDTO();
		this.horarios = Profesionales.getHorarios();
		this.horariosDTO = Profesionales.getHorariosDTO();
	}
	
	@Test
	public void findByIdTest() {
		
		Profesional p = this.profesionales.get(0);
		p.setServicios(this.servicios);
		when(dao.findById(anyInt())).thenReturn(p);
		
		Assert.assertEquals(profesionalService.findById(p.getId()), p);
	}
	
	@Test
    public void saveEmployeeNotNullTest(){
        doNothing().when(dao).save(any(Profesional.class));
        profesionalService.save(any(Profesional.class));
        verify(dao, atLeastOnce()).save(any(Profesional.class));
    }
	
	@Test
    public void saveEmployeeNullTest(){
        doNothing().when(dao).save(null);
        profesionalService.save(null);
        verify(dao, atLeastOnce()).save(null);
    }
	
	@Test
	public void updateTest() {
		doNothing().when(dao).update(any(Profesional.class));
        profesionalService.update(any(Profesional.class));
        verify(dao, atLeastOnce()).update(any(Profesional.class));
	}
	
	@Test
	public void findAllTest() {
		 when(dao.findAll()).thenReturn(this.profesionales);
		 Assert.assertEquals(profesionalService.findAll(), this.profesionales);
	}
	
	 @Test
	 public void findByProfesionalEmailTest(){
		 Profesional p = this.profesionales.get(0);
		 when(dao.findByEmail(anyString())).thenReturn(p);
		 Assert.assertEquals(profesionalService.findByEmail(anyString()), p);
	 }
	 
	 @Test
	 public void findByProfesionalEmailNullTest(){
		 Assert.assertNull(profesionalService.findByEmail(null));
	 }
	
	 @Test
	 public void convertToProfesionalNullTest()
	 {
		 Assert.assertNull(profesionalService.convertToProfesional(null));
	 }
	 
	 @Test
	 public void convertToProfesionalTest()
	 {
	 	ProfesionalDTO profesionalDTO = this.profesionalesDTO.get(0);
	 	profesionalDTO.setFechaDeNacimiento(null);
	 	Profesional profesional = this.profesionales.get(0);
		profesional.setFechaDeNacimiento(null);
		 Assert.assertEquals(profesionalService.convertToProfesional(profesionalDTO),profesional);
	 }
	 
	 @Test
	 public void convertToProfesionalAtributosNullTest()
	 {
		 Profesional p = new Profesional();
		 p.setId(3);
		 p.setNombre("");
		 p.setApellido("");
		 p.setEmail("");
		 p.setTelefono("");
//		 p.setFechaBaja(null);
		 p.setFechaDeNacimiento(null);
		 ProfesionalDTO dto = new ProfesionalDTO();
		 dto.setId(3);
		 dto.setNombre(null);
		 dto.setApellido(null);
		 dto.setEmail(null);
		 dto.setTelefono(null);
//		 dto.setFechaBaja(null);
		 dto.setFechaDeNacimiento("16/01/1993");
		 
		 Assert.assertEquals(profesionalService.convertToProfesional(dto),p);
	 }
	 
	 @Test
	 public void convertToDtoNULLTest() {
		 Assert.assertNull(profesionalService.convertToDTO(null));
	 }
	 
	 @Test
	 public void convertToDtoServiciosCargadosTest() {
		 Profesional p = this.profesionales.get(1);
		 p.setServicios(this.servicios);
		 ProfesionalDTO dto = this.profesionalesDTO.get(1);
		 dto.setEstado("ACTIVO");
		 Assert.assertEquals(this.profesionalService.convertToDTO(p), dto);
	 }
	 
	 @Test
	 public void convertToDtoCamposVaciosTest() {
		 Profesional p = new Profesional();
		 p.setId(3);
		 p.setNombre(null);
		 p.setApellido(null);
		 p.setEmail(null);
		 p.setTelefono(null);
		 p.setFechaBaja(null);
		 p.setFechaDeNacimiento(null);
		 p.setEstadoProfesional(null);
		 ProfesionalDTO dto = new ProfesionalDTO();
		 dto.setId(3);
		 dto.setNombre("");
		 dto.setApellido("");
		 dto.setEmail("");
		 dto.setTelefono("");
		 dto.setFechaBaja("");
		 dto.setFechaDeNacimiento("");
		 dto.setEstadoProfesional("");
		 Assert.assertEquals(this.profesionalService.convertToDTO(p), dto);
	 }
	 
	 @Test
	 public void convertDisponibilidadToDTOListTest() {
		 Assert.assertEquals(this.profesionalService.convertDisponibilidadToDTOList(this.disponbilidades), this.disponbilidadesDTO);
	 }
	 
	 @Test
	 public void convertDisponibilidadToDTOListHorariosNULLTest() {
		 Assert.assertEquals(this.profesionalService.convertDisponibilidadToDTOList(this.disponbilidades), this.disponbilidadesDTO);
	 }
	 
	 @Test
	 public void convertToDTOListTest()
	 {
	 	Profesional profesional = profesionales.get(0);
	 	ProfesionalDTO profesionalDTO = profesionalesDTO.get(0);
		profesional.setFechaBaja(null);
		profesional.setFechaDeNacimiento(null);
		profesionalDTO.setFechaBaja("");
		profesionalDTO.setFechaDeNacimiento("");
		profesionalDTO.setEstado("ACTIVO");
		List<Profesional> profesionalList = new ArrayList<>();
		profesionalList.add(profesional);
		List<ProfesionalDTO> profesionalDTOList = new ArrayList<>();
		profesionalDTOList.add(profesionalDTO);
		Assert.assertEquals(this.profesionalService.convertToDTOList(profesionalList), profesionalDTOList);
	 }
	 
	 @Test
	 public void findDisponibilidadByIdTest()
	 {
		 Profesional p = this.profesionales.get(0);
		 p.setDisponibilidad(this.disponbilidades);
		 when(dao.findById(anyInt())).thenReturn(p);
		 Assert.assertEquals(this.profesionalService.findDisponibilidadById(1, this.disponbilidades.get(0).getId()), this.disponbilidades.get(0));
	 }
	 
	 @Test
	 public void findDisponibilidadByIdNullTest()
	 {
		 Profesional p = this.profesionales.get(0);
		 p.setDisponibilidad(this.disponbilidades);
		 when(dao.findById(anyInt())).thenReturn(p);
		 Assert.assertNull(this.profesionalService.findDisponibilidadById(3, this.disponbilidades.get(0).getId()));
	 }
	 
	 @Test
	 public void convertHorarioToDTOListTest() {
		 Assert.assertEquals(this.profesionalService.convertHorarioToDTOList(this.horarios),this.horariosDTO);
	 }
	 
	 @Test
	 public void findAllWithServiceEqualsTest() {
		 when(dao.findAllProfesionalesActivosConServicio(anyInt())).thenReturn(this.profesionales);
	     Assert.assertEquals(this.profesionalService.findAllWithServiceEquals(anyInt()), this.profesionales);
	 }
	 
	 @Test
	 public void disponiblidadHashCodeTest() {
		 Disponibilidad d1 = new Disponibilidad();
		 d1.setId(null);
		 d1.setDia(null);
		 d1.setHoraDisponible(null);
		 Disponibilidad d2 = new Disponibilidad();
		 d2.setId(1);
		 d2.setDia("l");
		 d2.setHoraDisponible(new ArrayList<>());
		 Assert.assertNotEquals(d1.hashCode(),d2.hashCode());
		 Assert.assertNotEquals(d1.toString(),d2.toString());
	 }
	 
	 @Test
		public void profesionalEqualsTest() {
		 	Disponibilidad s1 = new Disponibilidad();
		 	Disponibilidad s2 = null;
			Assert.assertFalse(s1.equals(s2));
			
			s2 = new Disponibilidad();
			s1.setId(null);
			s2.setId(null);
			Assert.assertTrue(s1.equals(s2));
			s1.setId(null);
			s2.setId(2);
			Assert.assertFalse(s1.equals(s2));
			s1.setId(1);
			s2.setId(2);
			Assert.assertFalse(s1.equals(s2));
			s1.setId(3);
			s2.setId(3);
			Assert.assertTrue(s1.equals(s2));
			
			s2 = new Disponibilidad();
			s1.setDia(null);
			s2.setDia(null);
			Assert.assertFalse(s1.equals(s2));
			s1.setDia(null);
			s2.setDia("2");
			Assert.assertFalse(s1.equals(s2));
			s1.setDia("1");
			s2.setDia("2");
			Assert.assertFalse(s1.equals(s2));
			s1.setDia("3");
			s2.setDia("3");
			Assert.assertFalse(s1.equals(s2));
			
			s1.setHoraDisponible(null);
			s2.setHoraDisponible(null);
			Assert.assertFalse(s1.equals(s2));
			s1.setHoraDisponible(null);
			s2.setHoraDisponible(new ArrayList<>());
			Assert.assertFalse(s1.equals(s2));
			s1.setHoraDisponible(new ArrayList<>());
			s2.setHoraDisponible(new ArrayList<>());
			Assert.assertFalse(s1.equals(s2));
			s1.setHoraDisponible(new ArrayList<>());
			s2.setHoraDisponible(new ArrayList<>());
			Assert.assertFalse(s1.equals(s2));
	 }
	 
	 @Test
	 public void profesionalHashCodeTest() {
		 Profesional p = new Profesional();
		 p.setApellido(null);
		 p.setDisponibilidad(null);
		 p.setEmail(null);
		 p.setEstadoProfesional(null);
		 p.setFechaBaja(null);
		 p.setFechaDeNacimiento(null);
		 p.setId(null);
		 p.setNombre(null);
		 p.setServicios(null);
		 p.setSucursales(null);
		 p.setTelefono(null);
		 Profesional p2 = new Profesional();
		 p2.setApellido("a1");
		 p2.setDisponibilidad(new ArrayList<>());
		 p2.setEmail("email");
		 p2.setEstadoProfesional(EstadoProfesional.VACACIONES);
		 p2.setFechaBaja(LocalDateTime.now());
		 p2.setFechaDeNacimiento(LocalDate.now());
		 p2.setId(1);
		 p2.setNombre("pepe");
		 p2.setServicios(new ArrayList<>());
		 p2.setSucursales("sede");
		 p2.setTelefono("1234");
		 Assert.assertNotEquals(p.hashCode(),p2.hashCode());
		 Assert.assertNotEquals(p.getSucursales(),p2.getSucursales());
		 Assert.assertNotEquals(p.toString(),p2.toString());
	 }
	 
	 @Test
	 public void profesionalEqualTest() {
		 Profesional p1 = new Profesional();
		 Profesional p2 = null;
		 Assert.assertFalse(p1.equals(p2));
		 
		 p2 = new Profesional();
		 p1.setApellido(null);
		 p2.setApellido(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setApellido(null);
		 p2.setApellido("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setApellido("d1");
		 p2.setApellido("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setApellido("d");
		 p2.setApellido("d");
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setDisponibilidad(null);
		 p2.setDisponibilidad(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setDisponibilidad(null);
		 p2.setDisponibilidad(new ArrayList<>());
		 Assert.assertFalse(p1.equals(p2));
		 List<Disponibilidad> d = new ArrayList<Disponibilidad>();
		 d.add(new Disponibilidad());
		 p1.setDisponibilidad(d);
		 p2.setDisponibilidad(new ArrayList<>());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setDisponibilidad(new ArrayList<>());
		 p2.setDisponibilidad(new ArrayList<>());
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setEmail(null);
		 p2.setEmail(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setEmail(null);
		 p2.setEmail("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setEmail("d1");
		 p2.setEmail("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setEmail("d");
		 p2.setEmail("d");
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setEstadoProfesional(null);
		 p2.setEstadoProfesional(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setEstadoProfesional(null);
		 p2.setEstadoProfesional(EstadoProfesional.ACTIVO);
		 Assert.assertFalse(p1.equals(p2));
		 p1.setEstadoProfesional(EstadoProfesional.LICENCIA);
		 p2.setEstadoProfesional(EstadoProfesional.ACTIVO);
		 Assert.assertFalse(p1.equals(p2));
		 p1.setEstadoProfesional(EstadoProfesional.ACTIVO);
		 p2.setEstadoProfesional(EstadoProfesional.ACTIVO);
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setFechaBaja(null);
		 p2.setFechaBaja(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setFechaBaja(null);
		 p2.setFechaBaja(LocalDateTime.now());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setFechaBaja(LocalDateTime.now().plusDays(10));
		 p2.setFechaBaja(LocalDateTime.now());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setFechaBaja(LocalDateTime.now());
		 p2.setFechaBaja(p1.getFechaBaja());
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setFechaDeNacimiento(null);
		 p2.setFechaDeNacimiento(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setFechaDeNacimiento(null);
		 p2.setFechaDeNacimiento(LocalDate.now());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setFechaDeNacimiento(LocalDate.now().plusDays(10));
		 p2.setFechaDeNacimiento(LocalDate.now());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setFechaDeNacimiento(LocalDate.now());
		 p2.setFechaDeNacimiento(p1.getFechaDeNacimiento());
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setId(null);
		 p2.setId(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setId(null);
		 p2.setId(2);
		 Assert.assertFalse(p1.equals(p2));
		 p1.setId(1);
		 p2.setId(2);
		 Assert.assertFalse(p1.equals(p2));
		 p1.setId(3);
		 p2.setId(3);
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setNombre(null);
		 p2.setNombre(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setNombre(null);
		 p2.setNombre("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setNombre("d1");
		 p2.setNombre("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setNombre("d");
		 p2.setNombre("d");
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setServicios(null);
		 p2.setServicios(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setServicios(null);
		 p2.setServicios(new ArrayList<>());
		 Assert.assertFalse(p1.equals(p2));
		 List<Servicio> s = new ArrayList<>();
		 s.add(new Servicio());
		 p1.setServicios(s);
		 p2.setServicios(new ArrayList<>());
		 Assert.assertFalse(p1.equals(p2));
		 p1.setServicios(new ArrayList<>());
		 p2.setServicios(new ArrayList<>());
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setSucursales(null);
		 p2.setSucursales(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setSucursales(null);
		 p2.setSucursales("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setSucursales("d1");
		 p2.setSucursales("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setSucursales("d");
		 p2.setSucursales("d");
		 Assert.assertTrue(p1.equals(p2));
		 
		 p1.setTelefono(null);
		 p2.setTelefono(null);
		 Assert.assertTrue(p1.equals(p2));
		 p1.setTelefono(null);
		 p2.setTelefono("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setTelefono("d1");
		 p2.setTelefono("d2");
		 Assert.assertFalse(p1.equals(p2));
		 p1.setTelefono("d");
		 p2.setTelefono("d");
		 Assert.assertTrue(p1.equals(p2));
	 }
}

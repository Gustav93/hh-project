package com.hairandhead.project.profesional.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.profesional.dto.ProfesionalDTOJson;
import com.hairandhead.project.turno.service.DetalleTunoService;
import com.hairandhead.project.turno.service.TurnoService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.profesional.dto.DisponibilidadDTO;
import com.hairandhead.project.profesional.dto.ProfesionalDTO;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.profesional.service.ProfesionalService;
import com.hairandhead.project.profesional.utils.Profesionales;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.servicio.utils.Servicios;

public class ProfesionalControllerTest {

	@Mock
	@Autowired
	ProfesionalService profesionalService;
	
	@Mock
	@Autowired
	ServicioService servicioService;
	
	@Spy
	ModelMap model;
	
	@Spy
	List<Profesional> profesionales = new ArrayList<Profesional>();
	
	@Spy
	List<ProfesionalDTO> profesionalesDTO = new ArrayList<ProfesionalDTO>();
	
	@Spy
	List<Disponibilidad> disponibilidad = new ArrayList<Disponibilidad>();
	
	@Spy
	List<DisponibilidadDTO> disponibilidadDTO = new ArrayList<DisponibilidadDTO>();
	
	@Spy
	List<Servicio> servicios = new ArrayList<Servicio>();
	
	@Mock
	BindingResult result;
	 
	@Mock
	RedirectAttributes attributes;

	@InjectMocks
	ProfesionalController profesionalController;

	@Mock
	DetalleTunoService detalleTunoService;

	@Mock
	TurnoService turnoService;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		profesionales = Profesionales.getProfesionales();
		this.profesionalesDTO = Profesionales.getProfesionalesDTO();
		this.servicios = Servicios.getListServices();
		this.disponibilidad = Profesionales.getDisponibilidades();
		this.disponibilidadDTO = Profesionales.getDisponibilidadesDTO();
	}

	@Test
	public void listTest() {
		
		when(profesionalService.findAll()).thenReturn(profesionales);
		Assert.assertEquals(profesionalController.list(model), "profesionales/listProfesionales");
		verify(profesionalService, atLeastOnce()).findAll();
	}
	
	@Test
	public void createTest() {
		when(servicioService.findAllServicios()).thenReturn(this.servicios);
		Assert.assertEquals(profesionalController.create(model), "profesionales/formprofesional");
		Assert.assertNotNull(model.get("profesionalDTO"));
		Assert.assertFalse((Boolean)model.get("edit"));
	}
	
	@Test
	public void saveTest(){
		Profesional p = this.profesionales.get(0);
		ProfesionalDTO dto = this.profesionalesDTO.get(0);
		when(profesionalService.findByEmail(anyString())).thenReturn(p);
		when(profesionalService.convertToProfesional(any(ProfesionalDTO.class))).thenReturn(this.profesionales.get(0));
		doNothing().when(profesionalService).save(any(Profesional.class));
		when(profesionalService.convertToDTO(p)).thenReturn(dto);
		Assert.assertNotNull(model.get("profesionalDTO"));
		List<DisponibilidadDTO> dias = this.disponibilidadDTO;
		when(profesionalController.cargarDatosDisponibilidadDeProfesionales(p.getDisponibilidad())).thenReturn(dias);
		Assert.assertEquals(profesionalController.save(this.profesionalesDTO.get(0), model, attributes), "profesionales/registerserviciosprofesional");
	}
	
	@Test
	public void saveForsTest(){
		Profesional p = this.profesionales.get(0);
		ProfesionalDTO dto = this.profesionalesDTO.get(0);
		when(profesionalService.findByEmail(anyString())).thenReturn(p);
		when(profesionalService.convertToProfesional(any(ProfesionalDTO.class))).thenReturn(this.profesionales.get(0));
		doNothing().when(profesionalService).save(any(Profesional.class));
		when(profesionalService.convertToDTO(p)).thenReturn(dto);
		Assert.assertNotNull(model.get("profesionalDTO"));
		List<Servicio> ss = this.servicios;
		when(servicioService.findAllServicios()).thenReturn(ss);
		List<DisponibilidadDTO> dias = this.disponibilidadDTO;
		List<Servicio> ss2 = this.servicios;
		p.setServicios(ss2);
		when(profesionalController.cargarDatosDisponibilidadDeProfesionales(p.getDisponibilidad())).thenReturn(dias);
		Assert.assertEquals(profesionalController.save(this.profesionalesDTO.get(0), model, attributes), "profesionales/registerserviciosprofesional");
		Assert.assertNotNull(model.get("profesionalesServiciosList"));
		Assert.assertNotNull(model.get("listaDisponibles"));
	}
	
	
	
	
//	@Test
//	public void newServicioTest()
//	{
//		Profesional p = this.profesionales.get(0);
//        when(profesionalService.findById(anyInt())).thenReturn(p);
//	}
	
//	@Test
//	public void editTest() {
//		List<Servicio> ss = this.servicios;
//		this.profesionales.get(0).setServicios(ss);
//		Profesional p = this.profesionales.get(0);
//		when(profesionalService.findByEmail(anyString())).thenReturn(p);
//		
//		p.setServicios(this.servicios);
//		ProfesionalDTO dto = this.profesionalesDTO.get(0);
//		
//		List<DisponibilidadDTO> dias = this.disponibilidadDTO;
//		
//		when(profesionalController.cargarDatosDisponibilidadDeProfesionales(p.getDisponibilidad())).thenReturn(dias);
//		when(servicioService.findAllServicios()).thenReturn(ss);
//		
//		Assert.assertEquals(profesionalController.edit(dto.getId(), model), "profesionales/registerserviciosprofesional");
//	}
	
//	@Test
//	public void updateProfesionalTest() {
//		this.profesionales.get(0).setServicios(this.servicios);
//		this.profesionales.get(0).setDisponibilidad(this.disponibilidad);
//		Profesional p = this.profesionales.get(0);
//		ProfesionalDTO dto = this.profesionalesDTO.get(0);
//		when(profesionalService.findById(anyInt())).thenReturn(p);
//		Profesional p2 = this.profesionalService.convertToProfesional(dto);
////		p2.setServicios(p.getServicios());
//		when(this.profesionalService.convertToProfesional(dto)).thenReturn(p2);
//		
//		doNothing().when(profesionalService).update(p2);
//	    Assert.assertEquals(profesionalController.updateProfesional(dto, model), "redirect:/profesionales/list");
//	}
	
	@Test
	public void deleteTest() {
		//John estaba de vacaciones
		when(profesionalService.findById((anyInt()))).thenReturn(profesionales.get(0));
		doNothing().when(profesionalService).update(any(Profesional.class));
		when(detalleTunoService.findAllDetalleTurnosPorProfesional(anyInt())).thenReturn(new ArrayList<>());
		when(turnoService.findAllTurnosReservados()).thenReturn(new ArrayList<>());
		Assert.assertEquals(profesionalController.delete(anyInt()), "redirect:/profesionales/list");
		Assert.assertEquals(profesionales.get(0).getEstadoProfesional(), EstadoProfesional.INACTIVO);
		verify(profesionalService, atLeastOnce()).findById(anyInt());
		verify(profesionalService, atLeastOnce()).update(any(Profesional.class));
	}
	
	@Test
	public void activeTest() {
		when(profesionalService.findById((anyInt()))).thenReturn(profesionales.get(0));
		doNothing().when(profesionalService).update(any(Profesional.class));
		Assert.assertEquals(profesionalController.active(anyInt()), "redirect:/profesionales/list");
		Assert.assertEquals(profesionales.get(0).getEstadoProfesional(), EstadoProfesional.ACTIVO);
		verify(profesionalService, atLeastOnce()).findById(anyInt());
		verify(profesionalService, atLeastOnce()).update(any(Profesional.class));
	}
	
	@Test
	public void testTest() {
		List<Servicio> ss = this.servicios;
		when(profesionalController.test()).thenReturn(ss);
		Assert.assertEquals(profesionalController.test(), ss);
	}
	
	@Test  //DELETE SERVICIO
	public void updateProfesionalTest() {
		Profesional p = this.profesionales.get(0);
		p.setServicios(this.servicios);
		when(profesionalService.findById(anyInt())).thenReturn(p);
		List<DisponibilidadDTO> dias = this.disponibilidadDTO;
		when(profesionalController.cargarDatosDisponibilidadDeProfesionales(p.getDisponibilidad())).thenReturn(dias);
		Assert.assertEquals(profesionalController.updateProfesional(this.profesionalesDTO.get(0),1,model), "profesionales/registerserviciosprofesional");
		Assert.assertNotNull(model.get("profesionalesServiciosList"));
		Assert.assertNotNull(model.get("listaDisponibles"));
		Assert.assertTrue((Boolean)model.get("edit"));
	}
	
//	@Test
//	public void deleteDisponibilidadTest() {
//		Profesional p = this.profesionales.get(0);
//		p.setServicios(this.servicios);
//		when(profesionalService.findById(anyInt())).thenReturn(p);
//		Assert.assertEquals(profesionalController.deleteDisponibilidad(this.profesionalesDTO.get(0),1,1,model), "profesionales/registerserviciosprofesional");
//	}
	
	@Test
	public void newServicioTest() {
		Profesional p = this.profesionales.get(0);
		when(profesionalService.findById(anyInt())).thenReturn(p);
		
		Servicio s = this.servicios.get(0);
		when(servicioService.findServicioById(anyInt())).thenReturn(s);
		
		doNothing().when(profesionalService).update(any(Profesional.class));
		
		ProfesionalDTO dto = this.profesionalesDTO.get(0);
		when(profesionalService.convertToDTO(any(Profesional.class))).thenReturn(dto);
		
		when(servicioService.findAllServicios()).thenReturn(this.servicios);
		
		Assert.assertEquals(profesionalController.newServicio(this.profesionalesDTO.get(0), 1, model, attributes), "profesionales/registerserviciosprofesional");
		Assert.assertNotNull(model.get("profesionalDTO"));
		Assert.assertNotNull(model.get("profesionalesServiciosList"));
		Assert.assertNotNull(model.get("serviciosList"));
		Assert.assertNotNull(model.get("listaDisponibles"));
	}

	@Test
	public void listJsonTest() {
		Profesional p = this.profesionales.get(0);
		List<Profesional> profesionalList = new ArrayList<>();
		profesionalList.add(p);
		ProfesionalDTO profesionalDTO = new ProfesionalDTO();
		List<ProfesionalDTO> profesionalDTOList = new ArrayList<>();
		profesionalDTOList.add(profesionalDTO);

		List<ProfesionalDTOJson> profesionalDTOJsonList = new ArrayList<>();
		ProfesionalDTOJson profesionalDTOJson = new ProfesionalDTOJson();
		profesionalDTOJson.setText("  - ");
		profesionalDTOJsonList.add(profesionalDTOJson);

		when(profesionalService.findAllWithServiceEquals(anyInt())).thenReturn(profesionalList);
		when(profesionalService.convertToDTOList(profesionalList)).thenReturn(profesionalDTOList);

		Assert.assertEquals(profesionalController.listJson(anyInt(),LocalDate.now().toString()),
				new ResponseEntity<>(profesionalDTOJsonList, HttpStatus.OK));
	}

}

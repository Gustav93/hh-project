package com.hairandhead.project.profesional.dao;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.EstadoProfesional;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.servicio.dao.ServicioDao;
import com.hairandhead.project.servicio.model.EstadoServicio;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class ProfesionalDaoImplTest extends EntityDaoImplTest {

	@Autowired
	ProfesionalDao profesionalDao;
	
	@Autowired
	ServicioDao servicioDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Profesional.xml"));
		return dataSet;
	}
	
	@Test
	public void buscarPorIdTest() {
		Assert.assertNotNull(profesionalDao.findById(1));
		Assert.assertNull(profesionalDao.findById(3));
	}
	
	@Test
	public void findAllTest() {
		Assert.assertEquals(profesionalDao.findAll().size(), 1);
	}
	
	@Test
	public void saveTest() {
		Profesional profesional = new Profesional();
		profesional.setApellido("Lennon");
		profesional.setEmail("johnlemon@gmail.com");
		profesional.setEstadoProfesional(EstadoProfesional.VACACIONES);
		profesional.setFechaBaja(LocalDateTime.now());
		profesional.setNombre("John");
		profesional.setSucursales("HH1");
		profesional.setTelefono("1190906661");
		
		Servicio s1 = new Servicio();
		s1.setDescripcion("Corte de cabello por estilista Belga");
		s1.setEstado(EstadoServicio.INACTIVO);
		s1.setFechaDeBaja(LocalDateTime.now());
		s1.setNombre("Corte estilista");
		s1.setPrecio(new BigDecimal(1700.50));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(20));
		
		Servicio s2 = new Servicio();
		s2.setDescripcion("Corte de cabello por estilista Sueco");
		s2.setEstado(EstadoServicio.INACTIVO);
		s2.setFechaDeBaja(LocalDateTime.now());
		s2.setNombre("Corte estilista");
		s2.setPrecio(new BigDecimal(1000));
		s2.setTiempoPromedioDeDuracion(Duration.ofMinutes(30));
		
		List<Servicio> servicios = new ArrayList<Servicio>();
		servicios.add(s1);
		servicios.add(s2);
		
		profesional.setServicios(servicios);
		
		Disponibilidad d1 = new Disponibilidad();
		d1.setDia("LUNES");
		List<Horario> horariosD1 = new ArrayList<Horario>();
		Horario h1 = new Horario(LocalTime.now().minusHours(3), LocalTime.now());
		Horario h2 = new Horario(LocalTime.now().plusHours(1), LocalTime.now().plusHours(3));
		
		horariosD1.add(h1);
		horariosD1.add(h2);
		
		d1.setHoraDisponible(horariosD1);
		
		
		profesionalDao.save(profesional);
		
		Assert.assertEquals(2, profesionalDao.findAll().size());
		
		
	}
	
	
	@Test
	public void updateTest() {
		Profesional profesional = profesionalDao.findById(1);
		profesional.setServicios(new ArrayList<Servicio>());
		profesional.setNombre("Paul");
		
		profesionalDao.update(profesional);
		
		Assert.assertEquals(profesionalDao.findById(1).getNombre(), "Paul");
		Assert.assertEquals(profesionalDao.findById(1).getServicios().size(), 0);
	}
	
	@Test
	public void findByEstadoTest() {
		Assert.assertNotNull(profesionalDao.findByEstado(EstadoProfesional.VACACIONES));
	}
	
	
	@Test
	public void findByMailTest() {
		Assert.assertEquals(profesionalDao.findByEmail("gercos@gmail.com").getEmail(), "gercos@gmail.com");
	}
	
	//Faltan ver datos de hibernate
	@Test
	public void  findAllWithServiceEqualsTest() {
		Profesional profesional = new Profesional();
		profesional.setApellido("Lennon");
		profesional.setEmail("johnlemon@gmail.com");
		profesional.setEstadoProfesional(EstadoProfesional.VACACIONES);
		profesional.setFechaBaja(LocalDateTime.now());
		profesional.setNombre("John");
		profesional.setSucursales("HH1");
		profesional.setTelefono("1190906661");
		
		Servicio s1 = new Servicio();
		s1.setDescripcion("Corte de cabello por estilista Belga");
		s1.setEstado(EstadoServicio.INACTIVO);
		s1.setFechaDeBaja(LocalDateTime.now());
		s1.setNombre("Corte estilista");
		s1.setPrecio(new BigDecimal(1700.50));
		s1.setTiempoPromedioDeDuracion(Duration.ofMinutes(20));
		servicioDao.save(s1);
		
		Servicio s2 = new Servicio();
		s2.setDescripcion("Corte de cabello por estilista Sueco");
		s2.setEstado(EstadoServicio.INACTIVO);
		s2.setFechaDeBaja(LocalDateTime.now());
		s2.setNombre("Corte estilista");
		s2.setPrecio(new BigDecimal(1000));
		s2.setTiempoPromedioDeDuracion(Duration.ofMinutes(30));
		servicioDao.save(s2);
		
		
		List<Servicio> servicios = new ArrayList<Servicio>();
		servicios.add(s1);
		servicios.add(s2);
		
		profesional.setServicios(servicios);
		
		Disponibilidad d1 = new Disponibilidad();
		d1.setDia("LUNES");
		List<Horario> horariosD1 = new ArrayList<Horario>();
		Horario h1 = new Horario(LocalTime.now().minusHours(3), LocalTime.now());
		Horario h2 = new Horario(LocalTime.now().plusHours(1), LocalTime.now().plusHours(3));
		
		horariosD1.add(h1);
		horariosD1.add(h2);
		
		d1.setHoraDisponible(horariosD1);
		
		
		profesionalDao.save(profesional);
		
		Assert.assertEquals(profesionalDao.findAllProfesionalesActivosConServicio(1) , new ArrayList<Profesional>());
		
	}
	
	
	
	
}

package com.hairandhead.project.turno.utils;

import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.profesional.utils.Profesionales;
import com.hairandhead.project.servicio.utils.Servicios;
import com.hairandhead.project.turno.model.DetalleTurno;

public class DetalleTurnos {

	public static List<DetalleTurno> getDestallesTurnos()
	{
		List<DetalleTurno> lista = new ArrayList<>();
		
		DetalleTurno dt1 = new DetalleTurno();
		dt1.setId(1);
		dt1.setProfesional(Profesionales.getProfesionales().get(0));
		dt1.setServicio(Servicios.getListServices().get(0));
		
		DetalleTurno dt2 = new DetalleTurno();
		dt2.setId(2);
		dt2.setProfesional(Profesionales.getProfesionales().get(1));
		dt2.setServicio(Servicios.getListServices().get(1));
		
		lista.add(dt1);
		lista.add(dt2);
		
		return lista;
	}
}

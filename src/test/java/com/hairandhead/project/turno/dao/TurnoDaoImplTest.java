package com.hairandhead.project.turno.dao;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class TurnoDaoImplTest extends EntityDaoImplTest{
	
	@Autowired
	TurnoDao turnoDao;
	
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Turno.xml"));
		return dataSet;
	}
	
	@Test
	public void findByIdTest() {
		Assert.assertNotNull(turnoDao.findById(1));
		Assert.assertNull(turnoDao.findById(3));
	}
	@Test
	public void findAllTest() {
		Assert.assertEquals(turnoDao.findAllTurnos().size(), 1);
	}
	@Test
	public void deleteByIdTest() {
		turnoDao.deleteById(1);
		Assert.assertEquals(turnoDao.findAllTurnos().size(), 0);
	}
	@Test
	public void saveTest() {
		Turno t = new Turno();
		t.setAdministrativo("Admin1");
		
		turnoDao.save(t);
		
		Assert.assertEquals(turnoDao.findAllTurnos().size(), 2);
	}
	
	@Test
	public void updateTest() {
		Turno t = turnoDao.findById(1);
		t.setAdministrativo("German");
		
		turnoDao.update(t);
		
		Assert.assertEquals(turnoDao.findById(1).getAdministrativo(), "German");
	}

	@Test
	public void findAllTurnosReservadosTest(){
		Assert.assertEquals(turnoDao.findAllTurnosReservados().size(), 1);
	}
}

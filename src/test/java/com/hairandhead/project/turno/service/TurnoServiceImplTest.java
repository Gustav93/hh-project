package com.hairandhead.project.turno.service;

import com.hairandhead.project.config.model.DataConfig;
import com.hairandhead.project.config.service.DataConfigService;
import com.hairandhead.project.promocion.model.EstadoPromocion;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.turno.dao.TurnoDao;
import com.hairandhead.project.turno.dto.DetalleTurnoFrontDTO;
import com.hairandhead.project.turno.dto.EventoDTO;
import com.hairandhead.project.turno.dto.HorarioDTO;
import com.hairandhead.project.turno.dto.TurnoFrontDTO;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.EstadoTurno;
import com.hairandhead.project.turno.model.Horario;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.utils.DetalleTurnos;
import com.hairandhead.project.turno.utils.Turnos;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class TurnoServiceImplTest {

    @Mock
    DataConfigService dataConfigService;

    @Mock
    PromocionService promocionService;

    @Mock
    TurnoDao turnoDao;

    @InjectMocks
    TurnoServiceImpl turnoService;

    @Spy
    List<Turno> turnoList = new ArrayList<>();

    
    @Spy
    List<DetalleTurno> detalleTurnos = new ArrayList<>();


    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        turnoList = Turnos.getTurnos();
        detalleTurnos = DetalleTurnos.getDestallesTurnos();
    }

    @Test
    public void findAllTurnos(){
        when(turnoDao.findAllTurnos()).thenReturn(turnoList);
        Assert.assertEquals(turnoService.findAllTurnos(), turnoList);
    }
    
    @Test
    public void getById(){
    	 when(turnoDao.findById(anyInt())).thenReturn(turnoList.get(0));
         Assert.assertEquals(turnoService.findById(anyInt()), turnoList.get(0));
    }
     
    @Test
    public void saveTurno(){
    	 doNothing().when(turnoDao).save(any(Turno.class));
    	 turnoService.saveTurno(any(Turno.class));
    	 verify(turnoDao, atLeastOnce()).save(any(Turno.class));
    }
    
 	@Test
    public void updateTurno()
    {	
		doNothing().when(turnoDao).save(any(Turno.class));
        turnoService.updateTurno(any(Turno.class));
        verify(turnoDao, atLeastOnce()).update(any(Turno.class));
    }
 	
	@Test
	public void findByEmail() {
		 when(turnoDao.findTurnoByEmail(anyString())).thenReturn(turnoList.get(0));
         Assert.assertEquals(turnoService.findByEmail(anyString()), turnoList.get(0));
	}
	
	@Test
    public void deleteTurnoTest(){
        doNothing().when(turnoDao).deleteById(anyInt());
        turnoService.deleteTurnoById(anyInt());
        verify(turnoDao, atLeastOnce()).deleteById(anyInt());
    }
	
	@Test
    public void isTurnoUniqueTrueTest(){
        Turno t = turnoList.get(0);
        when(turnoDao.findById(anyInt())).thenReturn(t);
        Assert.assertTrue(turnoService.isUnique(t.getId()));
    }
	
	@Test
    public void isTurnoUniqueFalseTest(){
        Turno t = turnoList.get(0);
        when(turnoDao.findById(anyInt())).thenReturn(t);
        Assert.assertFalse(turnoService.isUnique(5));
    }

    @Test
    public void getPromotionsAvailableTest(){
        Turno t = turnoList.get(0);
        List<Promocion> promos = new ArrayList<>();
        when(promocionService.findByEstado(EstadoPromocion.ACTIVO)).thenReturn(promos);
        when(turnoDao.findById(anyInt())).thenReturn(t);
        Assert.assertEquals(turnoService.getPromotionsAvailable(t), promos);
    }

    @Test
    public void convertToTurnoFrontDTOTest(){
        Turno t = turnoList.get(0);
        TurnoFrontDTO turnoFrontDTO = new TurnoFrontDTO();
        turnoFrontDTO.setTurnoId("1");
        turnoFrontDTO.setStatus("RESERVADO");
        turnoFrontDTO.setTotalPrice("123");

        when(turnoDao.findById(anyInt())).thenReturn(t);
        Assert.assertEquals(turnoService.convertToTurnoFrontDTO(t), turnoFrontDTO);
    }

    @Test
    public void convertTurnoToEventDTOTest(){
        Turno t = turnoList.get(0);
        List<EventoDTO> turnoDto = new ArrayList<>();
        EventoDTO eventoDTO = new EventoDTO();
        eventoDTO.setId(1);
        eventoDTO.setTitle(" -  ");
        turnoDto.add(eventoDTO);

        when(turnoDao.findById(anyInt())).thenReturn(t);
        Assert.assertEquals(turnoService.convertTurnoToEventDTO(t), turnoDto);
    }

    @Test
    public void convertToDetalleTurnoFrontDTOListTest() {
        Turno turno = turnoList.get(0);
        List<DetalleTurnoFrontDTO> detalleTurnoFrontDTOList = new ArrayList<>();
        DetalleTurnoFrontDTO detalleTurnoFrontDTO = new DetalleTurnoFrontDTO();
        detalleTurnoFrontDTO.setId("1");
        detalleTurnoFrontDTO.setPrice("123");
        detalleTurnoFrontDTO.setHoraInicio("00:00");
        detalleTurnoFrontDTO.setHoraFin("00:15");
        detalleTurnoFrontDTOList.add(detalleTurnoFrontDTO);

        Assert.assertEquals(turnoService.convertToDetalleTurnoFrontDTOList(turno), detalleTurnoFrontDTOList);
    }

    @Test
    public void convertToHorarioDTOTest(){
        LocalTime start = LocalTime.of(0,0);
        LocalTime end = LocalTime.of(0,15);
        Horario horario = new Horario(start, end);
        HorarioDTO horarioDTO = new HorarioDTO();
        horarioDTO.setHoraInicio("00:00");
        horarioDTO.setHoraFin("00:15");
        horarioDTO.setPorcentaje("2%");
        horarioDTO.setDisponible(true);
        horarioDTO.setLibre(false);
        Assert.assertEquals(turnoService.convertToHorarioDTO(horario), horarioDTO);
    }

    @Test
    public void getExpiredTurnosTest() {
        Turno turno = new Turno();
        turno.setHoraInicio(LocalTime.of(0,0));
        turno.setHoraFin(LocalTime.of(0,20));
        turno.setEstadoTurno(EstadoTurno.AUSENTE);
        List<Turno> turnoList = new ArrayList<>();
        turnoList.add(turno);
        when(turnoDao.findAllTurnos()).thenReturn(turnoList);
        when(dataConfigService.findByProperty(anyString())).thenReturn(new DataConfig());
        Assert.assertEquals(turnoService.getExpiredTurnos(),new ArrayList<>());
    }

    @Test
    public void findAllBetweenAndSucursalEqualsTest() {
        Turno turno = new Turno();
        Sucursal sucursal = new Sucursal();
        sucursal.setId(1);
        sucursal.setNombre("sdf");
        turno.setSucursal(sucursal);
        turno.setFecha(LocalDate.now());
        turno.setHoraInicio(LocalTime.of(0,0));
        turno.setHoraFin(LocalTime.of(0,20));
        turno.setEstadoTurno(EstadoTurno.AUSENTE);
        List<Turno> turnoList = new ArrayList<>();
        turnoList.add(turno);
        when(turnoDao.findAllTurnos()).thenReturn(turnoList);
        when(dataConfigService.findByProperty(anyString())).thenReturn(new DataConfig());
        Assert.assertEquals(turnoService.findAllBetweenAndSucursalEquals(LocalDate.now(), LocalDate.now(), ""),new ArrayList<>());
    }
}

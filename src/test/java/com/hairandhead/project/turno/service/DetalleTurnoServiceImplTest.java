package com.hairandhead.project.turno.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.hairandhead.project.turno.utils.DetalleTurnos;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.turno.dao.DetalleTurnoDao;
import com.hairandhead.project.turno.model.DetalleTurno;

public class DetalleTurnoServiceImplTest {
	
	@Mock
    DetalleTurnoDao dao;

    @InjectMocks
    DetalleTurnoServiceImpl detalleTurnoturnoService;

    @Spy
    List<DetalleTurno> detalleTurnos = new ArrayList<>();

    @BeforeClass
    public void setUp(){
        detalleTurnos = DetalleTurnos.getDestallesTurnos();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByIdTest(){
    	DetalleTurno dt = this.detalleTurnos.get(0);
        when(dao.findById(anyInt())).thenReturn(dt);
        Assert.assertEquals(detalleTurnoturnoService.findById(dt.getId()),dt);
    }
    
    @Test
    public void saveDetalleTurnoTest(){
        doNothing().when(dao).save(any(DetalleTurno.class));
        detalleTurnoturnoService.save(any(DetalleTurno.class));
        verify(dao, atLeastOnce()).save(any(DetalleTurno.class));
    }
    
    @Test
    public void deleteDetalleTurnoTest(){
        doNothing().when(dao).delete(any(DetalleTurno.class));
        detalleTurnoturnoService.delete(any(DetalleTurno.class));
        verify(dao, atLeastOnce()).delete(any(DetalleTurno.class));
    }

    @Test
    public void deleteDetalleTurnoByIdTest(){
        DetalleTurno detalleTurno = new DetalleTurno();
        detalleTurno.setId(1);
        doNothing().when(dao).delete(any(DetalleTurno.class));
        when(dao.findById(anyInt())).thenReturn(detalleTurno);
        detalleTurnoturnoService.delete(1);
    }

    @Test
    public void updateDetaleTurnoTest(){
        DetalleTurno dt = this.detalleTurnos.get(0);
        when(dao.findById(anyInt())).thenReturn(dt);
        detalleTurnoturnoService.update(dt);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
}

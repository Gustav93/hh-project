package com.hairandhead.project.turno.controller;

import static org.mockito.Mockito.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.*;

import com.hairandhead.project.cliente.dto.ClienteJsonDTO;
import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteService;
import com.hairandhead.project.profesional.model.Disponibilidad;
import com.hairandhead.project.profesional.model.Horario;
import com.hairandhead.project.profesional.model.Profesional;
import com.hairandhead.project.profesional.service.ProfesionalService;
import com.hairandhead.project.promocion.model.Promocion;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.service.PromocionAplicadaService;
import com.hairandhead.project.promocion.service.PromocionService;
import com.hairandhead.project.servicio.dto.ServicioJsonDTO;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;
import com.hairandhead.project.turno.dto.*;
import com.hairandhead.project.turno.service.DetalleTunoService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;
import com.hairandhead.project.turno.utils.Turnos;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.servicio.utils.Servicios;

public class TurnoControllerTest {

	@Mock
	SucursalService sucursalService;

	@Mock
	RedirectAttributes	redirectAttributes;

	@Mock
	ProfesionalService profesionalService;

	@Mock
	PromocionService promocionService;

	@Mock
	PromocionAplicadaService promocionAplicadaService;

	@Mock
	ClienteService clienteService;

	@Mock
	TurnoService serviceTurno;

	@Mock
	DetalleTunoService detalleTunoService;

	@Mock
	ServicioService serviceServicio;

	@InjectMocks
	AppControllerTurno turnoController;
	
	@Mock
	MessageSource messageSource;

	@Spy
	List<Turno> turnos = new ArrayList<Turno>();

	@Spy
	List<TurnoFrontDTO> turnosFrontDTO = new ArrayList<TurnoFrontDTO>();

	@Spy
	List<Servicio> servicios = new ArrayList<Servicio>();

	@Spy
	List<EventoDTO> eventos = new ArrayList<EventoDTO>();

	@Spy
	ModelMap model;

	@Mock
	BindingResult result;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		turnos = Turnos.getTurnos();
		turnosFrontDTO = Turnos.getListTurnosFrontDTO();
		servicios = Servicios.getListServices();
		eventos = Turnos.getListEventDTO();
	}

	@Test
	public void showCalendarTest() {
		Assert.assertEquals(turnoController.listTurnos(model), "/turnos/calendar");
	}

	     @Test
	     public void listTurnosTest(){
	         when(serviceTurno.findAllTurnos()).thenReturn(turnos);
	         Assert.assertEquals(serviceTurno.findAllTurnos(), turnos);
	         verify(serviceTurno, atLeastOnce()).findAllTurnos();
			 Assert.assertEquals(turnoController.listAllTurnos( model),
					 "/turnos/listTurnoFrontDTO");
	     }

	@Test
	public void createTurnoTest() {
		Turno t = null;
		TurnoFrontDTO turnoDTO = turnosFrontDTO.get(0);
		List<Servicio> s = new ArrayList<Servicio>();

		when(serviceTurno.findByEmail((anyString()))).thenReturn(t);
		when(serviceTurno.convertToTurnoFrontDTO(any(Turno.class))).thenReturn(turnoDTO);
		when(serviceServicio.findAllServicios()).thenReturn(s);

		Assert.assertEquals(turnoController.newTurno( model),
				"/turnos/newregistrationTurno");
	}

	@Test
	public void getClientesTest() {
		Cliente cliente = new Cliente();
		cliente.setId(1);
		cliente.setNombre("pepe");
		List<Cliente> clientes = new ArrayList<>();
		clientes.add(cliente);

		ClienteJsonDTO clienteJsonDTO = new ClienteJsonDTO();
		clienteJsonDTO.setText("pepe  - ");
		clienteJsonDTO.setValue(1);

		List<ClienteJsonDTO> clienteJsonDTOList = new ArrayList<>();
		clienteJsonDTOList.add(clienteJsonDTO);

		when(clienteService.findAllClientesActivos()).thenReturn(clientes);

		Assert.assertEquals(turnoController.getClientes(),
				new ResponseEntity<>(clienteJsonDTOList, HttpStatus.OK));
	}

	@Test
	public void listTest() {
		Turno turno = turnos.get(1);
		List<Turno> turnos = new ArrayList<>();
		turnos.add(turno);

		List<EventoDTO> turnoDto = new ArrayList<>();
		EventoDTO eventoDTO = new EventoDTO();
		turnoDto.add(eventoDTO);

		when(serviceTurno.findAllTurnos()).thenReturn(turnos);
		when(serviceTurno.convertTurnoToEventDTO(any(Turno.class))).thenReturn(turnoDto);

		Assert.assertEquals(turnoController.list(),
				new ResponseEntity<>(turnoDto, HttpStatus.OK));
	}

	@Test
	public void showDeailsTest() {
		Turno turno = turnos.get(1);

		when(serviceTurno.findById(anyInt())).thenReturn(turno);
		when(serviceTurno.getPromotionsAvailable(any(Turno.class))).thenReturn(new HashSet<>());
		when(serviceTurno.convertToTurnoFrontDTO(any(Turno.class))).thenReturn(new TurnoFrontDTO());
		when(serviceTurno.convertToDetalleTurnoFrontDTOList(any(Turno.class))).thenReturn(new ArrayList<>());
		when(promocionAplicadaService.convertPromocionAplicadaToDTOList(turno.getPromociones())).thenReturn(new ArrayList<>());

		Assert.assertEquals(turnoController.showDetails(anyInt(), model),"/turnos/resumenturno");
	}

	@Test
	public void addPromoTest() {
		Turno turno = turnos.get(1);
		Promocion promo = new Promocion();

		when(serviceTurno.findById(anyInt())).thenReturn(turno);
		when(promocionService.findPromocionById(anyInt())).thenReturn(promo);

		Assert.assertEquals(turnoController.addPromo(anyInt(), 1, model),"redirect:/turnos/verdetalle");
	}

	@Test
	public void deletePromoTest() {
		Turno turno = turnos.get(0);
		Promocion promo = new Promocion();

		when(serviceTurno.findById(anyInt())).thenReturn(turno);
		when(promocionService.findPromocionById(anyInt())).thenReturn(promo);

		Assert.assertEquals(turnoController.deletePromo(anyInt(), 1, model),"redirect:/turnos/verdetalle");
	}


//	@Test
//	public void createReservaDisponibleTest() {
//	}

	@Test
	public void createReservaNoDisponibleTest() {
		Turno turno = turnos.get(0);
		turno.setFecha(LocalDate.of(2019,11,20));
		Profesional profesional = new Profesional();
		profesional.setId(1);

		Disponibilidad disponibilidad = new Disponibilidad();
		disponibilidad.setId(1);
		disponibilidad.setDia("MIERCOLES");

		com.hairandhead.project.profesional.model.Horario horario = new com.hairandhead.project.profesional.model.Horario();
		horario.setId(1);
		horario.setHoraInicio(LocalTime.of(0,0));
		horario.setHoraFin(LocalTime.of(23,59));
		List<Horario> horarios = new ArrayList<>();
		horarios.add(horario);
		disponibilidad.setHoraDisponible(horarios);
		List<Disponibilidad> disponibilidadList = new ArrayList<>();
		disponibilidadList.add(disponibilidad);

		profesional.setDisponibilidad(disponibilidadList);
		Servicio servicio = new Servicio();
		servicio.setId(2);
		servicio.setTiempoPromedioDeDuracion(Duration.ofMinutes(30));

		when(serviceTurno.findById(anyInt())).thenReturn(turno);
		when(profesionalService.findById(anyInt())).thenReturn(profesional);
		when(serviceServicio.findServicioById(anyInt())).thenReturn(servicio);

		Assert.assertEquals(turnoController.createReserva(anyInt(), 1, 1, "00:30", model, redirectAttributes),"redirect:/turnos/verdetalle");
	}


	@Test
	public void listServiciosJsonTest() {
		Servicio servicio = servicios.get(0);
		List<Servicio> servicios = new ArrayList<>();
		servicios.add(servicio);
		ServicioJsonDTO servicioJsonDTO = new ServicioJsonDTO();
		servicioJsonDTO.setText("Peluquería");
		servicioJsonDTO.setValue(1);
		List<ServicioJsonDTO> servicioJsonDTOList = new ArrayList<>();
		servicioJsonDTOList.add(servicioJsonDTO);

		when(serviceServicio.findAllServiciosActivos()).thenReturn(servicios);

		Assert.assertEquals(turnoController.listServiciosJson(),
				new ResponseEntity<>(servicioJsonDTOList, HttpStatus.OK));
	}

	@Test
	public void saveTurnoTest() {
		TurnoClienteDTO turnoClienteDTO = new TurnoClienteDTO();
		turnoClienteDTO.setCliente("1");
		turnoClienteDTO.setSucursal("1");
		turnoClienteDTO.setFecha("04/11/2019");
		Sucursal sucursal = new Sucursal();
		sucursal.setId(1);
		when(sucursalService.buscarPorId(anyInt())).thenReturn(sucursal);
		when(result.hasErrors()).thenReturn(false);
		doNothing().when(serviceTurno).saveTurno(any(Turno.class));
		when(serviceTurno.isUnique(anyInt())).thenReturn(true);
		Assert.assertEquals(turnoController.createTurno(turnoClienteDTO, model), "redirect:/turnos/verdetalle");
	}

	@Test
	public void saveTurnoErrorTest() {
		TurnoClienteDTO turnoClienteDTO = new TurnoClienteDTO();
		turnoClienteDTO.setCliente("1");
		turnoClienteDTO.setSucursal("1");
		turnoClienteDTO.setFecha("04/11/2019");
		Sucursal sucursal = new Sucursal();
		sucursal.setId(1);
		when(sucursalService.buscarPorId(anyInt())).thenReturn(sucursal);
		when(result.hasErrors()).thenReturn(true);
		doNothing().when(serviceTurno).saveTurno(any(Turno.class));
		Assert.assertEquals(turnoController.createTurno(turnoClienteDTO, model), "redirect:/turnos/verdetalle");
	}

	@Test
	public void saveTurnoNoUniqueTest() {
		TurnoClienteDTO turnoClienteDTO = new TurnoClienteDTO();
		turnoClienteDTO.setCliente("1");
		turnoClienteDTO.setSucursal("1");
		turnoClienteDTO.setFecha("04/11/2019");
		Sucursal sucursal = new Sucursal();
		sucursal.setId(1);
		when(result.hasErrors()).thenReturn(false);
		doNothing().when(serviceTurno).saveTurno(any(Turno.class));
		when(serviceTurno.isUnique(anyInt())).thenReturn(false);
		when(sucursalService.buscarPorId(anyInt())).thenReturn(sucursal);
		result.addError(new FieldError("turno","ssoId",messageSource.getMessage("non.unique.ssoId", new String[]{this.turnos.get(0).getId().toString()}, Locale.getDefault())));
		Assert.assertEquals(turnoController.createTurno(turnoClienteDTO, model), "redirect:/turnos/verdetalle");
	}

	@Test
	public void editTest() {
		Turno turno = turnos.get(0);
		when(result.hasErrors()).thenReturn(false);
		when(serviceTurno.findById(anyInt())).thenReturn(turno);
		Assert.assertEquals(turnoController.edit(turnosFrontDTO.get(0), model), "/turnos/resumenturno");
	}

	@Test
	public void deleteServicioTest() {
		Turno s = turnos.get(0);
		when(serviceTurno.findById(anyInt())).thenReturn(s);
		when(detalleTunoService.findById(anyInt())).thenReturn(s.getDetalleTurnos().get(0));
		Assert.assertEquals(turnoController.deleteReserva(anyInt(), 1, model), "redirect:/turnos/verdetalle");
	}
}

package com.hairandhead.project.reporte.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hairandhead.project.pago.model.DetallePago;
import com.hairandhead.project.pago.model.EstadoPago;
import com.hairandhead.project.pago.model.Pago;
import com.hairandhead.project.pago.model.TipoDetallePago;
import com.hairandhead.project.profesional.model.Profesional;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.pago.utils.Pagos;
import com.hairandhead.project.promocion.model.PromocionAplicada;
import com.hairandhead.project.promocion.model.ServicioPromocionAplicada;
import com.hairandhead.project.reportes.service.Reporte;
import com.hairandhead.project.reportes.service.ReporteServiceImpl;
import com.hairandhead.project.servicio.dto.ServicioDTO;
import com.hairandhead.project.servicio.model.Servicio;
import com.hairandhead.project.servicio.service.ServicioService;
import com.hairandhead.project.turno.model.DetalleTurno;
import com.hairandhead.project.turno.model.Turno;
import com.hairandhead.project.turno.service.TurnoService;

public class ReporteServiceImplTest {
	@Mock
	TurnoService turnoService;

	@Mock
	ServicioService servicioService;
	
	@InjectMocks
	ReporteServiceImpl reporteService;
	
	
	List<Turno> turnos;
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		turnos = getTurnos();
	}
	
	@Test
	public void serviciosTest() {
		when(servicioService.convertToDTO(any(Servicio.class))).thenReturn(new ServicioDTO());
		when(turnoService.findAllBetween(any(LocalDate.class),any(LocalDate.class))).thenReturn(turnos);
		Assert.assertNotNull(reporteService.servicios("1", "01/01/2020", "31/01/2020"));
	}
	
	@Test
	public void profesionalesTest() {
		Turno turno = turnos.get(0);
		Pago pago = new Pago();
		pago.setEstado(EstadoPago.PAGADO);
		DetallePago detallePago = new DetallePago();
		detallePago.setMonto(new BigDecimal("300"));
		detallePago.setTipo(TipoDetallePago.EFECTIVO);
		List<DetallePago> detallePagoList = new ArrayList<>();
		detallePagoList.add(detallePago);
		pago.setDetallePago(detallePagoList);
		turno.setPago(pago);
		List<Turno> turnoList = new ArrayList<>();
		turnoList.add(turno);
		when(turnoService.findAllBetween(any(LocalDate.class),any(LocalDate.class))).thenReturn(turnoList);
		Assert.assertNotNull(reporteService.profesionales("1", "01/01/2020", "31/01/2020"));
	}
	
	
	public static List<Turno> getTurnos()
	{
		List<Turno> lista = new ArrayList<>();
		
		Turno t1 = new Turno();
		t1.setId(1);
		t1.setAdministrativo("Lucas");
		try{
			t1.setFechaAlta(LocalDateTime.parse("23-08-2019T10:00:00"));
			t1.setFecha(LocalDate.now());
			t1.setHoraInicio(LocalTime.now().plus(5, ChronoUnit.MINUTES));
			t1.setHoraFin(LocalTime.parse("12:00:00"));
		}
		catch(DateTimeParseException e){
		    e.getMessage();
		}
		ArrayList<DetalleTurno> detalleTurnos1 = new ArrayList<>();
		DetalleTurno detalleTurno1 = new DetalleTurno();
		detalleTurno1.setId(1);
		Profesional profesional = new Profesional();
		detalleTurno1.setProfesional(profesional);
		Servicio servicio = new Servicio();
		servicio.setId(1);
		servicio.setPrecio(new BigDecimal(123));
		servicio.setNombre("Botox");
		servicio.setDescripcion("Pomulos perfectos");
		detalleTurno1.setServicio(servicio);

		detalleTurnos1.add(detalleTurno1);
		t1.setDetalleTurnos(detalleTurnos1);

		ServicioPromocionAplicada servicioAplicado = new ServicioPromocionAplicada();
		servicioAplicado.setId(1);
		servicioAplicado.setPrecio(new BigDecimal(123));
		
		PromocionAplicada promo = new PromocionAplicada();
		promo.setId(1);
		List<ServicioPromocionAplicada> serviciosAplicados = new ArrayList<>();
		serviciosAplicados.add(servicioAplicado);
		promo.setServicios(serviciosAplicados);
		List<PromocionAplicada> promocionList = new ArrayList<>();
		promocionList.add(promo);

		t1.setPromociones(promocionList);

		t1.setCliente(new Cliente());
		t1.setPago(Pagos.getListPagos().get(0));

		
		lista.add(t1);
		
		return lista;
	}
	
	
}

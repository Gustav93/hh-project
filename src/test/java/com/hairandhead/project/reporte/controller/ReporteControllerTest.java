package com.hairandhead.project.reporte.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hairandhead.project.reportes.controller.ReporteController;
import com.hairandhead.project.reportes.service.ReporteService;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.sucursal.service.SucursalService;

import net.sf.jasperreports.engine.JRException;


public class ReporteControllerTest {
	@Mock
	ReporteService reporteService;
	
	@Mock
	SucursalService sucursalService;
	
	@InjectMocks
	ReporteController controller;
	
	@BeforeMethod
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void listReportesTest() {
		Assert.assertEquals(controller.list(), "/reportes/listReportes");
	}
	
	@Test(expectedExceptions = NullPointerException.class)
	public void servicioMasRequeridoTest() throws IOException, JRException {
		
		//doNothing().when(controller.recaudacionPorProfesional("01/01/2020", "2020", sucursal, response);))
		Sucursal s = new Sucursal();
		s.setNombre("San Miguel");
		when(sucursalService.buscarPorId(any(Integer.class))).thenReturn(s);
		//when(reporteService.servicios(s.getNombre(), "01/01/2020", "31/01/2020")).thenReturn(new JasperPrint());
		//doNothing().when(jasperExportManager.exportReportToPdfStream(reporteService.servicios(s.getNombre(), "01/01/2020", "31/01/2020"), new OutputStream);
		controller.servicioMasRequerido("01/01/2020", "31/01/2020", "1", null);
		verify(sucursalService, atLeastOnce()).buscarPorId(any(Integer.class));
	}
	
	@Test(expectedExceptions = NullPointerException.class)
	public void recaudacionPorProfesionalTest() throws IOException, JRException {
		Sucursal s = new Sucursal();
		s.setNombre("San Miguel");
		when(sucursalService.buscarPorId(any(Integer.class))).thenReturn(s);
		controller.recaudacionPorProfesional("01/01/2020", "31/01/2020", "1", null);
		verify(sucursalService, atLeastOnce()).buscarPorId(any(Integer.class));
	}
	
}

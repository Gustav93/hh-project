package com.hairandhead.project.sucursal.dao;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.hairandhead.project.sucursal.model.EstadoSucursal;
import com.hairandhead.project.sucursal.model.Sucursal;
import com.hairandhead.project.utils.EntityDaoImplTest;

public class SucursalDaoImplTest extends EntityDaoImplTest {

	@Autowired
	SucursalDao sucursalDao;

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Sucursal.xml"));
		return dataSet;
	}

	@Test
	public void buscarPorIdTest() {
		Assert.assertNotNull(sucursalDao.buscarPorId(1));
		Assert.assertNull(sucursalDao.buscarPorId(3));
	}

	@Test
	public void obtenerSucursalesTest() {
		Assert.assertEquals(sucursalDao.obtenerSucursales().size(), 2);
	}

	@Test
	public void guardarTest() {
		Sucursal sucursal = new Sucursal();
		sucursal.setEstado(EstadoSucursal.BAJA);
		sucursal.setLocalidad("San Miguel");
		sucursal.setNombre("HHViejo");

		sucursalDao.guardar(sucursal);

		Assert.assertEquals(3, sucursalDao.obtenerSucursales().size());
	}
	
	@Test
	public void updateTest() {
		Sucursal sucursal = sucursalDao.buscarPorId(1);
		sucursal.setNombre("HHPolvorines");
		
		sucursalDao.update(sucursal);
		
		Assert.assertEquals("HHPolvorines", sucursalDao.buscarPorId(1).getNombre());
	}

}

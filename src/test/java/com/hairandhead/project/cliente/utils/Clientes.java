package com.hairandhead.project.cliente.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalDateTime;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.model.EstadoCliente;

public class Clientes {

	public static List<Cliente> getClientesList() {
		
		List<Cliente> c = new ArrayList<>();
		
		Cliente c1 = new Cliente();
		c1.setId(1);
		c1.setNombre("Cesar");
		c1.setApellido("Niveyro");
		c1.setEmail("cehn17@gmail.com");
		c1.setTelefono("1161137368");
		
		Cliente c2 = new Cliente();
		c2.setId(2);
		c2.setNombre("Gustav");
		c2.setApellido("Sanchez");
		c2.setEmail("vizaral2@gmail.com");
		c2.setTelefono("1137941577");
		c2.setEstado(EstadoCliente.INACTIVO);
		c2.setFechaDeBaja(LocalDateTime.now());
		c2.setPuntosAcumulados(c2.getPuntosAcumulados()+10);
		
		Cliente c3 = new Cliente();
		c3.setId(3);
		c3.setNombre("Lalo");
		c3.setApellido("Landa");
		c3.setEmail("lalolanda@gmail.com");
		c3.setTelefono("1123564512");
		
		c.add(c1);
		c.add(c2);
		c.add(c3);
		
		return c;
	}

}

package com.hairandhead.project.cliente.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.service.ClienteService;
import com.hairandhead.project.cliente.utils.Clientes;

public class ClienteControllerTest {

	@Mock
	ClienteService service;

//	@Mock
//	MessageSource message;

	@InjectMocks
	ClienteController clienteController;

	@Spy
	List<Cliente> clientes = new ArrayList<Cliente>();

	@Spy
	ModelMap model;

	@Mock
	BindingResult result;

	@BeforeClass
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		clientes = Clientes.getClientesList();
	}
	
	@Test
    public void listClienteTest(){
        when(service.findAllClientes()).thenReturn(this.clientes);
        Assert.assertEquals(clienteController.list(model), "clientes/listClientes");
        Assert.assertEquals(model.get("clientes"), this.clientes);
        verify(service, atLeastOnce()).findAllClientes();
    }
	
	@Test
    public void createTest(){
        Assert.assertEquals(clienteController.create(model), "clientes/formCliente");
        Assert.assertNotNull(model.get("cliente"));
        Assert.assertFalse((Boolean)model.get("edit"));
        Assert.assertNull(((Cliente)model.get("cliente")).getId());
    }
	
	@Test
    public void saveClienteWithValidationErrorTest(){
        when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).saveCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.save(clientes.get(0), model, result), "clientes/formCliente");
    }
	
	@Test
    public void saveClienteActivoSuccessfullTest(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).saveCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.save(clientes.get(2), model, result), "redirect:/clientes/list");
        Assert.assertEquals(model.get("mensaje"), "Cliente: Lalo Landa, Fue guardado con exito!!");
    }
	
	@Test
    public void saveClienteINActivoSuccessfullTest(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).saveCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.save(clientes.get(1), model, result), "redirect:/clientes/list");
        Assert.assertEquals(model.get("mensaje"), "Cliente: Gustav Sanchez, Fue guardado con exito!!");
    }
	
	@Test
    public void editClienteTest(){
        Cliente c = clientes.get(0);
        when(service.findClienteById(anyInt())).thenReturn(c);
        Assert.assertEquals(clienteController.edit(anyInt(), model), "clientes/formCliente");
        Assert.assertNotNull(model.get("cliente"));
        Assert.assertTrue((Boolean)model.get("edit"));
        Assert.assertEquals(((Cliente)model.get("cliente")).getId(), new Integer(1));
    }
	
	@Test
    public void updateClienteWithValidationErrorTest(){
        when(result.hasErrors()).thenReturn(true);
        doNothing().when(service).updateCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.update(clientes.get(2), model, result), "clientes/formCliente");
    }
	
	@Test
    public void updateClienteActivoSuccessfullTest(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).updateCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.update(clientes.get(2), model, result), "redirect:/clientes/list");
        Assert.assertEquals(model.get("mensaje"), "Cliente: Lalo Landa, Fue Actualizado con exito!!");
    }
	
	@Test
    public void updateClienteINActivoSuccessfullTest(){
        when(result.hasErrors()).thenReturn(false);
        doNothing().when(service).updateCliente(any(Cliente.class));
        Assert.assertEquals(clienteController.update(clientes.get(1), model, result), "redirect:/clientes/list");
        Assert.assertEquals(model.get("mensaje"), "Cliente: Gustav Sanchez, Fue Actualizado con exito!!");
    }
	
	@Test
	public void deleteClienteTest() {
		Cliente c = clientes.get(2);
	    when(service.findClienteById(anyInt())).thenReturn(c);
		Assert.assertEquals(clienteController.delete(anyInt(), model), "redirect:/clientes/list");
		Assert.assertEquals(model.get("mensaje"), "Cliente: Lalo Landa, Fue dado de Baja!!");
	}
	
	@Test
	public void activeClienteTest() {
		Cliente c = clientes.get(1);
	    when(service.findClienteById(anyInt())).thenReturn(c);
		Assert.assertEquals(clienteController.active(anyInt(), model), "redirect:/clientes/list");
		Assert.assertEquals(model.get("mensaje"), "Cliente: Gustav Sanchez, Fue dado de Alta!!");
	}
}

package com.hairandhead.project.cliente.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hairandhead.project.cliente.dao.ClienteDao;
import com.hairandhead.project.cliente.model.Cliente;
import com.hairandhead.project.cliente.model.EstadoCliente;
import com.hairandhead.project.cliente.utils.Clientes;

public class ClienteServiceImplTest {

	@Mock
    ClienteDao dao;
     
    @InjectMocks
    ClienteServiceImpl clienteService;
     
    @Spy
    List<Cliente> clientes = new ArrayList<Cliente>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        clientes = Clientes.getClientesList();
    }
    
    @Test
    public void findById(){
    	Cliente c = this.clientes.get(0);
        when(dao.findById(anyInt())).thenReturn(c);
        Assert.assertEquals(clienteService.findClienteById(c.getId()),c);
        Assert.assertEquals(clienteService.findClienteById(c.getId()).getEmail(),c.getEmail());
        Assert.assertEquals(clienteService.findClienteById(c.getId()).getTelefono(),c.getTelefono());
        Assert.assertEquals(clienteService.findClienteById(c.getId()).getPuntosAcumulados(),c.getPuntosAcumulados());
        Assert.assertEquals(clienteService.findClienteById(c.getId()).getFechaDeBaja(),c.getFechaDeBaja());
        Assert.assertEquals(clienteService.findClienteById(c.getId()).toString(),c.toString());
    }
    
    @Test
    public void saveCliente(){
        doNothing().when(dao).save(any(Cliente.class));
        clienteService.saveCliente(any(Cliente.class));
        verify(dao, atLeastOnce()).save(any(Cliente.class));
    }
    
    @Test
    public void updateCliente(){
        Cliente c = clientes.get(0);
        when(dao.findById(anyInt())).thenReturn(c);
        clienteService.updateCliente(c);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void findAllEmployees(){
        when(dao.findAll()).thenReturn(clientes);
        Assert.assertEquals(clienteService.findAllClientes(), this.clientes);
    }
    
    @Test
    public void hashCodeNullTest() {
    	Cliente c = new Cliente();
    	c.setId(null);
    	c.setApellido(null);
    	c.setEmail(null);
    	c.setEstado(null);
    	c.setFechaDeBaja(null);
    	c.setNombre(null);
    	c.setPuntosAcumulados(0);
    	c.setTelefono(null);
    	Assert.assertNotEquals(c.hashCode(), this.clientes.get(0).hashCode());
    }
    
    @Test
    public void hashCodeTest() {
    	Cliente c = new Cliente();
    	c.setId(1);
    	c.setApellido("Perex");
    	c.setEmail("pepe@gamil.com");
    	c.setEstado(EstadoCliente.ACTIVO);
    	c.setFechaDeBaja(LocalDateTime.now());
    	c.setNombre("Pepe");
    	c.setPuntosAcumulados(0);
    	c.setTelefono("1323323232");
    	Assert.assertNotEquals(c.hashCode(), this.clientes.get(0).hashCode());
    }
}
